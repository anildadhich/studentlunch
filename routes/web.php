<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'HomeController@IndexPage');
Route::get('registration/parent', 'ParentController@IndexPage');
Route::post('registration/parent/save', 'ParentController@SaveCntrl');
Route::get('registration/milkbar', 'MilkbarController@IndexPage');
Route::post('registration/milkbar/save', 'MilkbarController@SaveCntrl');
Route::get('user/login', 'LoginController@IndexPage');
Route::post('user/authenticate', 'LoginController@Login');
Route::post('logout', 'LoginController@Logout');
Route::get('user/forgot', 'PasswordController@ForgotPassword');
Route::post('user/send_otp', 'PasswordController@SendCode');
Route::get('user/verify', 'PasswordController@VerifyOtp');
Route::post('user/reset_password', 'PasswordController@ResetPass');

Route::group(['middleware' => 'prevent-back-history'], function()
{
	Auth::routes();
    Route::namespace('AdminController')->group(function () {
    	
    	Route::get('admin_panel/forgot_password', 'HomeController@FrgtPass');
    	Route::post('admin_panel/forgot_password/email', 'HomeController@FrgtEmail');
    	Route::get('admin_panel/reset_password', 'HomeController@RstPass');
    	Route::post('admin_panel/reset_password/save', 'HomeController@SavePass');


    	// Manage Dashboard
    	Route::get('admin_panel', 'HomeController@IndexPage');
    	Route::post('admin_panel/login', 'HomeController@LoginPage');
    	Route::post('admin_panel/logout', 'HomeController@Logout');
    
    	// Manage Milk Bar
    	Route::get('admin_panel/milk_bar/manage', 'MilkBarController@IndexPage');
    	Route::post('admin_panel/milk_bar/save', 'MilkBarController@SaveCntrl');
    	Route::get('admin_panel/milk_bar/list', 'MilkBarController@ListPage');
    	Route::get('admin_panel/milk_bar/detail', 'MilkBarController@DetailPage');
    	Route::get('admin_panel/milk_bar/active', 'MilkBarController@ActvStatus');
    	Route::get('admin_panel/milk_bar/export', 'MilkBarController@ExprtRcrd');
    	
    	// Manage Parent
    	Route::get('admin_panel/parent/list', 'ParentController@ListPage');
    	Route::get('admin_panel/parent/detail', 'ParentController@DetailPage');
    	Route::get('admin_panel/parent/export', 'ParentController@ExprtRcrd');
    	
    	// Manage School
    	Route::get('admin_panel/school/manage', 'SchoolController@IndexPage');
    	Route::post('admin_panel/school/save', 'SchoolController@SaveCntrl');
    	Route::get('admin_panel/school/list', 'SchoolController@ListPage');
    	Route::get('admin_panel/school/detail', 'SchoolController@DetailPage');
    	Route::get('admin_panel/school/grid', 'SchoolController@GridDtl');
    	Route::get('admin_panel/school/export', 'SchoolController@ExprtRcrd');
    	
    	// Manage Passowrd
		Route::get('admin_panel/change_password', 'AccountController@PswrdPage');
		Route::post('admin_panel/change_password/save', 'AccountController@PswrdCntrl');
		
		//Manage order
		Route::get('admin_panel/manage_order', 'OrderController@ListPage');
		Route::get('admin_panel/manage_order/export', 'OrderController@ExprtRcrd');
		
		//Manage Comission
		Route::get('admin_panel/manage_commission', 'CommissionController@ListPage');
		Route::get('admin_panel/manage_commission/export', 'CommissionController@ExprtRcrd');
		
    });
    
	Route::namespace('ParentController')->group(function () {

		// Manage Dashboard
		Route::get('parent_panel', 'HomeController@IndexPage');

		// Manage Account Details
		Route::get('parent_panel/manage_account', 'AccountController@IndexPage');
		Route::post('parent_panel/manage_account/save', 'AccountController@SaveCntrl');

		// Manage Passowrd
		Route::get('parent_panel/change_password', 'AccountController@PswrdPage');
		Route::post('parent_panel/change_password/save', 'AccountController@PswrdCntrl');
      
      	// Manage Order
		Route::get('parent_panel/place_order', 'OrderController@IndexPage');
		Route::get('parent_panel/get_menu', 'OrderController@GetMenu');
		Route::get('parent_panel/get_milk', 'OrderController@GetMlk');
		Route::post('parent_panel/place_order/save', 'OrderController@SaveOrder');
		Route::get('parent_panel/review_order', 'OrderController@RvwOrder');
		Route::get('parent_panel/checkout', 'OrderController@Checkout');
		Route::post('parent_panel/checkout', 'OrderController@CheckoutPost')->name('checkout.post');
		Route::post('parent_panel/checkoutcr', 'OrderController@CheckoutCrPost')->name('checkout.crpost');
      
		//Manage order
		Route::get('parent_panel/manage_order', 'OrderController@ListOrder');
		Route::get('parent_panel/cancel_order', 'OrderController@CancelOrder');
		Route::get('parent_panel/manage_order/export', 'OrderController@ExprtRcrd');
		
		//credits
		Route::get('parent_panel/my_credits', 'CreditsController@IndexPage');
		Route::get('parent_panel/my_credits/export', 'CreditsController@ExprtRcrd');
	});

	Route::namespace('MilkBarController')->group(function () {

		// Manage Dashboard
		Route::get('milkbar_panel', 'HomeController@IndexPage');

		// Manage Account Details
		Route::get('milkbar_panel/manage_account', 'AccountController@IndexPage');
		Route::get('milkbar_panel/stripe', 'AccountController@StripePage');
		Route::get('milkbar_panel/stripe/account', 'AccountController@StripeAcc');
		Route::post('milkbar_panel/my_account/save', 'AccountController@SaveCntrl');

		// Manage Passowrd
		Route::get('milkbar_panel/change_password', 'AccountController@PswrdPage');
		Route::post('milkbar_panel/change_password/save', 'AccountController@PswrdCntrl');

		// Manage Category
		Route::get('milkbar_panel/category/list', 'CategoryController@ListPage');
		Route::get('milkbar_panel/category/export', 'CategoryController@ExprtRcrd');
		Route::post('milkbar_panel/category/save', 'CategoryController@SaveCntrl');
		
		// Manage Menu
		Route::get('milkbar_panel/item/list', 'ItemController@ListPage');
		Route::get('milkbar_panel/item/export', 'ItemController@ExprtRcrd');
		Route::post('milkbar_panel/item/save', 'ItemController@SaveCntrl');
		
		// Manage Credits
		Route::get('milkbar_panel/my_credits', 'CreditsController@ListPage');
		Route::get('milkbar_panel/my_credits/export', 'CreditsController@ExprtRcrd');

		// Manage Order
		Route::get('milkbar_panel/my_orders', 'OrderController@ListPage');
		Route::get('milkbar_panel/my_orders/deliverd', 'OrderController@DelvOrd');
		Route::get('milkbar_panel/my_orders/export', 'OrderController@ExprtRcrd');
	});
});

Route::namespace('CommonController')->group(function () {

	// Get State
	Route::get('get_state', 'AjaxController@StateLst');
	
	// Chnage Status
	Route::get('record/change_status', 'CommonController@ChngStatus');

	// Delete Record
	Route::get('record/delete', 'CommonController@DelRec');

	// Verify Email
	Route::get('account/verify', 'CommonController@EmailVrfy');
	
	// Order Detail
	Route::get('order/detail', 'CommonController@OrderDtl');

});