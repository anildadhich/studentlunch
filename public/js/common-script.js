function GetState(lCntryIdNo)
{
    $('#loadingBox').removeClass('d-none');
    $.ajax({
        url:APP_URL+"/get_state?lCntryIdNo="+btoa(lCntryIdNo),
        success:function(response)
        {
            var sCntryCode = $('option:selected','#lCntryIdNo').data("code");
            $('.cnoutry_code').val(sCntryCode);
            $('#lStateIdNo').find('option').remove();
            $('#lStateIdNo').append(`<option value="">== Select State ==</option>`); 
            StateList = JSON.parse(response);
            StateList.forEach(function(StateList){
                var lStateIdNo = StateList['lState_IdNo'];
                var sStateName = StateList['sState_Name'];
                $('#lStateIdNo').append(`<option value="${lStateIdNo}">${sStateName}</option>`);
            });            
            $('#loadingBox').addClass('d-none');
        }
    });
}

function GetOrder(lOrdrIdNo)
{
    $('#loadingBox').removeClass('d-none');
    $.ajax({
        url:APP_URL+"/order/detail?lOrdrHdIdNo="+btoa(lOrdrIdNo),
        success:function(response)
        {
			aRspns = JSON.parse(response);
            $('#sOrdrIdNo').html("Order ID: "+aRspns['oOrdrDtl']['sOrdr_Id']);
            $('#sCrtDtTm').html("Date: "+aRspns['oOrdrDtl']['sCrt_DtTm']);
            $('#sBussName').html(aRspns['oOrdrDtl']['sBuss_Name']);
            $('#sMlkAdrs').html(aRspns['oOrdrDtl']['sStrt_No']+" "+ aRspns['oOrdrDtl']['sStrt_Name'] +", "+ aRspns['oOrdrDtl']['sSbrb_Name']+", "+ aRspns['oOrdrDtl']['sCntry_Name']+", "+ aRspns['oOrdrDtl']['sState_Name']+", "+ aRspns['oOrdrDtl']['sPin_Code']);
            $('#sMlkPhnNo').html(aRspns['oOrdrDtl']['sPhone_No']);
            $('#sMlkEmail').html(aRspns['oOrdrDtl']['sEmail_Id']);
			
			$('#sChldName').html(aRspns['oOrdrDtl']['sChld_Fname']+' '+aRspns['oOrdrDtl']['sChld_Lname']);
            $('#sSchlAdrs').html(aRspns['aSchlDtl']['sStrt_No']+" "+ aRspns['aSchlDtl']['sStrt_Name'] +", "+ aRspns['aSchlDtl']['sSbrb_Name']+", "+ aRspns['aSchlDtl']['sCntry_Name']+", "+ aRspns['aSchlDtl']['sState_Name']+", "+ aRspns['aSchlDtl']['sPin_Code']);
            $('#sSchlPhnNo').html(aRspns['aSchlDtl']['sPhone_No']);
            $('#sSchlEmail').html(aRspns['aSchlDtl']['sEmail_Id']);
            var i=1;
			var content = "";
			var total = 0;
			aRspns['oOrdrItms'].forEach(function(oOrdrItm){
				content = content + "<tr><td >"+i+"</td><td>"+oOrdrItm['sItem_Name']+"</td><td>"+oOrdrItm['nItm_Qty']+"</td><td>"+parseFloat(oOrdrItm['sItem_Prc']).toFixed(2)+"</td><td class='text-right'>$"+parseFloat(oOrdrItm['sItem_Prc'] * oOrdrItm['nItm_Qty']).toFixed(2)+"</td></tr>";
				i=i+1;
				total = total + (oOrdrItm['sItem_Prc'] * oOrdrItm['nItm_Qty']);
			});
			$('#aItms').html(content);
			
            $('#sSubTtl').html("$ "+parseFloat(total).toFixed(2));
            $('#sGst').html("$ "+parseFloat(total * 0.18).toFixed(2));
			if(aRspns['OrdrAmnt'] == null){
				$('#sCrdt').html('$ 0.00');
				$('#sPay').html("$ "+parseFloat(total + (total * 0.18)).toFixed(2));
			}else{
				$('#sCrdt').html("$ "+parseFloat(aRspns['OrdrAmnt']['sTtl_Amo']).toFixed(2));
				$('#sPay').html("$ "+parseFloat(total + (total * 0.18) - aRspns['OrdrAmnt']['sTtl_Amo']).toFixed(2));
			}
            $('#sTtl').html("$ "+parseFloat(total + (total * 0.18)).toFixed(2));
            
            $('#loadingBox').addClass('d-none');
        }
    });
}