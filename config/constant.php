<?php
return [
	"TITLE"	=> [
		"Dr."	=> 101,
		"Mr."	=> 102,
		"Miss."	=> 103,
		"Mr."	=> 104,
		"Ms."	=> 105,
		"Mrs."	=> 106,
	],
  
    "GST"	=> 0.18,
	
	"WEEK"	=> [
		"MONDAY"	=> 111,
		"TUESDAY"	=> 112,
		"WEDNESDAY"	=> 113,
		"THURSDAY"	=> 114,
		"FRIDAY"	=> 115,
		"SATURDAY"	=> 116,
		"SUNDAY"	=> 117,
	],

    "MONTH"	=> [
		"1"		=> "JAN",
		"2"		=> "FEB",
		"3"		=> "MAR",
		"4"		=> "APR",
		"5"		=> "MAY",
		"6"		=> "JUN",
		"7"		=> "JUL",
		"8"		=> "AUG",
		"9"		=> "SEP",
		"10"	=> "OCT",
		"11"	=> "NOV",
		"12"	=> "DEC",
	],

    "TRANS"	=> [
		"Credit"	=> 101,
		"Debit"		=> 102,
	],
	
	"ORDER_STATUS"	=> [
		"Pending"		=> 211,
		"Delivered"		=> 212,
		"Cancelled"		=> 213,
	],

	"USER"	=> [
		"PARENT"	=> 801,
		"MILK_BAR"	=> 802,
	],

	"STATUS"	=> [
		"BLOCK"		=> 409,
		"UNBLOCK"	=> 410,
	],

	"DEL_STATUS"	=> [
		"DELETED"		=> 609,
		"UNDELETED"		=> 610,
	],

	"MLK_STATUS"	=> [
		"UNACTIVE"		=> 209,
		"ACTIVE"		=> 210,
	],

	"MAIL_STATUS"	=> [
		"UNVERIFIED"	=> 309,
		"VERIFIED"		=> 310,
	],

	"SCHL_TYPE"	=> [
		"Child Care"	=> 501,
		"Kinder" 		=> 502,
		"Primary" 		=> 503,
		"Secondary" 	=> 504,
	],

	"SCHL_ROLE"	=> [
		"Admin"					=> 601,
		"Principle"				=> 602,
		"Assistant Principle"	=> 603,

	],

	"BUSS_TYPE"	=> [
		"Sole Proprietorship"	=> 701,
		"Small Business Onwer"	=> 702,
		"Family Business"		=> 703,
		"Other"					=> 704,
	],

	"RLTN_IDNO"	=> [
		"Father"	=> 1001,
		"Mother"	=> 1002,
	],

	'VLDT_MSG' => [
		'required' 			=> 'Required filed missing',
        'unique' 			=> 'Linked with another account',
        'alpha' 			=> 'Only alphabetic characters allowed',
        'digits' 			=> 'Minimum :digits numeric characters allowed',
        'min'				=> 'To sort, at least :min characters',
        'max'				=> 'To long, max :max characters',
        'between' 			=> 'Distance must be at least 0 - 7',
        'accepted' 			=> 'Accept Terms & Conditions',
        'integer' 			=> 'Must be an integer',
        'alpha_dash' 		=> 'Space not allowed',
        'same' 				=> 'Confirm password did not matched.',
        'sLgnPass.regex' 	=> 'Should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        'sCurrPass.regex' 	=> 'Should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        'sBussName.regex'	=> 'Special and numeric characters not allowed',
        'sFrstName.regex'	=> 'Special and numeric characters not allowed',
        'sLstName.regex'	=> 'Special and numeric characters not allowed',
        'sFrstName1.regex'	=> 'Special and numeric characters not allowed',
        'sLstName1.regex'	=> 'Special and numeric characters not allowed',
        'sSbrbName.regex'	=> 'Special and numeric characters not allowed',
        'sSbrbName1.regex'	=> 'Special and numeric characters not allowed',
        'sStrtName.regex'	=> 'Special and numeric characters not allowed',
        'sLgnName.regex'	=> 'Should contain alphabetic and underscore',
        'sCntryName.regex'	=> 'Should contain alphabetic and space',
        'sStateName.regex'	=> 'Should contain alphabetic and space',
        'sEmail.regex'		=> 'Invalid email address, Capital letter not allowed',
        'sLgnEmail.regex'	=> 'Couldn’t find your Account.',
	],
]
?>