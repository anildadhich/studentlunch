<?php
namespace App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SuperAdmin;
use App\Model\Wallet;
use App\Model\MilkBar;
use Excel;

class CommissionController extends Controller
{
	public function __construct()
	{
		$this->Wallet 	= new Wallet;
		$this->MilkBar 	= new MilkBar;
		$this->middleware(SuperAdmin::class);
	}

	public function ListPage(Request $request)
	{
		$sFrmDate  		= !empty($request['sFrmDate']) ? $request['sFrmDate']." 00:00:00" : '';
		$sToDate  		= !empty($request['sToDate']) ? $request['sToDate']." 23:59:59" : '';
		$lMilkIdNo  	= $request['lMilkIdNo'];
		$aMilkLst		= $this->MilkBar->FltrMilkLst();
		$oComLst		= $this->Wallet->CommLst($sFrmDate, $sToDate, $lMilkIdNo);
		$sTitle 		= "Manage Commission";
    	$aData 			= compact('sTitle','oComLst','aMilkLst','request');
        return view('admin_panel.commission_list',$aData);	
	}
	
	public function ExprtRcrd(Request $request)
	{
		$sFrmDate  		= !empty($request['sFrmDate']) ? $request['sFrmDate']." 00:00:00" : '';
		$sToDate  		= !empty($request['sToDate']) ? $request['sToDate']." 23:59:59" : '';
		$lMilkIdNo  	= $request['lMilkIdNo'];
		$aComLst		= $this->Wallet->ExlRcrdCom($sFrmDate, $sToDate, $lMilkIdNo);
		if(count($aComLst) > 0)
		{
			$FileName = 'Manage_Commission_'.date('Ymd').'_'.date('His');
	        Excel::create($FileName, function($excel) use ($aComLst) {
	            $excel->sheet('Sheet1', function($sheet)  use ($aComLst) {
	                $this->SetExlHeader($sheet, $lRaw);
	                $this->SetExlData($sheet, $lRaw, $aComLst);
	            });
	        })->download('xlsx');
	    }
	    else
	    {
        	return redirect()->back()->with('Success', 'Record not found...');
	    }

	}

	public function SetExlHeader($sheet, &$lRaw)
	{
		$lRaw = 1;
		Controller::SetCell(config('excel.XL_ORD_COMM.SR_NO'), $lRaw, 'Sr. No', $sheet, '', '#F2DDDC', 'left', True, '', False, 8, '', 10);
		Controller::SetCell(config('excel.XL_ORD_COMM.TRAN_DATE'), $lRaw, 'Transaction Date', $sheet, '', '#F2DDDC', 'left', True, '', False, 20, '', 10);
		Controller::SetCell(config('excel.XL_ORD_COMM.ORD_NO'), $lRaw, 'Order No', $sheet, '', '#F2DDDC', 'left', True, '', False, 12, '', 10);
		Controller::SetCell(config('excel.XL_ORD_COMM.MILK_NAME'), $lRaw, 'Milk Bar Name', $sheet, '', '#F2DDDC', 'left', True, '', False, 25, '', 10);
		Controller::SetCell(config('excel.XL_ORD_COMM.ORD_AMO'), $lRaw, 'Order Amount', $sheet, '', '#F2DDDC', 'left', True, '', False, 12, '', 10);
		Controller::SetCell(config('excel.XL_ORD_COMM.CRDT_AMO'), $lRaw, 'Used Credit', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ORD_COMM.PAY_AMO'), $lRaw, 'Pay Online', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ORD_COMM.COMM_AMO'), $lRaw, 'Commission', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
	}

	public function SetExlData($sheet, $lRaw, $aComLst)
	{
		$i = 0;
		while(isset($aComLst) && count($aComLst) > 0 && $i<count($aComLst))
		{
			$lRaw = $lRaw + 1;
			Controller::SetCell(config('excel.XL_ORD_COMM.SR_NO'), $lRaw, $i+1, $sheet, config('excel.XL_ORD_COMM.SR_NO'), '', 'right', False, '', False, 8, '', 10);
			Controller::SetCell(config('excel.XL_ORD_COMM.TRAN_DATE'), $lRaw, date('d M, Y h:i A', strtotime($aComLst[$i]['sCrt_DtTm'])), $sheet, '', '', 'left', True, '', False, 20, '', 10);
			Controller::SetCell(config('excel.XL_ORD_COMM.ORD_NO'), $lRaw, $aComLst[$i]['sOrdr_Id'], $sheet, '', '', 'left', True, '', False, 12, '', 10);
			Controller::SetCell(config('excel.XL_ORD_COMM.MILK_NAME'), $lRaw, $aComLst[$i]['sBuss_Name'], $sheet, '', '', 'left', False, '', False, 25, '', 10);
			Controller::SetCell(config('excel.XL_ORD_COMM.ORD_AMO'), $lRaw, $aComLst[$i]['sGrnd_Ttl'], $sheet, '', '', 'right', False, '#0.00', False, 12, '', 10);
			Controller::SetCell(config('excel.XL_ORD_COMM.CRDT_AMO'), $lRaw, $aComLst[$i]['sTtl_Amo'], $sheet, '', '', 'right', False, '#0.00', False, 10, '', 10);
			Controller::SetCell(config('excel.XL_ORD_COMM.PAY_AMO'), $lRaw, '='.Controller::GetColName(config('excel.XL_ORD_COMM.ORD_AMO')).$lRaw.'-'.Controller::GetColName(config('excel.XL_ORD_COMM.CRDT_AMO')).$lRaw, $sheet, '', '', 'right', False, '#0.00', False, 10, '', 10);
			Controller::SetCell(config('excel.XL_ORD_COMM.COMM_AMO'), $lRaw, '='.Controller::GetColName(config('excel.XL_ORD_COMM.PAY_AMO')).$lRaw.'*0.125', $sheet, '', '', 'right', False, '#0.00', False, 10, '', 10);
			$i++;
		}
	}
}
?>