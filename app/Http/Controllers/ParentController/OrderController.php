<?php
namespace App\Http\Controllers\ParentController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ParentAuth;
use Validator;
use Stripe;
use App\Model\Parents;
use App\Model\Child;
use App\Model\School;
use App\Model\Country;
use App\Model\State;
use App\Model\Item;
use App\Model\Wallet;
use App\Model\MilkBar;
use App\Model\OrderHd;
use App\Model\OrderDetail;
use App\Model\AssociateSchool;
use Excel;

class OrderController extends Controller
{
	public function __construct()
	{
		$this->Parents 	= new Parents;
		$this->Child 	= new Child;
		$this->School 	= new School;
		$this->MilkBar 	= new MilkBar;
		$this->Country 	= new Country;
		$this->State 	= new State;
		$this->OrderHd 	= new OrderHd;
		$this->OrderDetail 	= new OrderDetail;
		$this->Item 	= new Item;
		$this->Wallet 	= new Wallet;
		$this->AssociateSchool 	= new AssociateSchool;
		$this->middleware(ParentAuth::class);
	}

	public function IndexPage()
	{
		$lPrntIdNo 	= session('USER_ID');
		$aChldLst 	= $this->Child->ChldLst($lPrntIdNo);
		$aSchlIdLst	= array();
		foreach($aChldLst as $aChld){
			array_push($aSchlIdLst, $aChld['lSchl_IdNo']);
		}
		
		$aCart		= null;
		if(session()->has('CART_ITEMS')){
			$aCart		= json_decode(session('CART_ITEMS'));
		}
		
		$sTitle 	= "My Order";
    	$aData 		= compact('sTitle','aChldLst', 'aCart');
        return view('parent_panel.place_order',$aData);	
	}
	
	public function GetMlk(Request $request){
		$aMlkBars = $this->AssociateSchool->MlkLst($request['schl'], $request['dttm']);
		return json_encode($aMlkBars);
	}
	
	public function GetMenu(Request $request){
		$aItems = $this->Item->ItemCtgryLst($request['milkbar']);
		if(session()->has('CART_ITEMS')){
			$aCart		= json_decode(session('CART_ITEMS'));
			foreach($aItems as $key => $aItem){
				foreach($aCart->aCartItem as $lItemIdNo => $nItemQty)
				{
					if($aItem['lItem_IdNo'] == $lItemIdNo){
						$aItems[$key]['selected'] = true;
						$aItems[$key]['nItemQty'] = $nItemQty;
					}
				}
			}
		}
		return json_encode($aItems);
	}

	public function SaveOrder(Request $request)
	{
		$lPrntIdNo = session('USER_ID');
		$rules = [
	        'lChldIdNo' 	=> 'required',
            'lMilkIdNo' 	=> 'required',
            'sDtTm'         => 'required',
	    ];

	    $this->validate($request, $rules, config('constant.VLDT_MSG'));
		try
		{
			$aCart['lChldIdNo'] = $request['lChldIdNo'];
			$aCart['lMilkIdNo'] = $request['lMilkIdNo'];
			$aCart['sDelvDate'] = $request['sDtTm'];
			foreach($request['lItemIdNo'] as $lItemIdNo){
				$aCart['aCartItem'][$lItemIdNo] = $request['lItemQty'.$lItemIdNo];
			}
			$oCart = json_encode($aCart);
		    session(['CART_ITEMS' => $oCart]);
		    return redirect('parent_panel/review_order');
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', $e->getMessage().' on Line '.$e->getLine());
		}
	}
	
	public function RvwOrder(Request $request)
	{
		$sTitle 	= "My Order";
		if(!session()->has('CART_ITEMS')){
			return redirect("parent_panel/place_order");
		}
		$aCart		= json_decode(session('CART_ITEMS'));
		
		$aChldDtl	= $this->Child->ChldDtl($aCart->lChldIdNo);
		$aMlkDtl	= $this->MilkBar->MilkDtl($aCart->lMilkIdNo);
		$aItemQty	= array();
		$aItems		= array();

		foreach($aCart->aCartItem as $lItemIdNo => $nItemQty)
		{
			array_push($aItemQty, $nItemQty);
			array_push($aItems, $lItemIdNo);
		}
		$aCartData = $this->Item->CartItemLst($aItems);
    	$aData 		= compact('sTitle', 'aChldDtl', 'aMlkDtl', 'aItemQty', 'aCartData', 'request');
        return view('parent_panel.review_order',$aData);
	}
	
	public function Checkout()
	{
		$sTitle 	= "Checkout";
		if(!session()->has('CART_ITEMS')){
			return redirect("parent_panel/place_order");
		}
		$aCart		= json_decode(session('CART_ITEMS'));
		
		$lPrntIdNo 	= session('USER_ID');
		$aPrntsDtl 	= $this->Parents->PrntsDtl($lPrntIdNo);
		$aChldDtl	= $this->Child->ChldDtl($aCart->lChldIdNo);
		$aSchlDtl	= $this->School->SchlDtl($aChldDtl['lSchl_IdNo']);
		$aItemQty	= array();
		$aItems		= array();

		foreach($aCart->aCartItem as $lItemIdNo => $nItemQty)
		{
			array_push($aItemQty, $nItemQty);
			array_push($aItems, $lItemIdNo);
		}
		
		$i = 0;
		$total = 0;
		$qty = 0;
		
		$aCartData = $this->Item->CartItemLst($aItems);
		foreach($aCartData as $aRec){
			$total 	+= $aRec->sItem_Prc * $aItemQty[$i];
			$qty	+= 	$aItemQty[$i];
			$i++;
		}
		
		$nTtlCrdt = 0;
		$sMlkCrdt = 0;
		$aCrdtsArr = $this->Wallet->GetCrdts($lPrntIdNo);
		
		foreach($aCrdtsArr as $aCrdts)
		{
			if($aCrdts['nTyp_Status'] == config('constant.TRANS.Credit'))
			{
				$nTtlCrdt += $aCrdts['sTtl_Amo'];
				$nTtlCrdt = round($nTtlCrdt, 2);
				if($aCrdts['lMilk_IdNo'] == $aCart->lMilkIdNo)
				{
					$sMlkCrdt += $aCrdts['sTtl_Amo'];
					$sMlkCrdt = round($sMlkCrdt, 2);
				}
			}else{
				$nTtlCrdt -= $aCrdts['sTtl_Amo'];
				$nTtlCrdt = round($nTtlCrdt, 2);
				if($aCrdts['lMilk_IdNo'] == $aCart->lMilkIdNo)
				{
					$sMlkCrdt -= $aCrdts['sTtl_Amo'];
					$sMlkCrdt = round($sMlkCrdt, 2);
				}
			}
		}
		
		$aData 		= compact('sTitle', 'aSchlDtl', 'qty', 'total', 'aCartData', 'aPrntsDtl', 'nTtlCrdt', 'sMlkCrdt');
		
		return view('parent_panel.checkout',$aData);
	}
	
	public function CheckoutPost(Request $request)
	{
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		
		$lPrntIdNo 	= session('USER_ID');
		$aCart		= json_decode(session('CART_ITEMS'));
		$aPrntsDtl 	= $this->Parents->PrntsDtl($lPrntIdNo);
		$aCntryLst	= $this->Country->FrntLst();
		$aStateLst	= $this->State->FrntLst($aPrntsDtl['lCntry_IdNo']);
		
		$sCntry	=	null;
		foreach($aCntryLst as $aRec)
		{
			if($aPrntsDtl['lCntry_IdNo'] == $aRec['lCntry_IdNo'])
			{
				$sCntry	= $aRec['sCntry_Name'];
			}
		}
		
		$sState	=	null;
		foreach($aStateLst as $aRec)
		{
			if($aPrntsDtl['lState_IdNo'] == $aRec['lState_IdNo'])
			{
				$sState	= $aRec['lState_IdNo'];
			}
		}
		
		$aItemQty	= array();
		$aItems		= array();
		
		foreach($aCart->aCartItem as $lItemIdNo => $nItemQty)
		{
			array_push($aItemQty, $nItemQty);
			array_push($aItems, $lItemIdNo);
		}
		
		$i = 0;
		$total = 0;
		$qty = 0;
		
		$aCartData = $this->Item->CartItemLst($aItems);
		foreach($aCartData as $aRec){
			$total 	+= $aRec->sItem_Prc * $aItemQty[$i];
			$qty	+= 	$aItemQty[$i];
			$i++;
		}
		$sMlkCrdt = 0;
		
		$sGrndTtl = $total;
		$total = number_format($total + ($total  * config('constant.GST')), 2, '.', '');
		$aCrdtsArr = $this->Wallet->GetCrdts($lPrntIdNo);
		foreach($aCrdtsArr as $aCrdts)
		{
			if($aCrdts['nTyp_Status'] == config('constant.TRANS.Credit') && $aCrdts['lMilk_IdNo'] == $aCart->lMilkIdNo)
			{
				$sMlkCrdt += $aCrdts['sTtl_Amo'];
			}
			elseif($aCrdts['nTyp_Status'] == config('constant.TRANS.Debit') && $aCrdts['lMilk_IdNo'] == $aCart->lMilkIdNo)
			{
				$sMlkCrdt -= $aCrdts['sTtl_Amo'];
			}
		}
		$total = $total - $sMlkCrdt;
				
		try{
			$customer = \Stripe\Customer::create(array(
				'name' => $aPrntsDtl['sFrst_Name'].' '.$aPrntsDtl['sLst_Name'],
				'description' => $aPrntsDtl['sAcc_Id'],
				'email' => $aPrntsDtl['sEmail_Id'],
				"source" => $request->stripeToken,
				"address" => ["city" => $aPrntsDtl['sSbrb_Name'], "country" => $sCntry, "line1" => $aPrntsDtl['sSbrb_Name'], "postal_code" => $aPrntsDtl['sPin_Code'], "state" => $sState]
			));
			
			$orderID =  substr(str_shuffle('1234567890'),0, 8);
			
			$aOrder = Stripe\Charge::create ([
					"customer" => $customer->id,
					"amount" => number_format($total , 2, '.', '')* 100,
					"currency" => "aud",
					"description" => "Payment for Order Number: ".$orderID.".",
					"metadata" => array( 
								'order_id' => $orderID 
							)
			]);
			
			$aChldDtl	= $this->Child->ChldDtl($aCart->lChldIdNo);
			$aSchlDtl	= $this->School->SchlDtl($aChldDtl['lSchl_IdNo']);
			$aOrderHdArr	= $this->HdArr($aCart->sDelvDate, $orderID, $aCart->lMilkIdNo, $aPrntsDtl['lPrnt_IdNo'], $aCart->lChldIdNo, $aSchlDtl['lSchl_IdNo'], $sGrndTtl, $aOrder->id);
			$lOrdrHdIdNo 	= $this->OrderHd->InsrtRecrd($aOrderHdArr);
			
			foreach($aCart->aCartItem as $lItemIdNo => $nItemQty)
			{
				$oItmDtl = $this->Item->DtlItm($lItemIdNo);
				$aOrderHdArr	= $this->DtlArr($lOrdrHdIdNo, $oItmDtl->lCatg_IdNo, $lItemIdNo, $nItemQty, $oItmDtl->sItem_Prc);
				$this->OrderDetail->InsrtRecrd($aOrderHdArr);
			}
			
			$oOrdrDtl = $this->OrderHd->GetOrder($lOrdrHdIdNo);
			if($sMlkCrdt > 0)
			{
			    $aWlltArr = $this->WlltArr($oOrdrDtl, config('constant.TRANS.Debit'), $sMlkCrdt);
			    $this->Wallet->InsrtRecrd($aWlltArr);
			}
			$aMlkDtl	= $this->MilkBar->MilkDtl($aCart->lMilkIdNo);
			$stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
			$oTrnsctn = $stripe->transfers->create([
			  'amount' => number_format($total * 0.85 , 2, '.', '')* 100, 
			  'currency' => 'aud',
			  'destination' => $aMlkDtl['sStrp_Acc_Id'],
			  "source_transaction" => $aOrder->id,
			  'transfer_group' => $lOrdrHdIdNo,
			]);
			
			$this->OrderHd->UpdateTrans($lOrdrHdIdNo, $oTrnsctn->id);
			
			session()->forget('CART_ITEMS');
			return redirect("parent_panel/manage_order")->with('Success', 'Your Order has been successfully placed!');
		}
		catch(\Exception $e){
			return redirect()->back()->with('Failed', "Unable to process payment this time. Please try again later");
		}  
	}
	
	
	public function CheckoutCrPost(Request $request)
	{
		$lPrntIdNo 	= session('USER_ID');
		$aCart		= json_decode(session('CART_ITEMS'));
		$aPrntsDtl 	= $this->Parents->PrntsDtl($lPrntIdNo);
		$aCntryLst	= $this->Country->FrntLst();
		$aStateLst	= $this->State->FrntLst($aPrntsDtl['lCntry_IdNo']);
		
		$sCntry	=	null;
		foreach($aCntryLst as $aRec)
		{
			if($aPrntsDtl['lCntry_IdNo'] == $aRec['lCntry_IdNo'])
			{
				$sCntry	= $aRec['sCntry_Name'];
			}
		}
		
		$sState	=	null;
		foreach($aStateLst as $aRec)
		{
			if($aPrntsDtl['lState_IdNo'] == $aRec['lState_IdNo'])
			{
				$sState	= $aRec['lState_IdNo'];
			}
		}
		
		$aItemQty	= array();
		$aItems		= array();
		
		foreach($aCart->aCartItem as $lItemIdNo => $nItemQty)
		{
			array_push($aItemQty, $nItemQty);
			array_push($aItems, $lItemIdNo);
		}
		
		$i = 0;
		$total = 0;
		$qty = 0;
		
		$aCartData = $this->Item->CartItemLst($aItems);
		foreach($aCartData as $aRec){
			$total 	+= $aRec->sItem_Prc * $aItemQty[$i];
			$qty	+= 	$aItemQty[$i];
			$i++;
		}
		$sSimpleTtl = $total;
		$total = number_format($total + ($total  * config('constant.GST')), 2, '.', '');
			
		try{
			$orderID =  substr(str_shuffle('1234567890'),0, 8);
			
			$aChldDtl	= $this->Child->ChldDtl($aCart->lChldIdNo);
			$aSchlDtl	= $this->School->SchlDtl($aChldDtl['lSchl_IdNo']);
			
			$aOrderHdArr	= $this->HdArr($aCart->sDelvDate, $orderID, $aCart->lMilkIdNo, $aPrntsDtl['lPrnt_IdNo'], $aCart->lChldIdNo, $aSchlDtl['lSchl_IdNo'], $sSimpleTtl);
			$lOrdrHdIdNo 	= $this->OrderHd->InsrtRecrd($aOrderHdArr);
			
			foreach($aCart->aCartItem as $lItemIdNo => $nItemQty)
			{
				$oItmDtl = $this->Item->DtlItm($lItemIdNo);
				$aOrderHdArr	= $this->DtlArr($lOrdrHdIdNo, $oItmDtl->lCatg_IdNo, $lItemIdNo, $nItemQty, $oItmDtl->sItem_Prc);
				$this->OrderDetail->InsrtRecrd($aOrderHdArr);
			}
			
			$oOrdrDtl = $this->OrderHd->GetOrder($lOrdrHdIdNo);
			$aWlltArr = $this->WlltArr($oOrdrDtl, config('constant.TRANS.Debit'), $total);
			$this->Wallet->InsrtRecrd($aWlltArr);
			$aMlkDtl	= $this->MilkBar->MilkDtl($aCart->lMilkIdNo);
			
			session()->forget('CART_ITEMS');
			return redirect("parent_panel/manage_order")->with('Success', 'Your Order has been successfully placed!');
		}
		catch(\Exception $e){
			return redirect()->back()->with('Failed', "Unable to process payment this time. Please try again later");
		} 
	}
	
	public function HdArr($sDelvDate, $sOrdrId, $lMilkIdNo, $lPrntIdNo, $lChldIdNo, $lSchlIdNo, $sSubTtl, $sStrpTrnsId=null)
	{
		$aConArr = array(
		    'sOrdr_Id'      => $sOrdrId,
			'lMilk_IdNo' 	=> $lMilkIdNo,
			'lPrnt_IdNo' 	=> $lPrntIdNo,
            'lChld_IdNo' 	=> $lChldIdNo,
            'lSchl_IdNo' 	=> $lSchlIdNo,
			'sDelv_Date'	=> $sDelvDate,
            'sSub_Ttl' 		=> number_format($sSubTtl, 2, '.', ''),
            'sGst_Amo'		=> number_format($sSubTtl  * config('constant.GST'), 2, '.', ''),
            'sGrnd_Ttl'		=> number_format($sSubTtl + ($sSubTtl  * config('constant.GST')), 2, '.', ''),
            'nOrdr_Status' 	=> config('constant.ORDER_STATUS.Pending'),
            'sStrp_Trns_Id'	=> $sStrpTrnsId,
		);
		return $aConArr;
	}
	
	public function DtlArr($lOrdrHdIdNo, $lCtgryIdNo, $lItmIdNo, $nItmQty, $sItemPrc)
	{
		$aConArr = array(
			'lOrdr_Hd_IdNo' => $lOrdrHdIdNo,
			'lCtgry_IdNo' 	=> $lCtgryIdNo,
            'lItm_IdNo' 	=> $lItmIdNo,
            'nItm_Qty' 		=> $nItmQty,
            'sItem_Prc' 	=> $sItemPrc,
		);
		return $aConArr;
	}
	
	public function ListOrder(Request $request)
	{
		$lPrntIdNo 		= session('USER_ID');
		$sCrtDtTm 		= $request['sCrtDtTm'];
		$nOrdrStatus 	= $request['nOrdrStatus'];
		$oOrderLst 		= $this->OrderHd->OrderLst($lPrntIdNo, $sCrtDtTm, $nOrdrStatus);
		$sTitle 		= "Manage Order";
    	$aData 			= compact('sTitle','oOrderLst','request');
        return view('parent_panel.order_list',$aData);
	}
	
	public function CancelOrder(Request $request)
	{
	    $lOrderIdNo = base64_decode($request['lRecIdNo']);
		$oOrdrDtl = $this->OrderHd->GetOrder($lOrderIdNo);
		$oSchl = $this->AssociateSchool->GetRecrd($oOrdrDtl->lSchl_IdNo, $oOrdrDtl->lMilk_IdNo);
		if(strtotime($oSchl->sCut_Tm) > strtotime(date('H:i:s')) && strtotime($oOrdrDtl->sDelv_Date) >= strtotime(date('Y-m-d'))){
			$oOrdrDtl = $this->OrderHd->Cancel($lOrderIdNo);
		}else{
			return redirect()->back()->with('Failed', "Order can't be cancelled after cutoff time.");
		}
		
		
		//add to wallet
		
		$aWlltArr = $this->WlltArr($oOrdrDtl, config('constant.TRANS.Credit'), $oOrdrDtl->sGrnd_Ttl);
		$this->Wallet->InsrtRecrd($aWlltArr);
		
		return redirect()->back()->with('Success', "Order cancelled successfully. Amount is added to your wallet.");
	}
	
	public function WlltArr($oOrdrDtl, $nTrans, $sAmnt)
	{
		$aWlltArr = array(
			'lOrder_IdNo' 	=> $oOrdrDtl->lOrder_IdNo,
            'lMilk_IdNo' 	=> $oOrdrDtl->lMilk_IdNo,
            'lPrnt_IdNo' 	=> $oOrdrDtl->lPrnt_IdNo,
            'lChld_IdNo' 	=> $oOrdrDtl->lChld_IdNo,
			'sTtl_Amo'		=> $sAmnt,
			'nTyp_Status'	=> $nTrans,
		);
		return $aWlltArr;
	}
	
	public function ExprtRcrd(Request $request)
	{
		$lPrntIdNo 		= session('USER_ID');
		$sCrtDtTm  		= $request['sCrtDtTm'];
		$nOrdrStatus  	= $request['nOrdrStatus'];
		$aOrdLst		= $this->OrderHd->ExlRcrdPrnt($lPrntIdNo, $sCrtDtTm, $nOrdrStatus);
		if(count($aOrdLst) > 0)
		{
			$FileName = 'My_Orders_'.date('Ymd').'_'.date('His');
	        Excel::create($FileName, function($excel) use ($aOrdLst) {
	            $excel->sheet('Sheet1', function($sheet)  use ($aOrdLst) {
	                $this->SetExlHeader($sheet, $lRaw);
	                $this->SetExlData($sheet, $lRaw, $aOrdLst);
	            });
	        })->download('xlsx');
	    }
	    else
	    {
        	return redirect()->back()->with('Success', 'Record not found...');
	    }

	}

	public function SetExlHeader($sheet, &$lRaw)
	{
		$lRaw = 1;
		Controller::SetCell(config('excel.XL_ORD_PRNT.SR_NO'), $lRaw, 'Sr. No', $sheet, '', '#F2DDDC', 'left', True, '', False, 8, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.DEL_DATE'), $lRaw, 'Delivery Date', $sheet, '', '#F2DDDC', 'left', True, '', False, 12, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.ORD_NO'), $lRaw, 'Order No', $sheet, '', '#F2DDDC', 'left', True, '', False, 12, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.MILK_NAME'), $lRaw, 'Milk Bar Name', $sheet, '', '#F2DDDC', 'left', True, '', False, 25, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.STDNT_NAME'), $lRaw, 'Student Name', $sheet, '', '#F2DDDC', 'left', True, '', False, 25, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.SUB_AMO'), $lRaw, 'Sub Total', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.GST_AMO'), $lRaw, 'GST', $sheet, '', '#F2DDDC', 'left', True, '', False, 8, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.GRNT_AMO'), $lRaw, 'Grand Total', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.TRAN_DATE'), $lRaw, 'Transaction Date', $sheet, '', '#F2DDDC', 'left', True, '', False, 18, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.ORD_STATUS'), $lRaw, 'Status', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.ITM_NAME'), $lRaw, 'Item Name', $sheet, '', '#F2DDDC', 'left', True, '', False, 25, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.ITM_PRC'), $lRaw, 'Item Price', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.ITM_QTY'), $lRaw, 'Quantity', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ORD_PRNT.TTL_PRC'), $lRaw, 'Item Total', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
	}

	public function SetExlData($sheet, $lRaw, $aOrdLst)
	{
		$i = 0;
		while(isset($aOrdLst) && count($aOrdLst) > 0 && $i<count($aOrdLst))
		{
			$lRaw = $lRaw + 1;
			$aItmLst = $this->OrderDetail->ExlRcrd($aOrdLst[$i]['lOrder_IdNo']);
			$nMrgCell = count($aItmLst) > 1 ? count($aItmLst) - 1 : '';
			Controller::SetCell(config('excel.XL_ORD_PRNT.SR_NO'), $lRaw, $i+1, $sheet, config('excel.XL_ORD_PRNT.SR_NO'), '', 'right', False, '', False, 8, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.DEL_DATE'), $lRaw, date('d M, Y', strtotime($aOrdLst[$i]['sDelv_Date'])), $sheet, config('excel.XL_ORD_PRNT.DEL_DATE'), '', 'left', True, '', False, 12, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.ORD_NO'), $lRaw, $aOrdLst[$i]['sOrdr_Id'], $sheet, config('excel.XL_ORD_PRNT.ORD_NO'), '', 'left', True, '', False, 12, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.MILK_NAME'), $lRaw, $aOrdLst[$i]['sBuss_Name'], $sheet, config('excel.XL_ORD_PRNT.MILK_NAME'), '', 'left', False, '', False, 25, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.STDNT_NAME'), $lRaw, $aOrdLst[$i]['sChld_FName']." ".$aOrdLst[$i]['sChld_LName'], $sheet, config('excel.XL_ORD_PRNT.STDNT_NAME'), '', 'left', False, '', False, 25, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.SUB_AMO'), $lRaw, $aOrdLst[$i]['sSub_Ttl'], $sheet, config('excel.XL_ORD_PRNT.SUB_AMO'), '', 'right', False, '#0.00', False, 10, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.GST_AMO'), $lRaw, $aOrdLst[$i]['sGst_Amo'], $sheet, config('excel.XL_ORD_PRNT.GST_AMO'), '', 'right', False, '#0.00', False, 8, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.GRNT_AMO'), $lRaw, $aOrdLst[$i]['sGrnd_Ttl'], $sheet, config('excel.XL_ORD_PRNT.GRNT_AMO'), '', 'right', False, '#0.00', False, 10, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.ORD_STATUS'), $lRaw, strtoupper(array_search($aOrdLst[$i]['nOrdr_Status'], config('constant.ORDER_STATUS'))), $sheet, config('excel.XL_ORD_PRNT.ORD_STATUS'), '', 'center', True, '', False, 10, $nMrgCell, 10);
			Controller::SetCell(config('excel.XL_ORD_PRNT.TRAN_DATE'), $lRaw, date('d M, Y h:i A', strtotime($aOrdLst[$i]['sCrt_DtTm'])), $sheet, config('excel.XL_ORD_PRNT.TRAN_DATE'), '', 'left', False, '', False, 18, $nMrgCell, 10);

			$c = 0;
			while(isset($aItmLst) && count($aItmLst) > 0 && $c<count($aItmLst))
			{
				Controller::SetCell(config('excel.XL_ORD_PRNT.ITM_NAME'), $lRaw, $aItmLst[$c]['sItem_Name'], $sheet, '', '', 'left', False, '', False, 25, '', 10);
				Controller::SetCell(config('excel.XL_ORD_PRNT.ITM_PRC'), $lRaw, $aItmLst[$c]['sItem_Prc'], $sheet, '', '', 'right', False, '#0.00', False, 10, '', 10);
				Controller::SetCell(config('excel.XL_ORD_PRNT.ITM_QTY'), $lRaw, $aItmLst[$c]['nItm_Qty'], $sheet, '', '', 'right', False, '', False, 10, '', 10);
				Controller::SetCell(config('excel.XL_ORD_PRNT.TTL_PRC'), $lRaw, '='.Controller::GetColName(config('excel.XL_ORD_PRNT.ITM_PRC')).$lRaw.'*'.Controller::GetColName(config('excel.XL_ORD_PRNT.ITM_QTY')).$lRaw, $sheet, '', '', 'right', False, '#0.00', False, 10, '', 10);

				if($c==count($aItmLst)) 
				{
	        		break;	
	        	}
        		$c++;
				$lRaw = $lRaw + 1;	
			}
			$i++;
		}
	}
}
?>