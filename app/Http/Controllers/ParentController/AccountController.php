<?php
namespace App\Http\Controllers\ParentController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ParentAuth;
use Validator;
use App\Model\Parents;
use App\Model\Child;
use App\Model\School;
use App\Model\Country;
use App\Model\State;

class AccountController extends Controller
{
	public function __construct()
	{
		$this->Parents 	= new Parents;
		$this->Child 	= new Child;
		$this->School 	= new School;
		$this->Country 	= new Country;
		$this->State 	= new State;
		$this->middleware(ParentAuth::class);
	}

	public function IndexPage()
	{
		$lPrntIdNo 	= session('USER_ID');
		$aPrntsDtl 	= $this->Parents->PrntsDtl($lPrntIdNo);
		$aCntryLst	= $this->Country->FrntLst();
		$aSchlLst	= $this->School->SchlAll();
		$aChldLst	= $this->Child->ChldLst($lPrntIdNo);
		$aStateLst	= $this->State->FrntLst($aPrntsDtl['lCntry_IdNo']);
		$sTitle 	= "Manage Account";
    	$aData 		= compact('sTitle','aPrntsDtl','aCntryLst','aStateLst','aSchlLst','aChldLst');
        return view('parent_panel.manage_account',$aData);	
	}

	public function SaveCntrl(Request $request)
	{
		$lPrntIdNo = session('USER_ID');
		$rules = [
	        'sFrstName' 	=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sLstName' 		=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sMobileNo' 	=> 'required|unique:mst_prnts,sMobile_No,'.$lPrntIdNo.',lPrnt_IdNo|digits:9',
            'lRltnIdNo'		=> 'required',
            'lCntryIdNo'	=> 'required',
            'lStateIdNo'	=> 'required',
            'sSbrbName'		=> 'required|min:3|max:20|regex:/^[\pL\s]+$/u',
            'sPinCode'		=> 'required|digits:4',
            'nSchlType1'	=> 'required',
            'lSchlIdNo1'	=> 'required',
            'sFrstName1'	=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sLstName1'		=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sClsName1'		=> 'required',
	    ];

	    $this->validate($request, $rules, config('constant.VLDT_MSG'));
		try
		{
		    \DB::beginTransaction();
		    	$aHdArr 	= $this->HdArr($request);
		    	$nRow		= $this->Parents->UpDtRecrd($aHdArr, $lPrntIdNo);
		    	$aDelArr 	= $this->DelArr(); 
		    	$this->Child->DelRecrd($aDelArr, $lPrntIdNo);
		    	if((!empty($lPrntIdNo) && $lPrntIdNo > 0) || isset($nRow))
		    	{
		    		$i=1;
		    		for($i==1;$i<=$request['nTtlRec'];$i++)
		    		{
		    			if(!empty($request['nSchlType'.$i]) && !empty($request['lSchlIdNo'.$i]) && !empty($request['sFrstName'.$i]) && !empty($request['sLstName'.$i]) && !empty($request['sClsName'.$i]))
		    			{
		    				$aChldArr = $this->ChldArr($lPrntIdNo, $request['nSchlType'.$i], $request['lSchlIdNo'.$i], $request['sFrstName'.$i], $request['sLstName'.$i], $request['sClsName'.$i]);
		    				if(isset($request['lChldIdNo'.$i]) && !empty($request['lChldIdNo'.$i]))
		    				{
		    					$this->Child->UpDtRecrd($aChldArr, $request['lChldIdNo'.$i]);
		    				}
		    				else
		    				{
		    					$this->Child->InsrtRecrd($aChldArr);
		    				}
		    			}
		    		}
		    	}	
			\DB::commit();
		    return redirect()->back()->with('Success', 'Account updated successfully...');
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', $e->getMessage().' on Line '.$e->getLine());
		}
	}

	public function HdArr($request)
	{
		$aConArr = array(
			'sFrst_Name' 	=> $request['sFrstName'],
			'sLst_Name' 	=> $request['sLstName'],
            'sMobile_No' 	=> $request['sCntryCode']." ".$request['sMobileNo'],
            'lRltn_IdNo'	=> $request['lRltnIdNo'],
            'lCntry_IdNo'	=> $request['lCntryIdNo'],
            'lState_IdNo'	=> $request['lStateIdNo'],
            'sSbrb_Name'	=> $request['sSbrbName'],
            'sPin_Code'		=> $request['sPinCode'],
		);
		return $aConArr;
	}

	public function ChldArr($lPrntIdNo, $nSchlType, $lSchlIdNo, $sFrstName, $sLstName, $sClsName)
	{
		$aConArr = array(
			"sAcc_Id"		=> strtoupper($sFrstName[0].$sLstName[0]."-".rand(1111,9999)),
			"lPrnt_IdNo"	=> $lPrntIdNo,
			"nSchl_Type"	=> $nSchlType,
			"lSchl_IdNo"	=> $lSchlIdNo,
			"sFrst_Name"	=> $sFrstName,
			"sLst_Name"		=> $sLstName,
			"sCls_Name"		=> $sClsName,
			'nBlk_UnBlk'	=> config('constant.STATUS.UNBLOCK'),
			'nDel_Status' 	=> config('constant.DEL_STATUS.UNDELETED'),
		);
		return $aConArr;
	}

	public function DelArr()
	{
		$aDelArr = array(
			"nDel_Status"	=> config('constant.DEL_STATUS.DELETED'),
		);
		return $aDelArr;
	}

	public function PswrdPage()
	{
		$sTitle 	= "Change Password";
    	$aData 		= compact('sTitle');
        return view('parent_panel.manage_password',$aData);	
	}

	public function PswrdCntrl(Request $request)
	{
		$lPrntIdNo 	= session('USER_ID');
		$rules = [
	        'sCurrPass'		=> 'required|min:8|max:16|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
	        'sLgnPass'		=> 'required|min:8|max:16|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'sCnfrmPass'	=> 'required|required_with:sLgnPass|same:sLgnPass',
	    ];

	    $this->validate($request, $rules, config('constant.VLDT_MSG'));

	    try
		{
			$yPassExist = $this->Parents->IsPassExist($request['sCurrPass'], $lPrntIdNo);
			if(!$yPassExist)
			{
				return redirect()->back()->with('Failed', 'Current password did not matched...');
			}
			else
			{
			    $aPassArr 	= $this->PassArr($request['sCnfrmPass']);
	    		$nRow		= $this->Parents->UpDtRecrd($aPassArr, $lPrntIdNo);
	    		if($nRow > 0)
	    		{
	    			return redirect()->back()->with('Success', "Password updated successfully...");
	    		}
	    		else
	    		{
	    			return redirect()->back()->with('Alert', "No change found in password...");	
	    		}
	    	}
		}
		catch(\Exception $e)
		{
			return redirect()->back()->with('Failed', $e->getMessage().' on Line '.$e->getLine());
		}
	}

	public function PassArr($sUserPass)
	{
		$aPassArr = array(
			"sLgn_Pass"	=> md5($sUserPass),
		);
		return $aPassArr;
	}
}
?>