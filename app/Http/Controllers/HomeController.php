<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	
	public function IndexPage()
	{
		$sTitle = "Welcome";
    	$aData 	= compact('sTitle');
        return view('welcome',$aData);	
	}
}
?>