<?php

namespace App\Http\Controllers\CommonController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Country;
use App\Model\State;

class AjaxController extends Controller
{
	public function __construct()
	{
		$this->Country 			= new Country;
		$this->State 			= new State;
	}

	public function StateLst(Request $request)
	{
		$lCntryIdNo = base64_decode($request['lCntryIdNo']);
		$aStateLst	= $this->State->FrntLst($lCntryIdNo);
		return json_encode($aStateLst, JSON_PRETTY_PRINT);
	}
}
?>