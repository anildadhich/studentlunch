<?php
namespace App\Http\Controllers\CommonController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Parents;
use App\Model\MilkBar;
use App\Model\OrderHd;
use App\Model\Wallet;
use App\Model\OrderDetail;
use App\Model\School;
use DB;

class CommonController extends Controller
{
	public function __construct()
	{
		$this->MilkBar = new MilkBar;
		$this->Parents = new Parents;
		$this->OrderHd = new OrderHd;
		$this->Wallet = new Wallet;
		$this->OrderDetail = new OrderDetail;
		$this->School = new School;
	}

	public function ChngStatus(Request $request)
	{
		$lRecIdNo 	= base64_decode($request['lRecIdNo']);
		$sTblName 	= base64_decode($request['sTblName']);
		$sFldName 	= base64_decode($request['sFldName']);
		$nBlkUnBlk 	= base64_decode($request['nBlkUnBlk']);
		
		try
		{
			$IsRecExist = DB::table($sTblName)->Where($sFldName,$lRecIdNo)->Where('nDel_Status',config('constant.DEL_STATUS.UNDELETED'))->first();
			if(!isset($IsRecExist))
			{
				return redirect()->back()->with('Failed', 'Record not found...');
			}
			else
			{
				$aStatusArr = $this->StatusArr($nBlkUnBlk);
				$nRow = DB::table($sTblName)->Where($sFldName,$lRecIdNo)->update($aStatusArr);
				if($nRow > 0)
				{
					return redirect()->back()->with('Success', 'Status changed successfully...');		
				}
				else
				{
					return redirect()->back()->with('Alert', 'Status did not changed...');			
				}
			}
		}
		catch(\Exception $e)
		{
			return redirect()->back()->with('Failed', $e->getMessage());
		}
	}


	public function StatusArr($nBlkUnBlk)
	{
		$aConArr = array(
			'nBlk_UnBlk' 	=> $nBlkUnBlk,
		);
		return $aConArr;
	}

	public function DelRec(Request $request)
	{
		$lRecIdNo 	= base64_decode($request['lRecIdNo']);
		$sTblName 	= base64_decode($request['sTblName']);
		$sFldName 	= base64_decode($request['sFldName']);
		
		try
		{
			$IsRecExist = DB::table($sTblName)->Where($sFldName,$lRecIdNo)->Where('nDel_Status',config('constant.DEL_STATUS.UNDELETED'))->first();
			if(!isset($IsRecExist))
			{
				return redirect()->back()->with('Failed', 'Record not found...');
			}
			else
			{
				$DelArr = $this->DelArr();
				$nRow = DB::table($sTblName)->Where($sFldName,$lRecIdNo)->update($DelArr);
				if($nRow > 0)
				{
					return redirect()->back()->with('Success', 'Record deleted successfully...');		
				}
				else
				{
					return redirect()->back()->with('Alert', 'Record did not deleted...');			
				}
			}
		}
		catch(\Exception $e)
		{
			return redirect()->back()->with('Failed', $e->getMessage());
		}
	}

	public function DelArr()
	{
		$aConArr = array(
			'nDel_Status' 	=> config('constant.DEL_STATUS.DELETED'),
		);
		return $aConArr;
	}

	public function EmailVrfy(Request $request)
	{
		$sEmailId 	= base64_decode($request['sEmailId']);
		$lRecIdNo 	= base64_decode($request['lRecIdNo']);
		$nUserType 	= base64_decode($request['nUserType']);
		if(!empty($sEmailId) && !empty($lRecIdNo) && !empty($nUserType))
		{
			try
			{
				$aAdmnArr = $this->AdmnArr();
				\DB::beginTransaction();
					if($nUserType == config('constant.USER.PARENT'))
					{
						$yEmailExst = $this->Parents->IsEmailExist($sEmailId, $lRecIdNo);
						if(!$yEmailExst)
						{
							return redirect('user/login')->with('Failed', 'Unauthorized Access 1...');
						}
						else
						{
							$nRow = $this->Parents->UpDtRecrd($aAdmnArr, $lRecIdNo);
            				$aGetPrnt   = $this->Parents->ShrtDtl($sEmailId);
							$aEmailData = ['sUserName' => $aGetPrnt['sFrst_Name']];
            				Controller::SendEmail($aGetPrnt['sEmail_Id'], $aGetPrnt['sFrst_Name'], 'parents_welcome_email', 'Welcome to MyLunchOrder.Online', $aEmailData);
						}
					}
					else if($nUserType == config('constant.USER.MILK_BAR'))
					{
						$yEmailExst = $this->MilkBar->IsEmailExist($sEmailId, $lRecIdNo);
						if(!$yEmailExst)
						{
							return redirect('user/login')->with('Failed', 'Unauthorized Access 2...');
						}
						else
						{
							$nRow = $this->MilkBar->UpDtRecrd($aAdmnArr, $lRecIdNo);
							$aGetMilk   = $this->MilkBar->ShrtDtl($sEmailId);
							$aEmailData = ['sUserName' => $aGetMilk['sFrst_Name']];
            				Controller::SendEmail($aGetMilk['sEmail_Id'], $aGetMilk['sFrst_Name'], 'milkbar_welcome_email', 'Welcome to MyLunchOrder.Online', $aEmailData);
						}
					}
					else
					{
						return redirect('user/login')->with('Failed', 'Unauthorized Access 3...');
					}
				\DB::commit();
			}
			catch(\Exception $e)
			{
				return redirect('user/login')->with('Failed', $e->getMessage().' on Line '.$e->getLine());
			}
			return redirect('user/login')->with('Success', 'Email verified, Login Please...');
		}
		else
		{
			return redirect('user/login')->with('Failed', 'Unauthorized Access 4...');
		}
	}

	public function AdmnArr()
	{
		$aComnArr = array(
			"nEmail_Status"	=> config('constant.MAIL_STATUS.VERIFIED'),
		);
		return $aComnArr;
	}
	
	public function OrderDtl(Request $request)
	{
		$oOrdrDtl = $this->OrderHd->GetOrderDtl(base64_decode($request['lOrdrHdIdNo']));
		$OrdrAmnt = $this->Wallet->OrdrIdGet($oOrdrDtl->lOrder_IdNo);
		$oOrdrItms = $this->OrderDetail->ItemLst($oOrdrDtl->lOrder_IdNo);
		$aSchlDtl = $this->School->SchlDtl($oOrdrDtl->lSchl_IdNo);
		
		return json_encode(['oOrdrDtl' => $oOrdrDtl, 'aSchlDtl' => $aSchlDtl, 'oOrdrItms' => $oOrdrItms, 'OrdrAmnt' => $OrdrAmnt]);
	}
}
?>