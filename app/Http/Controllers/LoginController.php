<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MilkBar;
use App\Model\Parents;
use Session;

class LoginController extends Controller
{	
	public function __construct()
	{
		$this->MilkBar = new MilkBar;
		$this->Parents = new Parents;
	}
	
	public function IndexPage()
	{
		$sTitle = "Login Panel";
    	$aData 	= compact('sTitle');
		if(Session::has('user')){
			return view('login',$aData);
		}
        return view('login',$aData);	
	}
	
	public function Login(Request $request)
	{
		$rules = [
	        'sEmailId' 		=> 'required|max:50|regex:^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^',
            'sLgnPass'		=> 'required|min:8|max:16|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
	    ];	
		
		$this->validate($request, $rules, config('constant.VLDT_MSG'));
		
		$IsMlkEmailExst = $this->MilkBar->IsMailExist($request['sEmailId']);
		$IsPrntEmailExst = $this->Parents->IsMailExist($request['sEmailId']);
		if($IsMlkEmailExst || $IsPrntEmailExst)
		{
			$yMilkExist = $this->MilkBar->IsUserExist($request['sEmailId'], $request['sLgnPass'], $aMilkDtl);
			
			if(!$yMilkExist)
			{
				$yPrntExist = $this->Parents->IsUserExist($request['sEmailId'], $request['sLgnPass'], $aParentsDtl);
				if(!$yPrntExist)
				{
					return redirect()->back()->with('Failed', 'Invalid password. Try again or click on Forgot password to reset it');
				}
				else
				{
					if($aParentsDtl['nEmail_Status'] == config('constant.MAIL_STATUS.VERIFIED'))
					{
						session(['USER_ID' => $aParentsDtl['lPrnt_IdNo']]);
						session(['USER_TYPE' => 'P']);
						session(['USER_NAME' => $aParentsDtl['sFrst_Name']." ".$aParentsDtl['sLst_Name']]);
						return redirect('parent_panel');
					}
					else
					{
						return redirect()->back()->with('Failed', 'Email verifaction is pending...');		
					}
				}
			}
			else 
			{
				if($aMilkDtl['nEmail_Status'] == config('constant.MAIL_STATUS.VERIFIED'))
				{
					session(['USER_ID' => $aMilkDtl['lMilk_IdNo']]);
					session(['USER_TYPE' => 'M']);
					session(['USER_NAME' => $aMilkDtl['sFrst_Name']." ".$aMilkDtl['sLst_Name']]);
					return redirect('milkbar_panel');
				}
				else
				{
					return redirect()->back()->with('Failed', 'Email verifaction is pending...');		
				}
			}
		}
		else
		{
			return redirect()->back()->with('Failed', 'Couldn’t find your Account');	
		}
	}

	public function Logout()
	{
		session()->flush();
		return redirect("");
	}
}
?>