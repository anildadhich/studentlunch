<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Helper;
use App\Model\Parents;
use App\Model\Child;
use App\Model\School;
use App\Model\Country;

class ParentController extends Controller
{
	public function __construct()
	{
		$this->Parents 	= new Parents;
		$this->Child 	= new Child;
		$this->School 	= new School;
		$this->Country 	= new Country;
		//$this->Helper 	= new Helper;
	}

	public function IndexPage()
	{
		
		$oSchlLst	= $this->School->SchlLst();
		$aCntryLst	= $this->Country->FrntLst();
		$sTitle 	= "Parent Registreation";
    	$aData 		= compact('sTitle','oSchlLst','aCntryLst');
        return view('parent_register',$aData);	
	}

	public function SaveCntrl(Request $request)
	{
		$rules = [
	        'sFrstName' 	=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sLstName' 		=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sEmailId' 		=> 'required|unique:mst_prnts,sEmail_Id|max:50|regex:^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^',
            'sMobileNo' 	=> 'required|unique:mst_prnts,sMobile_No|digits:9',
            'lRltnIdNo'		=> 'required',
            'lCntryIdNo'	=> 'required',
            'lStateIdNo'	=> 'required',
            'sSbrbName'		=> 'required|min:3|max:20|regex:/^[\pL\s]+$/u',
            'sPinCode'		=> 'required|digits:4',
            'sLgnPass'		=> 'required|min:8|max:16|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'sCnfrmPass'	=> 'required|required_with:sLgnPass|same:sLgnPass',
            'nSchlType1'	=> 'required',
            'lSchlIdNo1'	=> 'required',
            'sFrstName1'	=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sLstName1'		=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sClsName1'		=> 'required',
            'nTerms'		=> 'accepted',
	    ];

	    $this->validate($request, $rules, config('constant.VLDT_MSG'));

		try
		{
		    $aHdArr 	= $this->HdArr($request);
		    \DB::beginTransaction();
		    	$lPrntIdNo	= $this->Parents->InsrtRecrd($aHdArr);
		    	if(!empty($lPrntIdNo) && $lPrntIdNo > 0)
		    	{
		    		$i=1;
		    		for($i==1;$i<=$request['nTtlRec'];$i++)
		    		{
		    			if(!empty($request['nSchlType'.$i]) && !empty($request['lSchlIdNo'.$i]) && !empty($request['sFrstName'.$i]) && !empty($request['sLstName'.$i]) && !empty($request['sClsName'.$i]))
		    			{
		    				$aChldArr = $this->ChldArr($lPrntIdNo, $request['nSchlType'.$i], $request['lSchlIdNo'.$i], $request['sFrstName'.$i], $request['sLstName'.$i], $request['sClsName'.$i]);
		    				$this->Child->InsrtRecrd($aChldArr);
		    			}
		    		}
		    	}	
			\DB::commit();
			$aEmailData = ['sUserName' => $request['sFrstName'], 'sEmailId' => $request['sEmailId'], 'lRecIdNo' => $lPrntIdNo, 'nUserType' => config('constant.USER.PARENT')];
            Controller::SendEmail($request['sEmailId'], $request['sFrstName'], 'verification_email', 'Email Verification', $aEmailData);
		    return redirect()->back()->with('Success', 'Your account created successfully...');
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', 'We have some technicial issue, please try again...');
		}
	}

	public function HdArr($request)
	{
		$aConArr = array(
			'sAcc_Id' 		=> strtoupper($request['sFrstName'][0].$request['sLstName'][0]."-".rand(1111,9999)),
			'sFrst_Name' 	=> $request['sFrstName'],
            'sLst_Name' 	=> $request['sLstName'],
            'sEmail_Id' 	=> $request['sEmailId'],
            'sMobile_No' 	=> $request['sCntryCode']." ".$request['sMobileNo'],
            'lRltn_IdNo'	=> $request['lRltnIdNo'],
            'lCntry_IdNo'	=> $request['lCntryIdNo'],
            'lState_IdNo'	=> $request['lStateIdNo'],
            'sSbrb_Name'	=> $request['sSbrbName'],
            'sPin_Code'		=> $request['sPinCode'],
            'sLgn_Pass'		=> md5($request['sLgnPass']),
            'nBlk_UnBlk'	=> config('constant.STATUS.UNBLOCK'),
            'nDel_Status'	=> config('constant.DEL_STATUS.UNDELETED'),
            'nEmail_Status'	=> config('constant.MAIL_STATUS.UNVERIFIED'),
		);
		return $aConArr;
	}

	public function ChldArr($lPrntIdNo, $nSchlType, $lSchlIdNo, $sFrstName, $sLstName, $sClsName)
	{
		$aConArr = array(
			"sAcc_Id"		=> strtoupper($sFrstName[0].$sLstName[0]."-".rand(1111,9999)),
			"lPrnt_IdNo"	=> $lPrntIdNo,
			"nSchl_Type"	=> $nSchlType,
			"lSchl_IdNo"	=> $lSchlIdNo,
			"sFrst_Name"	=> $sFrstName,
			"sLst_Name"		=> $sLstName,
			"sCls_Name"		=> $sClsName,
			'nBlk_UnBlk'	=> config('constant.STATUS.UNBLOCK'),
			'nDel_Status'	=> config('constant.DEL_STATUS.UNDELETED'),
		);
		return $aConArr;
	}
}
?>