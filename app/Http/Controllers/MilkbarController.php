<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Model\MilkBar;
use App\Model\School;
use App\Model\AssociateSchool;
use App\Model\Country;

class MilkbarController extends Controller
{
	public function __construct()
	{
		$this->MilkBar 			= new MilkBar;
		$this->School 			= new School;
		$this->AssociateSchool 	= new AssociateSchool;
		$this->Country 			= new Country;
	}

	public function IndexPage()
	{
		$aSchlLst	= $this->School->SchlLst();
		$aCntryLst	= $this->Country->FrntLst();
		$sTitle 	= "Milk Bar Registreation";
    	$aData 		= compact('sTitle','aSchlLst','aCntryLst');
        return view('milk_bar_register',$aData);	
	}

	public function SaveCntrl(Request $request)
	{
		$rules = [
	        'sBussName' 	=> 'required|min:5|max:50|regex:/^[\pL\s]+$/u',
            'nBussType' 	=> 'required',
            'sAbnNo' 		=> 'required|unique:mst_milk_bar,sAbn_No|digits:11',
            'sFrstName' 	=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sLstName' 		=> 'required|min:2|max:15|regex:/^[\pL\s]+$/u',
            'sMobileNo' 	=> 'required|unique:mst_milk_bar,sMobile_No|digits:9',
            'sAreaCode' 	=> 'required|digits:1',
            'sPhoneNo' 		=> 'required|unique:mst_milk_bar,sPhone_No|digits:8',
            'sEmailId' 		=> 'required|unique:mst_milk_bar,sEmail_Id|max:50|regex:^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^',
            'sStrtNo'		=> 'required|max:5',
            'lCntryIdNo'	=> 'required',
            'lStateIdNo'	=> 'required',
            'sStrtName'		=> 'required|min:5|max:50|regex:/^[\pL\s]+$/u',
            'sSbrbName'		=> 'required|min:3|max:20|regex:/^[\pL\s]+$/u',
            'sPinCode'		=> 'required|digits:4',
            'nSchlType1'	=> 'required',
            'lSchlIdNo1'	=> 'required',
            'dDistKm1'		=> 'required|between:0,7',
            'sSbrbName1'	=> 'required|min:3|max:20|regex:/^[\pL\s]+$/u',
            'sPinCode1'		=> 'required|digits:4',
            'sCutTm1'		=> 'required',
            'sLgnPass'		=> 'required|min:8|max:16|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'sCnfrmPass'	=> 'required|required_with:sLgnPass|same:sLgnPass',
            'nTerms'		=> 'accepted',
	    ];

	    $this->validate($request, $rules, config('constant.VLDT_MSG'));

	    try
		{
		    $aHdArr 	= $this->HdArr($request);
		    \DB::beginTransaction();
		    	$lMilkIdNo	= $this->MilkBar->InsrtRecrd($aHdArr);
		    	if(!empty($lMilkIdNo) && $lMilkIdNo > 0)
		    	{
		    		$i=1;
		    		for($i==1;$i<=$request['nTtlRec'];$i++)
		    		{
		    			if(!empty($request['nSchlType'.$i]) && !empty($request['lSchlIdNo'.$i]) && !empty($request['dDistKm'.$i]) && !empty($request['sSbrbName'.$i]) && !empty($request['sPinCode'.$i]) && !empty($request['sCutTm'.$i]))
		    			{
		    				$aSchlArr = $this->SchlArr($lMilkIdNo, $request['nSchlType'.$i], $request['lSchlIdNo'.$i], $request['dDistKm'.$i], $request['sSbrbName'.$i], $request['sPinCode'.$i], $request['sCutTm'.$i]);
		    				$this->AssociateSchool->InsrtRecrd($aSchlArr);
		    			}
		    		}
		    	}	
			\DB::commit();
			$aEmailData = ['sUserName' => $request['sFrstName'], 'sEmailId' => $request['sEmailId'], 'lRecIdNo' => $lMilkIdNo, 'nUserType' => config('constant.USER.MILK_BAR')];
            Controller::SendEmail($request['sEmailId'], $request['sFrstName'], 'verification_email', 'Email Verification', $aEmailData);
		    return redirect()->back()->with('Success', 'Your account created successfully...');
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect()->back()->with('Failed', $e->getMessage());
		}
	}

	public function HdArr($request)
	{
		$aConArr = array(
			'sAcc_Id' 		=> strtoupper(substr($request['sBussName'], 0, 2)."-".rand(1111,9999)),
			'sBuss_Name' 	=> $request['sBussName'],
            'nBuss_Type' 	=> $request['nBussType'],
            'sAbn_No' 		=> $request['sAbnNo'],
            'sFrst_Name' 	=> $request['sFrstName'],
            'sLst_Name'		=> $request['sLstName'],
            'sPhone_No'		=> $request['sCntryCode']." ".$request['sAreaCode']." ".$request['sPhoneNo'],
            'sMobile_No'	=> $request['sCntryCode']." ".$request['sMobileNo'],
            'sEmail_Id'		=> $request['sEmailId'],
            'sStrt_No'		=> $request['sStrtNo'],
            'sStrt_Name'	=> $request['sStrtName'],
            'sSbrb_Name'	=> $request['sSbrbName'],
            'lCntry_IdNo'	=> $request['lCntryIdNo'],
            'lState_IdNo'	=> $request['lStateIdNo'],
            'sPin_Code'		=> $request['sPinCode'],
            'sLgn_Pass'		=> md5($request['sLgnPass']),
            'nBlk_UnBlk'	=> config('constant.STATUS.UNBLOCK'),
            'nDel_Status'	=> config('constant.DEL_STATUS.UNDELETED'),
            'nAdmin_Status'	=> config('constant.MLK_STATUS.UNACTIVE'),
            'nEmail_Status'	=> config('constant.MAIL_STATUS.UNVERIFIED'),
		);
		return $aConArr;
	}

	public function SchlArr($lMilkIdNo, $nSchlType, $lSchlIdNo, $dDistKm, $sSbrbName, $sPinCode, $sCutTm)
	{
		$aConArr = array(
			"lMilk_IdNo"	=> $lMilkIdNo,
			"nSchl_Type"	=> $nSchlType,
			"lSchl_IdNo"	=> $lSchlIdNo,
			"dDist_Km"		=> $dDistKm,
			"sSbrb_Name"	=> $sSbrbName,
			"sPin_Code"		=> $sPinCode,
			"sCut_Tm"		=> $sCutTm,
			'nBlk_UnBlk'	=> config('constant.STATUS.UNBLOCK'),
			'nDel_Status'	=> config('constant.DEL_STATUS.UNDELETED'),
		);
		return $aConArr;
	}
}
?>