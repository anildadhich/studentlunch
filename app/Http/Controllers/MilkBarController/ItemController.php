<?php
namespace App\Http\Controllers\MilkbarController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MilkBarAuth;
use App\Model\Category;
use App\Model\Item;
use Excel;

class ItemController extends Controller
{
	public function __construct()
	{
		$this->Category 	= new Category;
		$this->Item 		= new Item;
		$this->middleware(MilkBarAuth::class);
	}

	public function ListPage(Request $request)
	{
		$lMilkIdNo 	= session('USER_ID');
		$sItemName  = $request['sItemName'];
		$lCatgIdNo 	= $request['lCatgIdNo'];
		$aCatgLst	= $this->Category->CatgLstFlt($lMilkIdNo);
		$aItemLst	= $this->Item->ItemLst($lMilkIdNo, $lCatgIdNo, $sItemName);
		$sTitle 	= "Manage Item";
    	$aData 		= compact('sTitle','aCatgLst','aItemLst','request');
        return view('milkbar_panel.item_list',$aData);	
	}

	public function SaveCntrl(Request $request)
	{
		$lItemIdNo 	= base64_decode($request['lItemIdNo']);
		$rules = [
	        'sItemName' 	=> 'required|max:30|regex:/^[\pL\s]+$/u',
	        'sItemDscrptn' 	=> 'required|max:150|regex:/^[\pL\s]+$/u',
	        'sItemPrc' 		=> 'required',
	        'aItemWeek' 	=> 'required',
	    ];

	    $this->validate($request, $rules, config('constant.VLDT_MSG'));
	    try
		{
		    $aHdArr 	= $this->HdArr($request);
		    \DB::beginTransaction();
		    	if($lItemIdNo == 0)
		    	{
		    		$this->InsrtArr($aHdArr);
		    		$nRow		= $this->Item->InsrtRecrd($aHdArr);
		    		$sMessage	= "Item created successfully...";
		    	}
		    	else
		    	{
		    		$nRow		= $this->Item->UpDtRecrd($aHdArr, $lItemIdNo);
		    		$sMessage	= "Item updated successfully...";
		    	}
			\DB::commit();
		    return redirect('milkbar_panel/item/list')->with('Success', $sMessage);
		}
		catch(\Exception $e)
		{
			\DB::rollback();
			return redirect('milkbar_panel/item/list')->with('Failed', $e->getMessage().' on Line '.$e->getLine());
		}
	}

	public function HdArr($request)
	{
		$aConArr = array(
			'sItem_Name' 	=> ucfirst($request['sItemName']),
			'sItem_Dscrptn' => $request['sItemDscrptn'],
			'lCatg_IdNo' 	=> $request['lCatgIdNo'],
			'sItem_Prc' 	=> $request['sItemPrc'],
			'aItem_Week' 	=> implode(',', $request['aItemWeek']),
		);
		return $aConArr;
	}

	public function InsrtArr(&$aHdArr)
	{
		$aHdArr['lItem_Unq_Id']	= rand(1001,9999);
		$aHdArr['lMilk_IdNo']	= session('USER_ID');
		$aHdArr['nBlk_UnBlk']	= config('constant.STATUS.UNBLOCK');
		$aHdArr['nDel_Status']	= config('constant.DEL_STATUS.UNDELETED');
	}

	public function ExprtRcrd(Request $request)
	{
		$lMilkIdNo 	= session('USER_ID');
		$sItemName  = $request['sItemName'];
		$lCatgIdNo 	= $request['lCatgIdNo'];
		$oItmLst	= $this->Item->ExlRcrd($lMilkIdNo, $lCatgIdNo, $sItemName);
		if(count($oItmLst) > 0)
		{
			$FileName 	= 'Item_'.date('Ymd').'_'.date('His');
	        Excel::create($FileName, function($excel) use ($oItmLst) {
	            $excel->sheet('Sheet1', function($sheet)  use ($oItmLst) {
	                $this->SetExlHeader($sheet, $lRaw);
	                $this->SetExlData($sheet, $lRaw, $oItmLst);
	            });
	        })->download('xlsx');
	    }
	    else
	    {
	    	return redirect()->back()->with('Failed', 'Record not found...');
	    }
	}

	public function SetExlHeader($sheet, &$lRaw)
	{
		$lRaw = 1;
		Controller::SetCell(config('excel.XL_ITEM.SR_NO'), $lRaw, 'Sr. No', $sheet, '', '#F2DDDC', 'right', True, '', False, 8, '', 10);
		Controller::SetCell(config('excel.XL_ITEM.ITEM_ID'), $lRaw, 'Unique Id', $sheet, '', '#F2DDDC', 'right', True, '', False, 10, '', 10);
		Controller::SetCell(config('excel.XL_ITEM.CATG_NAME'), $lRaw, 'Category Name', $sheet, '', '#F2DDDC', 'left', True, '', False, 20, '', 10);
		Controller::SetCell(config('excel.XL_ITEM.ITEM_NAME'), $lRaw, 'Item Name', $sheet, '', '#F2DDDC', 'left', True, '', False, 20, '', 10);
		Controller::SetCell(config('excel.XL_ITEM.ITEM_PRC'), $lRaw, 'Price', $sheet, '', '#F2DDDC', 'right', True, '', False, 8, '', 10);
		Controller::SetCell(config('excel.XL_ITEM.ITEM_DESC'), $lRaw, 'Description', $sheet, '', '#F2DDDC', 'left', True, '', True, 35, '', 10);
		Controller::SetCell(config('excel.XL_ITEM.ITEM_DAY'), $lRaw, 'Availability Day', $sheet, '', '#F2DDDC', 'left', True, '', False, 40, '', 10);
		Controller::SetCell(config('excel.XL_ITEM.ITEM_STATUS'), $lRaw, 'Status', $sheet, '', '#F2DDDC', 'left', True, '', False, 10, '', 10);
	}

	public function SetExlData($sheet, $lRaw, $oItmLst)
	{
		$i = 0;
		while(isset($oItmLst) && count($oItmLst) > 0 && $i<count($oItmLst))
		{
			$sWeakName = '';
			$nWeakDay = explode(",", $oItmLst[$i]['aItem_Week']);
			foreach ($nWeakDay as $nWkKey) 
			{
				$sWeakName .= array_search($nWkKey, config('constant.WEEK')).", ";
			}
			$lRaw = $lRaw + 1;

			Controller::SetCell(config('excel.XL_ITEM.SR_NO'), $lRaw, $i+1, $sheet, '', '', 'right', False, '', False, 8, '', 10);
			Controller::SetCell(config('excel.XL_ITEM.ITEM_ID'), $lRaw, $oItmLst[$i]['lItem_Unq_Id'], $sheet, '', '', 'right', False, '', False, 10, '', 10);
			Controller::SetCell(config('excel.XL_ITEM.CATG_NAME'), $lRaw, $oItmLst[$i]['sCatg_Name'], $sheet, '', '', 'left', False, '', False, 20, '', 10);
			Controller::SetCell(config('excel.XL_ITEM.ITEM_NAME'), $lRaw, $oItmLst[$i]['sItem_Name'], $sheet, '', '', 'left', False, '', False, 20, '', 10);
			Controller::SetCell(config('excel.XL_ITEM.ITEM_PRC'), $lRaw, $oItmLst[$i]['sItem_Prc'], $sheet, '', '', 'right', False, '#00.00', False, 8, '', 10);
			Controller::SetCell(config('excel.XL_ITEM.ITEM_DESC'), $lRaw, $oItmLst[$i]['sItem_Dscrptn'], $sheet, '', '', 'left', False, '', True, 35, '', 10);
			Controller::SetCell(config('excel.XL_ITEM.ITEM_DAY'), $lRaw, substr($sWeakName, 0, -2), $sheet, '', '', 'left', False, '', True, 40, '', 10);
			Controller::SetCell(config('excel.XL_ITEM.ITEM_STATUS'), $lRaw, $oItmLst[$i]['nBlk_UnBlk'] == config('constant.STATUS.BLOCK') ? 'Block' : 'Unblock', $sheet, '', '', 'left', False, '', False, 10, '', 10);

			$i++;
		}
	}
}
?>