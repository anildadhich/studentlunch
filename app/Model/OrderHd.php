<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class OrderHd extends Model
{
    public $timestamps  = false;
    protected $table    = 'mst_ordr_hd';

    public function InsrtRecrd($aHdArr)
    {
    	try
    	{
	        $lOrderId	= OrderHd::insertGetId($aHdArr);
			return $lOrderId;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }
    
    public function UpDtRecrd($aHdArr, $lOrderIdNo)
    {
    	try
    	{
	        $nRow	= OrderHd::Where('lOrder_IdNo',$lOrderIdNo)->update($aHdArr);
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function OrderLst($lPrntIdNo, $sCrtDtTm = '', $nOrdrStatus = '')
    {
    	try
    	{
	        $oOrderLst	= OrderHd::Select('mst_ordr_hd.*','mst_chld.sFrst_Name','mst_chld.sLst_Name','mst_milk_bar.sBuss_Name')->leftjoin('mst_chld', 'mst_chld.lChld_IdNo', '=', 'mst_ordr_hd.lChld_IdNo')
						->leftjoin('mst_milk_bar', 'mst_milk_bar.lMilk_IdNo', '=', 'mst_ordr_hd.lMilk_IdNo')
	        			->Where(function($query) use ($sCrtDtTm, $nOrdrStatus) {
                            if (isset($sCrtDtTm) && !empty($sCrtDtTm)) {
                                $query->where('mst_ordr_hd.sCrt_DtTm', 'LIKE', $sCrtDtTm."%");
                            }
                            if (isset($nOrdrStatus)) {
                                $query->where('mst_ordr_hd.nOrdr_Status',$nOrdrStatus);
                            }
                        })->where('mst_ordr_hd.lPrnt_IdNo', $lPrntIdNo)->paginate(15);
	        return $oOrderLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }

    public function Cancel($lOrdrHdIdNo)
    {
    	try
    	{
	        OrderHd::where(['lOrder_IdNo' => $lOrdrHdIdNo])
			->update(['nOrdr_Status' => config('constant.ORDER_STATUS.Cancelled')]);
			$row = OrderHd::where(['lOrder_IdNo' => $lOrdrHdIdNo])->first();
			
	        return $row;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }
	
	
	public function GetOrder($lOrdrHdIdNo)
    {
    	try
    	{
			$oRow = OrderHd::where(['lOrder_IdNo' => $lOrdrHdIdNo])->first();
			
	        return $oRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }
	
	public function UpdateTrans($lOrdrHdIdNo, $sTrnsctnId)
	{
		try
    	{
			$nRow = OrderHd::where(['lOrder_IdNo' => $lOrdrHdIdNo])->update(['sStrp_Trnf_Id' => $sTrnsctnId]);
			
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
	}
	
	public function CntOrd($lMilkIdNo, $nOrdrStatus = '')
    {
    	try
    	{
	        $aCntOrd = OrderHd::Select(DB::raw('COUNT(*) As nTtlRec'))->Where('lMilk_IdNo',$lMilkIdNo)
	        			->Where(function($query) use ($nOrdrStatus) {
                            if (isset($nOrdrStatus) && !empty($nOrdrStatus)) {
                                $query->where('nOrdr_Status', '=',$nOrdrStatus);
                            }
                        })->first()->toArray();
	        return $aCntOrd;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }

    public function CntOrdPrnt($lPrntIdNo, $nOrdrStatus = '')
    {
    	try
    	{
	        $aCntOrd = OrderHd::Select(DB::raw('COUNT(*) As nTtlRec'))->Where('lPrnt_IdNo',$lPrntIdNo)
	        			->Where(function($query) use ($nOrdrStatus) {
                            if (isset($nOrdrStatus) && !empty($nOrdrStatus)) {
                                $query->where('nOrdr_Status', '=',$nOrdrStatus);
                            }
                        })->first()->toArray();
	        return $aCntOrd;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
    
    public function MilkOrdLst($lMilkIdNo, $sDelvDate = '', $lSchlIdNo = '', $nOrdrStatus = '')
    {
    	try
    	{
	        $oOrderLst	= OrderHd::Select('lOrder_IdNo','sOrdr_Id','sDelv_Date','sGrnd_Ttl','nOrdr_Status','mst_chld.sFrst_Name','mst_chld.sLst_Name','sCls_Name','sSchl_Name')->leftjoin('mst_chld', 'mst_chld.lChld_IdNo', '=', 'mst_ordr_hd.lChld_IdNo')
						->leftjoin('mst_schl', 'mst_schl.lSchl_IdNo', '=', 'mst_ordr_hd.lSchl_IdNo')
	        			->Where(function($query) use ($sDelvDate, $lSchlIdNo, $nOrdrStatus) {
                            if (isset($sDelvDate) && !empty($sDelvDate)) {
                                $query->where('sDelv_Date',$sDelvDate);
                            }
                            if (isset($lSchlIdNo) && !empty($lSchlIdNo)) {
                                $query->where('mst_ordr_hd.lSchl_IdNo',$lSchlIdNo);
                            }
                            if (isset($nOrdrStatus) && !empty($nOrdrStatus)) {
                                $query->where('nOrdr_Status',$nOrdrStatus);
                            }
                        })->where('mst_ordr_hd.lMilk_IdNo', $lMilkIdNo)->paginate(15);
	        return $oOrderLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }

    public function ExlRcrd($lMilkIdNo, $sDelvDate = '', $lSchlIdNo = '', $nOrdrStatus = '')
    {
    	try
    	{
	        $oOrderLst	= OrderHd::Select('lOrder_IdNo','sOrdr_Id','sDelv_Date','sSub_Ttl','sGst_Amo','sGrnd_Ttl','nOrdr_Status','mst_chld.sFrst_Name as sChld_FName','mst_chld.sLst_Name as sChld_LName', 'mst_prnts.sFrst_Name as sPrnt_FName','mst_prnts.sLst_Name as sPrnt_LName', 'sCls_Name','sSchl_Name')->leftjoin('mst_chld', 'mst_chld.lChld_IdNo', '=', 'mst_ordr_hd.lChld_IdNo')
						->leftjoin('mst_schl', 'mst_schl.lSchl_IdNo', '=', 'mst_ordr_hd.lSchl_IdNo')
                        ->leftjoin('mst_prnts', 'mst_prnts.lPrnt_IdNo', '=', 'mst_ordr_hd.lPrnt_IdNo')
	        			->Where(function($query) use ($sDelvDate, $lSchlIdNo, $nOrdrStatus) {
                            if (isset($sDelvDate) && !empty($sDelvDate)) {
                                $query->where('sDelv_Date',$sDelvDate);
                            }
                            if (isset($lSchlIdNo) && !empty($lSchlIdNo)) {
                                $query->where('mst_ordr_hd.lSchl_IdNo',$lSchlIdNo);
                            }
                            if (isset($nOrdrStatus) && !empty($nOrdrStatus)) {
                                $query->where('nOrdr_Status',$nOrdrStatus);
                            }
                        })->where('mst_ordr_hd.lMilk_IdNo', $lMilkIdNo)->OrderBy('sDelv_Date')->get()->toArray();
	        return $oOrderLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
    
    public function ExlRcrdPrnt($lPrntIdNo, $sCrtDtTm = '', $nOrdrStatus = '')
    {
    	try
    	{
	        $oOrderLst	= OrderHd::Select('lOrder_IdNo','sOrdr_Id','sDelv_Date','sSub_Ttl','sGst_Amo','sGrnd_Ttl','nOrdr_Status','mst_ordr_hd.sCrt_DtTm','mst_chld.sFrst_Name as sChld_FName','mst_chld.sLst_Name as sChld_LName','sCls_Name','sBuss_Name')->leftjoin('mst_chld', 'mst_chld.lChld_IdNo', '=', 'mst_ordr_hd.lChld_IdNo')
                        ->leftjoin('mst_milk_bar', 'mst_milk_bar.lMilk_IdNo', '=', 'mst_ordr_hd.lMilk_IdNo')
	        			->Where(function($query) use ($sCrtDtTm, $nOrdrStatus) {
                            if (isset($sCrtDtTm) && !empty($sCrtDtTm)) {
                                $query->where('mst_ordr_hd.sCrt_DtTm', 'LIKE', $sCrtDtTm."%");
                            }
                            if (isset($nOrdrStatus) && !empty($nOrdrStatus)) {
                                $query->where('nOrdr_Status',$nOrdrStatus);
                            }
                        })->where('mst_ordr_hd.lPrnt_IdNo', $lPrntIdNo)->OrderBy('sDelv_Date')->get()->toArray();;
	        return $oOrderLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }

    public function AccMilkLst($lPrntIdNo)
    {
        try
        {
            $aMilkLst = OrderHd::Select('mst_ordr_hd.lMilk_IdNo','mst_milk_bar.sBuss_Name')->leftjoin('mst_milk_bar', 'mst_milk_bar.lMilk_IdNo', '=', 'mst_ordr_hd.lMilk_IdNo')->Where('mst_ordr_hd.lPrnt_IdNo',$lPrntIdNo)->groupBy('mst_ordr_hd.lMilk_IdNo','mst_milk_bar.sBuss_Name')->get()->toArray();
            return $aMilkLst;
        }
        catch(\Expection $e)
        {
            return $e->getMessage();
        }
    }

    public function GetOrderDtl($lOrdrHdIdNo)
    {
    	try
    	{
			$oRow = OrderHd::select('mst_ordr_hd.*', 'mst_chld.sFrst_Name as sChld_Fname', 'mst_chld.sLst_Name as sChld_Lname', 'mst_milk_bar.*', 'mst_cntry.sCntry_Name', 'mst_state.sState_Name')
			->where(['mst_ordr_hd.sOrdr_Id' => $lOrdrHdIdNo])
			->leftjoin('mst_milk_bar','mst_ordr_hd.lMilk_IdNo', '=', 'mst_milk_bar.lMilk_IdNo')
			->leftjoin('mst_chld','mst_ordr_hd.lChld_IdNo', '=', 'mst_chld.lChld_IdNo')
			->leftjoin('mst_state','mst_state.lState_IdNo','=','mst_milk_bar.lState_IdNo')
			->leftjoin('mst_cntry','mst_cntry.lCntry_IdNo','=','mst_milk_bar.lCntry_IdNo')
			->first();
			
	        return $oRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }
    
    public function AdmnOrdLst($sFrmDate = '', $sToDate = '', $sPrntName = '', $lMilkIdNo = '', $sSbrbName = '')
    {
    	try
    	{
	        $oOrderLst	= OrderHd::Select('sOrdr_Id','sDelv_Date','sGrnd_Ttl','nOrdr_Status','mst_chld.sFrst_Name as sChld_FName','mst_chld.sLst_Name as sChld_LName','mst_prnts.sFrst_Name as sPrnt_FName','mst_prnts.sLst_Name as sPrnt_LName','sSchl_Name','sBuss_Name')->leftjoin('mst_chld', 'mst_chld.lChld_IdNo', '=', 'mst_ordr_hd.lChld_IdNo')
						->leftjoin('mst_schl', 'mst_schl.lSchl_IdNo', '=', 'mst_ordr_hd.lSchl_IdNo')
						->leftjoin('mst_prnts', 'mst_prnts.lPrnt_IdNo', '=', 'mst_ordr_hd.lPrnt_IdNo')
						->leftjoin('mst_milk_bar', 'mst_milk_bar.lMilk_IdNo', '=', 'mst_ordr_hd.lMilk_IdNo')
	        			->Where(function($query) use ($sFrmDate, $sToDate, $sPrntName, $lMilkIdNo, $sSbrbName) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && isset($sToDate) && !empty($sToDate)) {
                                $query->whereBetween('sDelv_Date',array($sFrmDate, $sToDate));
                            }
                            if (isset($sPrntName) && !empty($sPrntName)) {
                                $query->where('mst_prnts.sFrst_Name', 'LIKE', "%".$sPrntName."%");
                                $query->Orwhere('mst_prnts.sLst_Name', 'LIKE', "%".$sPrntName."%");
                            }
                            if (isset($lMilkIdNo) && !empty($lMilkIdNo)) {
                                $query->where('mst_ordr_hd.lMilk_IdNo',$lMilkIdNo);
                            }
                            if (isset($sSbrbName) && !empty($sSbrbName)) {
                                $query->where('mst_schl.sSbrb_Name', 'LIKE', $sSbrbName."%");
                            }
                        })->paginate(15);
	        return $oOrderLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }

    public function ExlRcrdAdmn($sFrmDate = '', $sToDate = '', $sPrntName = '', $lMilkIdNo = '', $sSbrbName = '')
    {
    	try
    	{
	        $aOrderLst	= OrderHd::Select('lOrder_IdNo','sOrdr_Id','sDelv_Date','sSub_Ttl','sGst_Amo','sGrnd_Ttl','nOrdr_Status','mst_ordr_hd.sCrt_DtTm','mst_chld.sFrst_Name as sChld_FName','mst_chld.sLst_Name as sChld_LName','mst_prnts.sFrst_Name as sPrnt_FName','mst_prnts.sLst_Name as sPrnt_LName','sSchl_Name','sBuss_Name')->leftjoin('mst_chld', 'mst_chld.lChld_IdNo', '=', 'mst_ordr_hd.lChld_IdNo')
						->leftjoin('mst_schl', 'mst_schl.lSchl_IdNo', '=', 'mst_ordr_hd.lSchl_IdNo')
						->leftjoin('mst_prnts', 'mst_prnts.lPrnt_IdNo', '=', 'mst_ordr_hd.lPrnt_IdNo')
						->leftjoin('mst_milk_bar', 'mst_milk_bar.lMilk_IdNo', '=', 'mst_ordr_hd.lMilk_IdNo')
	        			->Where(function($query) use ($sFrmDate, $sToDate, $sPrntName, $lMilkIdNo, $sSbrbName) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && isset($sToDate) && !empty($sToDate)) {
                                $query->whereBetween('sDelv_Date',array($sFrmDate, $sToDate));
                            }
                            if (isset($sPrntName) && !empty($sPrntName)) {
                                $query->where('mst_prnts.sFrst_Name', 'LIKE', "%".$sPrntName."%");
                                $query->Orwhere('mst_prnts.sLst_Name', 'LIKE', "%".$sPrntName."%");
                            }
                            if (isset($lMilkIdNo) && !empty($lMilkIdNo)) {
                                $query->where('mst_ordr_hd.lMilk_IdNo',$lMilkIdNo);
                            }
                            if (isset($sSbrbName) && !empty($sSbrbName)) {
                                $query->where('mst_schl.sSbrb_Name', 'LIKE', $sSbrbName."%");
                            }
                        })->get()->toArray();
	        return $aOrderLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
    
    public function GetMlkEarn($lMilkIdNo)
    {
    	try
    	{
			return OrderHd::groupBy('date')
			->orderBy('date', 'desc')
			->take(5)
			->where('lMilk_IdNo',$lMilkIdNo)
			->where('nOrdr_Status', '!=', config('constant.ORDER_STATUS.Cancelled'))
			->get([
				DB::raw('MONTH(sCrt_DtTm) as date'),
				DB::raw('SUM(sGrnd_Ttl) as total')
			])
			->pluck('total', 'date');
			
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }
	
	public function GetPrntCnt($lPrntIdNo)
    {
    	try
    	{
			return OrderHd::groupBy('date')
			->orderBy('date', 'desc')
			->take(5)
			->where('lPrnt_IdNo',$lPrntIdNo)
			->where('nOrdr_Status', '!=', config('constant.ORDER_STATUS.Cancelled'))
			->get([
				DB::raw('MONTH(sCrt_DtTm) as date'),
				DB::raw('COUNT(*) as total')
			])
			->pluck('total', 'date');
			
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }
	
	public function GetTtlRvnu()
    {
    	try
    	{
			return OrderHd::groupBy('date')
			->orderBy('date', 'desc')
			->take(5)
			->where('nOrdr_Status', '!=', config('constant.ORDER_STATUS.Cancelled'))
			->get([
				DB::raw('MONTH(sCrt_DtTm) as date'),
				DB::raw('SUM(sGrnd_Ttl) as total')
			])
			->pluck('total', 'date');
			
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function CntAllOrd($sFrmDate, $sToDate)
    {
    	try
    	{
	        $aCntRec = OrderHd::Select(DB::raw('COUNT(*) As nTtlRec'))->whereBetween('sDelv_Date',array($sFrmDate,$sToDate))->first()->toArray();
	        return $aCntRec;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
}
