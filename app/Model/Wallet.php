<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Wallet extends Model
{
    public $timestamps  = false;
    protected $table    = 'mst_wallet';

    public function InsrtRecrd($aHdArr)
    {
    	try
    	{
	        $nRow	= Wallet::insert($aHdArr);
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

	public function WlltLst($lPrntIdNo, $sFrmDate = '', $sToDate = '', $lMlkIdNo = '', $lOrdrIdNo = '', $nCredit = '', $nDebit = '')
    {
    	try
    	{
	        $oWlltLst	= 	Wallet::select('mst_wallet.*', 'mst_chld.sFrst_Name', 'mst_chld.sLst_Name', 'mst_milk_bar.sBuss_Name')
							->Where(function($query) use ($lMlkIdNo) {
	                            if (isset($lMlkIdNo) && !empty($lMlkIdNo)) {
	                                $query->where('lMilk_IdNo', $lMlkIdNo);
	                            }
	                        })
							->where(function($query) use ($lOrdrIdNo) {
	                            if (isset($lOrdrIdNo) && !empty($lOrdrIdNo)) {
	                                $query->where('lOrder_IdNo', $lOrdrIdNo);
	                            }
	                        })
							->where(function($query) use ($sFrmDate) {
	                            if (isset($sFrmDate) && !empty($sFrmDate)) {
	                                $query->where('mst_wallet.sCrt_DtTm', '>=', $sFrmDate);
	                            }
	                        })
							->where(function($query) use ($sToDate) {
	                            if (isset($sToDate) && !empty($sToDate)) {
	                                $query->where('mst_wallet.sCrt_DtTm', '<=', $sToDate);
	                            }
	                        })
							->where(function($query) use ($nCredit) {
	                            if (isset($nCredit) && !empty($nCredit)) {
	                                $query->where('nTyp_Status', config('constant.TRANS.Credit'));
	                            }
	                        })
							->where(function($query) use ($nDebit) {
	                            if (isset($nDebit) && !empty($nDebit)) {
	                                $query->where('nTyp_Status', config('constant.TRANS.Debit'));
	                            }
	                        })->leftjoin('mst_chld', 'mst_wallet.lChld_IdNo', '=', 'mst_chld.lChld_IdNo')
							->leftjoin('mst_milk_bar', 'mst_wallet.lMilk_IdNo', '=', 'mst_milk_bar.lMilk_IdNo')->where('mst_wallet.lPrnt_IdNo',$lPrntIdNo)
	        				->paginate(20);
	        return $oWlltLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
    
    public function OrdrIdGet($lOrdrIdNo)
    {
    	try
    	{
	        $oOrdrAmnt	= 	Wallet::where('lOrder_IdNo',$lOrdrIdNo)->where('nTyp_Status', config('constant.TRANS.Debit'))->first();
	        return $oOrdrAmnt;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
	
	public function MlkIdLst($lPrntIdNo)
    {
    	try
    	{
	        $aMlkIdLst	= 	Wallet::Select('mst_wallet.lMilk_IdNo', 'mst_milk_bar.sBuss_Name')
			->leftjoin('mst_milk_bar', 'mst_wallet.lMilk_IdNo', '=', 'mst_milk_bar.lMilk_IdNo')
			->where('lPrnt_IdNo',$lPrntIdNo)->get()->toArray();
	        return $aMlkIdLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
    
	public function GetCrdts($lPrntIdNo)
    {
    	try
    	{
	        $aCrdts	= 	Wallet::Select('sTtl_Amo', 'lMilk_IdNo', 'nTyp_Status')
			->where('lPrnt_IdNo',$lPrntIdNo)->get()->toArray();
	        return $aCrdts;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
    
    public function MilkCrdt($lMilkIdNo, $nTypStatus = '', $sFrmDate = '', $sToDate = '', $lSchlIdNo = '', $sOrdrId = '')
    {
    	try
    	{
	        $aWaltAmo	= Wallet::Select(DB::raw('SUM(sTtl_Amo) As sTtlAmo'))->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')->where('mst_wallet.lMilk_IdNo',$lMilkIdNo)
	        			->where(function($query) use ($sFrmDate, $sToDate) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_wallet.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })
                        ->where(function($query) use ($nTypStatus) {
                            if (isset($nTypStatus) && !empty($nTypStatus)) {
                                $query->where('nTyp_Status', $nTypStatus);
                            }
                        })
                        ->where(function($query) use ($lSchlIdNo) {
                            if (isset($lSchlIdNo) && !empty($lSchlIdNo)) {
                                $query->where('mst_ordr_hd.lSchl_IdNo', $lSchlIdNo);
                            }
                        })
                        ->where(function($query) use ($sOrdrId) {
                            if (isset($sOrdrId) && !empty($sOrdrId)) {
                                $query->where('mst_ordr_hd.sOrdr_Id','LIKE', $sOrdrId."%");
                            }
                        })->first()->toArray();
	        return $aWaltAmo;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }

    public function MilkCrdtsLst($lMilkIdNo, $sFrmDate = '', $sToDate = '', $lSchlIdNo = '', $sOrdrId = '')
    {
    	try
    	{
    		$oCrdtDtl = Wallet::Select('mst_wallet.*','mst_ordr_hd.sOrdr_Id','mst_chld.sFrst_Name','mst_chld.sLst_Name')->leftjoin('mst_chld','mst_chld.lChld_IdNo','=','mst_wallet.lChld_IdNo')->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')
    					->where(function($query) use ($sFrmDate, $sToDate) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_wallet.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })
                        ->where(function($query) use ($lSchlIdNo) {
                            if (isset($lSchlIdNo) && !empty($lSchlIdNo)) {
                                $query->where('mst_ordr_hd.lSchl_IdNo', $lSchlIdNo);
                            }
                        })
                        ->where(function($query) use ($sOrdrId) {
                            if (isset($sOrdrId) && !empty($sOrdrId)) {
                                $query->where('mst_ordr_hd.sOrdr_Id','LIKE', $sOrdrId."%");
                            }
                        })->Where('mst_wallet.lMilk_IdNo',$lMilkIdNo)->paginate(20);
    		return $oCrdtDtl;
    	}
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function ExlRcrd($lMilkIdNo, $sFrmDate = '', $sToDate = '', $lSchlIdNo = '', $sOrdrId = '')
    {
    	try
    	{
    		$aCrdtDtl = Wallet::Select('mst_wallet.*','mst_ordr_hd.sOrdr_Id','mst_chld.sFrst_Name','mst_chld.sLst_Name')->leftjoin('mst_chld','mst_chld.lChld_IdNo','=','mst_wallet.lChld_IdNo')->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')
    					->where(function($query) use ($sFrmDate, $sToDate) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_wallet.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })
                        ->where(function($query) use ($lSchlIdNo) {
                            if (isset($lSchlIdNo) && !empty($lSchlIdNo)) {
                                $query->where('mst_ordr_hd.lSchl_IdNo', $lSchlIdNo);
                            }
                        })
                        ->where(function($query) use ($sOrdrId) {
                            if (isset($sOrdrId) && !empty($sOrdrId)) {
                                $query->where('mst_ordr_hd.sOrdr_Id','LIKE', $sOrdrId."%");
                            }
                        })->Where('mst_wallet.lMilk_IdNo',$lMilkIdNo)->get()->toArray();
    		return $aCrdtDtl;
    	}
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function PrntCrdt($lPrntIdNo, $nTypStatus = '', $sFrmDate = '', $sToDate = '', $lMilkIdNo = '', $sOrdrId = '')
    {
        try
        {
            $aWaltAmo   = Wallet::Select(DB::raw('SUM(sTtl_Amo) As sTtlAmo'))->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')->where('mst_wallet.lPrnt_IdNo',$lPrntIdNo)
                        ->where(function($query) use ($sFrmDate, $sToDate) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_wallet.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })
                        ->where(function($query) use ($nTypStatus) {
                            if (isset($nTypStatus) && !empty($nTypStatus)) {
                                $query->where('nTyp_Status', $nTypStatus);
                            }
                        })
                        ->where(function($query) use ($lMilkIdNo) {
                            if (isset($lMilkIdNo) && !empty($lMilkIdNo)) {
                                $query->where('mst_ordr_hd.lMilk_IdNo', $lMilkIdNo);
                            }
                        })
                        ->where(function($query) use ($sOrdrId) {
                            if (isset($sOrdrId) && !empty($sOrdrId)) {
                                $query->where('mst_ordr_hd.sOrdr_Id','LIKE', $sOrdrId."%");
                            }
                        })->first()->toArray();
            return $aWaltAmo;
        }
        catch(\Expection $e)
        {
            return $e->getMessage();
        }   
    }

    public function PrntCrdtsLst($lPrntIdNo, $sFrmDate = '', $sToDate = '', $lMilkIdNo = '', $sOrdrId = '')
    {
        try
        {
            $oCrdtLst = Wallet::Select('mst_wallet.*','mst_ordr_hd.sOrdr_Id','mst_chld.sFrst_Name','mst_chld.sLst_Name','sBuss_Name')->leftjoin('mst_chld','mst_chld.lChld_IdNo','=','mst_wallet.lChld_IdNo')->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')->leftjoin('mst_milk_bar','mst_milk_bar.lMilk_IdNo','=','mst_wallet.lMilk_IdNo')
                        ->where(function($query) use ($sFrmDate, $sToDate) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_wallet.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })
                        ->where(function($query) use ($lMilkIdNo) {
                            if (isset($lMilkIdNo) && !empty($lMilkIdNo)) {
                                $query->where('mst_ordr_hd.lMilk_IdNo', $lMilkIdNo);
                            }
                        })
                        ->where(function($query) use ($sOrdrId) {
                            if (isset($sOrdrId) && !empty($sOrdrId)) {
                                $query->where('mst_ordr_hd.sOrdr_Id','LIKE', $sOrdrId."%");
                            }
                        })->Where('mst_wallet.lPrnt_IdNo',$lPrntIdNo)->paginate(20);
            return $oCrdtLst;
        }
        catch(\Expection $e)
        {
            return $e->getMessage();
        }
    }

    public function ExlRcrdPrnt($lPrntIdNo, $sFrmDate = '', $sToDate = '', $lMilkIdNo = '', $sOrdrId = '')
    {
        try
        {
            $aCrdtDtl = Wallet::Select('mst_wallet.*','mst_ordr_hd.sOrdr_Id','mst_chld.sFrst_Name','mst_chld.sLst_Name','sBuss_Name')->leftjoin('mst_chld','mst_chld.lChld_IdNo','=','mst_wallet.lChld_IdNo')->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')->leftjoin('mst_milk_bar','mst_milk_bar.lMilk_IdNo','=','mst_wallet.lMilk_IdNo')
                        ->where(function($query) use ($sFrmDate, $sToDate) {
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_wallet.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })
                        ->where(function($query) use ($lMilkIdNo) {
                            if (isset($lMilkIdNo) && !empty($lMilkIdNo)) {
                                $query->where('mst_ordr_hd.lMilk_IdNo', $lMilkIdNo);
                            }
                        })
                        ->where(function($query) use ($sOrdrId) {
                            if (isset($sOrdrId) && !empty($sOrdrId)) {
                                $query->where('mst_ordr_hd.sOrdr_Id','LIKE', $sOrdrId."%");
                            }
                        })->Where('mst_wallet.lPrnt_IdNo',$lPrntIdNo)->get()->toArray();
            return $aCrdtDtl;
        }
        catch(\Expection $e)
        {
            return $e->getMessage();
        }
    }
    
    public function CommLst($sFrmDate = '', $sToDate = '', $lMilkIdNo = '')
    {
        try
        {
            $oComLst = Wallet::Select('sTtl_Amo','sOrdr_Id','sGrnd_Ttl','sBuss_Name','mst_ordr_hd.sCrt_DtTm')->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')->leftjoin('mst_milk_bar','mst_milk_bar.lMilk_IdNo','=','mst_wallet.lMilk_IdNo')
                        ->where(function($query) use ($sFrmDate, $sToDate, $lMilkIdNo) {
                            if (isset($lMilkIdNo) && !empty($lMilkIdNo)) {
                                $query->where('mst_ordr_hd.lMilk_IdNo', $lMilkIdNo);
                            }
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_ordr_hd.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })->whereNotNull('sStrp_Trns_Id')->Where('sTtl_Amo','<','sGrnd_Ttl')->Where('nTyp_Status',config('constant.TRANS.Debit'))->paginate(15);
            return $oComLst;
        }
        catch(\Expection $e)
        {
            return $e->getMessage();
        }
    }

    public function ExlRcrdCom($sFrmDate = '', $sToDate = '', $lMilkIdNo = '')
    {
        try
        {
            $aComLst = Wallet::Select('sTtl_Amo','sOrdr_Id','sGrnd_Ttl','sBuss_Name','mst_ordr_hd.sCrt_DtTm')->leftjoin('mst_ordr_hd','mst_ordr_hd.lOrder_IdNo','=','mst_wallet.lOrder_IdNo')->leftjoin('mst_milk_bar','mst_milk_bar.lMilk_IdNo','=','mst_wallet.lMilk_IdNo')
                        ->where(function($query) use ($sFrmDate, $sToDate, $lMilkIdNo) {
                            if (isset($lMilkIdNo) && !empty($lMilkIdNo)) {
                                $query->where('mst_ordr_hd.lMilk_IdNo', $lMilkIdNo);
                            }
                            if (isset($sFrmDate) && !empty($sFrmDate) && !empty($sToDate)) {
                                $query->whereBetween('mst_ordr_hd.sCrt_DtTm', array($sFrmDate,$sToDate));
                            }
                        })->whereNotNull('sStrp_Trns_Id')->Where('sTtl_Amo','<','sGrnd_Ttl')->Where('nTyp_Status',config('constant.TRANS.Debit'))->get()->toArray();
            return $aComLst;
        }
        catch(\Expection $e)
        {
            return $e->getMessage();
        }
    }
}