<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class Country extends Model
{
    public $timestamps  = false;
    protected $table    = 'mst_cntry';

    public function InsrtRecrd($aHdArr)
    {
    	try
    	{
	        $nRow	= Country::insert($aHdArr);
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function FrntLst()
    {
    	try
    	{
	        $aGetRec = Country::Select('lCntry_IdNo','sCntry_Name','sCntry_Code')->Where('nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.UNDELETED'))->get()->toArray();
	        return $aGetRec;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
}
