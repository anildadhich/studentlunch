<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class AssociateSchool extends Model
{
    public $timestamps  = false;
    protected $table    = 'milk_schl';

    public function InsrtRecrd($aHdArr)
    {
    	try
    	{
	        $nRow	= AssociateSchool::insert($aHdArr);
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function DelRecrd($aHdArr, $lMilkIdNo)
    {
    	try
    	{
	        $nRow	= AssociateSchool::Where('lMilk_IdNo',$lMilkIdNo)->update($aHdArr);
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function UpDtRecrd($aHdArr, $lMilkSchlIdNo)
    {
    	try
    	{
	        $nRow	= AssociateSchool::Where('lMilk_Schl_IdNo',$lMilkSchlIdNo)->update($aHdArr);
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function AccSchlLst($lMilkIdNo)
    {
    	try
    	{
	        $aSchlLst	= AssociateSchool::Select('milk_schl.*','mst_schl.sSchl_Name')->leftjoin('mst_schl', 'mst_schl.lSchl_IdNo', '=', 'milk_schl.lSchl_IdNo')->Where('milk_schl.nDel_Status',config('constant.DEL_STATUS.UNDELETED'))->Where('lMilk_IdNo',$lMilkIdNo)->get()->toArray();
	        return $aSchlLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
	
	public function MlkLst($sSchlIdLst, $sDate)
    {
    	try
    	{
			if(strtotime($sDate) == strtotime(date('Y-m-d')))
			{
				$aMlkLst	= AssociateSchool::Select('mst_milk_bar.sBuss_Name','mst_milk_bar.lMilk_IdNo','milk_schl.lSchl_IdNo')->Rightjoin('mst_milk_bar', 'mst_milk_bar.lMilk_IdNo', '=', 'milk_schl.lMilk_IdNo')->where('milk_schl.sCut_Tm', '>', \DB::raw('NOW()'))->Where('mst_milk_bar.nDel_Status',config('constant.DEL_STATUS.UNDELETED'))->Where('mst_milk_bar.nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('mst_milk_bar.nAdmin_Status',config('constant.MLK_STATUS.ACTIVE'))->Where('milk_schl.lSchl_IdNo',$sSchlIdLst)->get()->toArray();
			}else{
				$aMlkLst	= AssociateSchool::Select('mst_milk_bar.sBuss_Name','mst_milk_bar.lMilk_IdNo','milk_schl.lSchl_IdNo')->Rightjoin('mst_milk_bar', 'mst_milk_bar.lMilk_IdNo', '=', 'milk_schl.lMilk_IdNo')->Where('mst_milk_bar.nDel_Status',config('constant.DEL_STATUS.UNDELETED'))->Where('mst_milk_bar.nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('mst_milk_bar.nAdmin_Status',config('constant.MLK_STATUS.ACTIVE'))->Where('milk_schl.lSchl_IdNo',$sSchlIdLst)->get()->toArray();
	        }
			return $aMlkLst;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
    
    public function GetRecrd($lSchlIdNo, $lMilkIdNo)
    {
    	try
    	{
	        $nAssSchl	= AssociateSchool::Where('lMilk_IdNo',$lMilkIdNo)->where('lSchl_IdNo',$lSchlIdNo)->first();
	        return $nAssSchl;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }
}
