<?php

namespace App\Model;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use DB;

class State extends Model
{
    public $timestamps  = false;
    protected $table    = 'mst_state';

    public function InsrtRecrd($aHdArr)
    {
    	try
    	{
	        $nRow	= Country::insert($aHdArr);
	        return $nRow;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }
    }

    public function FrntLst($lCntryIdNo)
    {
    	try
    	{
	        $aGetRec = State::Select('lState_IdNo','sState_Name')->Where('lCntry_IdNo',$lCntryIdNo)->Where('nBlk_UnBlk',config('constant.STATUS.UNBLOCK'))->Where('nDel_Status',config('constant.DEL_STATUS.UNDELETED'))->get()->toArray();
	        return $aGetRec;
	    }
	    catch(\Expection $e)
	    {
	    	return $e->getMessage();
	    }	
    }
}
