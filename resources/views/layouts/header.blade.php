<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="{{url('./')}}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ $sTitle }} | Student Lunch</title>
<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>

<body>
<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-6"><p>ABN: 39436254039</p></div>
            <div class="col-6">
                <ul class="top-social-media">
                    <li><a href="#"><span class="top-linkdin-icon"></span></a></li>
                    <li><a href="#"><span class="top-youtube-icon"></span></a></li>
                    <li><a href="#"><span class="top-facebook-icon"></span></a></li>
                    <li><a href="#"><span class="top-instagram-icon"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main-header">
    <div class="container">
        <div class="row">
            <div class="logo-area">
                <a href="{{url('')}}"><img src="{{url('images/logo.png')}}" alt="" id="logo" /></a>
            </div>
            <div class="menu-area" id="menu-area">
                <ul>
                    <li class="active"><a href="{{url('')}}">Home</a></li>           
                    <li><a href="#">About us</a>
                        <ul>
                            <li><a href="#">Dropdown 1</a></li>
                            <li><a href="#">Dropdown 2</a></li>
                            <li><a href="#">Dropdown 3</a></li>
                            <li><a href="#">Dropdown 4</a></li>
                            <li><a href="#">Dropdown 5</a></li>
                        </ul>
                    </li>            
                    <li><a href="#">Menu</a></li>            
                    <li><a href="#">Services</a></li>            
                    <li><a href="#">Offers</a></li>            
                    <li><a href="#">Contact Us</a></li> 
			       <!--		Session('user')['sFrst_Name'] -->
					@if(Session::has('USER_ID'))
						<li> 
							<button class="btn dropdown-toggle" type="button" data-toggle="dropdown">{{ Session('USER_NAME') }}
							<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li><a href="{{ Session('USER_TYPE') == 'P' ? url('parent_panel') : url('milkbar_panel')}}">Account</a></li>
								<li> 
									<a class="dropdown-item" href="{{ url('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
									<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
								</li>
							</ul>
						</li>
					@else
						<li><a href="{{url('user/login')}}" class="btn-blue-border">Login</a></li> 
						<li><a href="{{url('registration/parent')}}" class="btn-blue">Register</a></li>
					@endif
                </ul>
            </div>
            <div class="toggle-menu" onclick="toogle_menu(this)">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </div>
    </div>
</div>