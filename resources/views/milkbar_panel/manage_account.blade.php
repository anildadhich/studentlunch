@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('milkbar_panel.layouts.side_panel')
            <form action="{{url('milkbar_panel/my_account/save')}}" method="post" id="general_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="lMilkIdNo" id="lMilkIdNo" value="{{base64_encode($aMilkDtl['lMilk_IdNo'])}}">
                <main>
                    <div class="page-breadcrumb">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="page-title">Manage Account ({{$aMilkDtl['sAcc_Id']}})</h4>
                            </div>
                        </div>
                    </div>
                    @include('admin_panel.layouts.message')
                    <div class="container-fluid card-commission-section  parent-details-section">
                        <div class="row">
                            <div class="col-sm-3 col-lg-12">
                                <div>
                                    <h4>Basic Information</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row account-form">
                            <div class="col">
                                <label>Business Name</label>
                                <input type="text" class="form-control @error('sBussName') is-invalid @enderror" name="sBussName" value="{{ old('sBussName', $aMilkDtl['sBuss_Name']) }}" onkeypress="return IsAlpha(event, this.value, '50')" required />
                                @error('sBussName') <div class="invalid-feedback"><span>{{$errors->first('sBussName')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label>Business Type</label>
                                <select class="form-control @error('nBussType') is-invalid @enderror" name="nBussType" required>
                                    <option value="">Business Type</option>
                                    @foreach(config('constant.BUSS_TYPE') as $sTypeName => $nBussType)
                                        <option {{ old('sBussName', $aMilkDtl['nBuss_Type']) == $nBussType ? 'selected' : ''}} value="{{$nBussType}}">{{$sTypeName}}</option>
                                    @endforeach
                                </select>
                                @error('nBussType') <div class="invalid-feedback"><span>{{$errors->first('nBussType')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label>Business ABN</label>
                                <input type="text" class="form-control @error('sAbnNo') is-invalid @enderror" name="sAbnNo" value="{{ old('sAbnNo', $aMilkDtl['sAbn_No']) }}" onkeypress="return IsNumber(event, this.value, '11')" required />
                                @error('sAbnNo') <div class="invalid-feedback"><span>{{$errors->first('sAbnNo')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label>Country</label>
                                <select class="form-control @error('lCntryIdNo') is-invalid @enderror" name="lCntryIdNo" id="lCntryIdNo" onchange="GetState(this.value)" required>
                                    <option value="">== Select Country ==</option>
                                    @foreach($aCntryLst as $aRec)
                                        <option {{ old('lCntryIdNo', $aMilkDtl['lCntry_IdNo']) == $aRec['lCntry_IdNo'] ? 'selected' : ''}} value="{{$aRec['lCntry_IdNo']}}" data-code="{{$aRec['sCntry_Code']}}">{{$aRec['sCntry_Name']}}</option>
                                    @endforeach
                                </select>
                                @error('lCntryIdNo') <div class="invalid-feedback"><span>{{$errors->first('lCntryIdNo')}}</span></div>@enderror
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid card-commission-section parent-list-section parent-details-section">
                        <div class="row">
                            <div class="col-sm-3 col-lg-12">
                                <div>
                                    <h4>Business Contact Details</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row  account-form">
                            <div class="col">
                                <label>First Name</label>
                                <input type="text" class="form-control @error('sFrstName') is-invalid @enderror" name="sFrstName" value="{{ old('sFrstName', $aMilkDtl['sFrst_Name']) }}" onkeypress="return IsAlpha(event, this.value, '15')" required />
                                @error('sFrstName') <div class="invalid-feedback"><span>{{$errors->first('sFrstName')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label>Surname</label>
                                <input type="text" class="form-control @error('sLstName') is-invalid @enderror" name="sLstName" value="{{ old('sLstName', $aMilkDtl['sLst_Name']) }}" onkeypress="return IsAlpha(event, this.value, '15')" required />
                                @error('sLstName') <div class="invalid-feedback"><span>{{$errors->first('sLstName')}}</span></div>@enderror
                            </div>
                            @php
                            $aPhoneNo = explode(" ", $aMilkDtl['sPhone_No']);
                            @endphp
                            <div class="col">
                                <div class="col">
                                    <label>Phone Number </label>
                                    <div class="row">
                                        <div class="col-lg-3 p-l-15">
                                            <input type="text" class="form-control cnoutry_code" name="sCntryCodePhone" value="{{ old('sCntryCodePhone', $aPhoneNo[0]) }}" readonly />    
                                        </div>
                                        <div class="col-lg-3 p-0">
                                            <input type="text" class="form-control" name="sAreaCode" value="{{ old('sAreaCode', $aPhoneNo[1]) }}" placeholder="Area Code" onkeypress="return IsNumber(event, this.value, '1')" required />    
                                        </div>
                                        <div class="col-lg-6  p-r-15">
                                            <input type="text" class="form-control @error('sPhoneNo') is-invalid @enderror" name="sPhoneNo" value="{{ old('sPhoneNo', $aPhoneNo[2]) }}" onkeypress="return IsNumber(event, this.value, '8')" required />    
                                        </div>
                                    </div>
                                    @error('sPhoneNo') <div class="invalid-feedback"><span>{{$errors->first('sPhoneNo')}}</span></div>@enderror
                                </div>
                            </div>
                        </div>
                        @php
                        $aMobileNo = explode(" ", $aMilkDtl['sMobile_No']);
                        @endphp
                        <div class="row account-form">
                            <div class="col">
                                <label>Mobile Number </label>
                                <div class="row">
                                    <div class="col-lg-3 p-l-15">
                                        <input type="text" class="form-control cnoutry_code" name="sCntryCode" value="{{ old('sCntryCode', $aMobileNo[0]) }}" readonly />    
                                    </div>
                                    <div class="col-lg-9  p-r-15">
                                        <input type="text" class="form-control @error('sMobileNo') is-invalid @enderror" name="sMobileNo" value="{{ old('sMobileNo', $aMobileNo[1]) }}" onkeypress="return IsNumber(event, this.value, '9')" required />    
                                    </div>
                                </div>
                                @error('sMobileNo') <div class="invalid-feedback"><span>{{$errors->first('sMobileNo')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label> Email</label>
                                <input type="email" class="form-control" value="{{$aMilkDtl['sEmail_Id']}}" name="sEmailId" readonly />
                            </div>
                            <div class="col">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid card-commission-section parent-list-section parent-details-section">
                        <div class="row">
                            <div class="col-sm-3 col-lg-12">
                                <div>
                                    <h4>Address</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row  account-form">
                            <div class="col">
                                <label>Street No.</label>
                                <input type="text" class="form-control @error('sStrtNo') is-invalid @enderror" name="sStrtNo" value="{{ old('sStrtNo', $aMilkDtl['sStrt_No']) }}" onkeypress="return IsNumber(event, this.value, '4')" required />
                                @error('sStrtNo') <div class="invalid-feedback"><span>{{$errors->first('sStrtNo')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label>Street Name</label>
                                <input type="text" class="form-control @error('sStrtName') is-invalid @enderror" name="sStrtName" value="{{ old('sStrtName', $aMilkDtl['sStrt_Name']) }}" onkeypress="return IsAlpha(event, this.value, '50')" required />
                                @error('sStrtName') <div class="invalid-feedback"><span>{{$errors->first('sStrtName')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label>  Suburb</label>
                                <input type="text" class="form-control @error('sSbrbName') is-invalid @enderror" name="sSbrbName" value="{{ old('sSbrbName', $aMilkDtl['sSbrb_Name']) }}" onkeypress="return IsAlpha(event, this.value, '20')" required />
                                @error('sSbrbName') <div class="invalid-feedback"><span>{{$errors->first('sSbrbName')}}</span></div>@enderror
                            </div>
                        </div>
                        <div class="row account-form">
                            <div class="col">
                                <label>State</label>
                                <select class="form-control @error('lStateIdNo') is-invalid @enderror" name="lStateIdNo" id="lStateIdNo" required>
                                    <option value="">== Select State ==</option>
                                    @if(!empty($aStateLst))
                                        @foreach($aStateLst as $aRec)
                                            <option {{ old('lStateIdNo', $aMilkDtl['lState_IdNo']) == $aRec['lState_IdNo'] ? 'selected' :  '' }} onkeypress="return IsAlpha(event, this.value, '20')" required ''}} value="{{$aRec['lState_IdNo']}}">{{$aRec['sState_Name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @error('lStateIdNo') <div class="invalid-feedback"><span>{{$errors->first('lStateIdNo')}}</span></div>@enderror
                            </div>
                            <div class="col">
                                <label> Post Code</label>
                                <input type="text" class="form-control @error('sPinCode') is-invalid @enderror" name="sPinCode" value="{{ old('sPinCode', $aMilkDtl['sPin_Code']) }}" onkeypress="return IsNumber(event, this.value, '4')" required />
                                @error('sPinCode') <div class="invalid-feedback"><span>{{$errors->first('sPinCode')}}</span></div>@enderror
                            </div>
                            <div class="col">
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid card-commission-section parent-list-section parent-details-section">
                        <div class="row ">
                            <div class="col-sm-3 col-lg-6">
                                <div>
                                    <h4>School's You Service</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-lg-12 school-service-table">
                                <table style="border: 1px red solid; width: 100%; " id="display-table">
                                    <tr>
                                        <th></th>
                                        <th>School Type</th>
                                        <th>School Name</th>
                                        <th>Distance (in KM)</th>
                                        <th>Suburb</th>
                                        <th>Post Code</th>
                                        <th>Order Cut-Off Time</th>
                                    </tr>
                                    <tbody>
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach($aAccSchl as $aRes)
                                            <input type="hidden" name="lMilkSchlIdNo{{$i}}" value="{{$aRes['lMilk_Schl_IdNo']}}"/>
                                            <tr id="Row_{{$i}}">
                                                @if($i == 1)
                                                    <td><i class="fa fa-plus" onclick="CrtRow()"></i></td>
                                                @else
                                                    <td><i class="fa fa-minus" onclick="DeleteRow('{{$i}}')"></i></td>
                                                @endif
                                                <td>
                                                    <select name="nSchlType{{$i}}" class="form-control">
                                                        <option value="">School Type</option>
                                                         @foreach(config('constant.SCHL_TYPE') as $sTypeName => $nType)
                                                            <option {{ $aRes['nSchl_Type'] == $nType ? 'selected' : ''}} value="{{$nType}}">{{$sTypeName}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="lSchlIdNo{{$i}}" class="form-control">
                                                        <option value="">Child School Name</option>
                                                        @foreach($aSchlLst as $aRec)
                                                            <option {{ $aRes['lSchl_IdNo'] == $aRec['lSchl_IdNo'] ? 'selected' : ''}} value="{{$aRec['lSchl_IdNo']}}">{{$aRec['sSchl_Name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" class="form-control" name="dDistKm{{$i}}" value="{{$aRes['dDist_Km']}}" onkeypress="return ChkKm(event, this.value, 1)" required /></td>
                                                <td><input type="text" class="form-control" name="sSbrbName{{$i}}" value="{{$aRes['sSbrb_Name']}}" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>
                                                <td><input type="text" class="form-control" name="sPinCode{{$i}}"  value="{{$aRes['sPin_Code']}}" onkeypress="return IsNumber(event, this.value, 4)" required /></td>
                                                <td><input type="time" class="form-control text-right" name="sCutTm{{$i}}" value="{{$aRes['sCut_Tm']}}" required /></td>
                                            </tr>
                                            @php
                                            $i++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                <input type="hidden" name="nTtlRec" id="nTtlRec" value="{{count($aAccSchl)}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 services-btns">
                                <ul class="m-auto text-center pt-4 pb-4">
                                    <li>
                                        <div class="add-btn  mt-0"><button title="Add Milk Bar" class="mt-0">Back</button></div>
                                    </li>
                                    <li>
                                        <div class="add-btn  mt-0"><button title="Add Milk Bar" type="submit" class="mt-0">Save</button></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </main>
            </form>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
function CrtRow()
{
    var rowCount = $('#display-table tbody tr').length-1;
    if(rowCount == 3)
    {
        alert("Maximum 3 schools allowed...");
    }
    else
    {
        total = $("#nTtlRec").val();
        next_no = parseInt(total)+1;
        newdiv = document.createElement('tr');
        divid = "Row_"+next_no;
        newdiv.setAttribute('id', divid);
        content = '';
        content += '<tr id="Row_'+next_no+'">';
        content += '<td><i class="fa fa-minus" onclick="DeleteRow('+next_no+')"></i></td>';
        content += '<td><select name="nSchlType'+next_no+'" class="form-control"><option value="">School Type</option>@foreach(config('constant.SCHL_TYPE') as $sTypeName => $nType)<option value="{{$nType}}">{{$sTypeName}}</option>  @endforeach</select></td>';
        content += '<td><select name="lSchlIdNo'+next_no+'" class="form-control"><option value="">Child School Name</option>@foreach($aSchlLst as $aRec)<option value="{{$aRec['lSchl_IdNo']}}">{{$aRec['sSchl_Name']}}</option>@endforeach</select></td>';
        content += '<td><input type="text" class="form-control" name="dDistKm'+next_no+'" value="" onkeypress="return ChkKm(event, this.value, 1)" required /></td>';
        content += '<td><input type="text" class="form-control" name="sSbrbName'+next_no+'" value="" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>';
        content += '<td><input type="text" class="form-control" name="sPinCode'+next_no+'"  value="" onkeypress="return IsNumber(event, this.value, 4)" required /></td>';
        content += '<td><input type="time" class="form-control text-right" name="sCutTm'+next_no+'" value="" required /></td>';
        content += '</tr>';
        newdiv.innerHTML = content;
        $("#nTtlRec").val(next_no);
        $("#display-table tbody").last().append(newdiv);
    }
}

function DeleteRow(nRow)
{
    if(confirm("Are you sure to delete this row") == true) {
        var row = $('#Row_'+nRow);
        row.remove();
    }
}
</script>