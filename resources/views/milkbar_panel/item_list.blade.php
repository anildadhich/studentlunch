@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('milkbar_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Manage Menu</h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section manage-page">
                    <div class="row">
                        <div class="col-sm-3 col-lg-12">
                            <div>
                                <h4>Manage Menu</h4>
                            </div>
                        </div>
                    </div>
                    <form action="{{url('milkbar_panel/item/list')}}" method="get">
                        <div class="row first-block parent-list-form">
                            <div class='col-sm-3'>
                                <label>Category Name</label>
                                <select class="form-control" name="lCatgIdNo">
                                    <option value="">== Select Category ==</option>
                                    @foreach($aCatgLst as $aRec)
                                        <option {{ $request['lCatgIdNo'] == $aRec['lCatg_IdNo'] ? 'selected' : ''}} value="{{$aRec['lCatg_IdNo']}}">{{$aRec['sCatg_Name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class='col-sm-3'>
                                <label>Item Name</label>
                                <input type="text" name="sItemName" placeholder="Item Name" value="{{$request['sItemName']}}">
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button type="submit" title="Filter">Filter</button></li>
                                    <li><button class="mr-0" type="button" title="Export To Excel" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                            <div class='col-sm-3 text-right'>
                                <div class="add-btn">
                                    <button title="Add Category" type="button" data-rec="" data-toggle="modal" data-target="#ItemModel">Add Item</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Item ID</th>
                                    <th>Category Name</th>
                                    <th>Item Name </th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                @if(count($aItemLst) > 0)
                                    @foreach($aItemLst as $aRec)
                                    <tr>
                                        <td>{{$aRec->lItem_Unq_Id}}</td>
                                        <td>{{$aRec->sCatg_Name}}</td>
                                        <td>{{$aRec->sItem_Name}}</td>
                                        <td>{{number_format($aRec->sItem_Prc, 2)}}</td>
                                        <td>
                                            @if($aRec->nBlk_UnBlk == config('constant.STATUS.BLOCK'))
                                                <button class="block-btn" title="Block" onclick="chngStatus('{{base64_encode('mst_item')}}','{{base64_encode('lItem_IdNo')}}','{{base64_encode($aRec->lItem_IdNo)}}','{{base64_encode(config('constant.STATUS.UNBLOCK'))}}')">Block</button>
                                            @else
                                                <button class="active-btn" title="Unblock" onclick="chngStatus('{{base64_encode('mst_item')}}','{{base64_encode('lItem_IdNo')}}','{{base64_encode($aRec->lItem_IdNo)}}','{{base64_encode(config('constant.STATUS.BLOCK'))}}')">Unblock</button>
                                            @endif
                                        </td>
                                        <td class="action-btns">
                                            <ul>
                                                <li><a href="#" title="Edit {{$aRec->sItem_Name}}" data-rec="{{$aRec}}" data-toggle="modal" data-target="#ItemModel"> <i class="fa fa-edit"></i></a></li>
                                                <li><i class="fa fa-trash" onclick="DelRec('{{base64_encode('mst_item')}}','{{base64_encode('lItem_IdNo')}}','{{base64_encode($aRec->lItem_IdNo)}}')"></i></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 pagination">
                            {{$aItemLst->appends($request->all())->render()}}
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

    <div class="modal fade" id="ItemModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('milkbar_panel/item/save')}}" method="post" id="general_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="lItemIdNo" id="lItemIdNo" value="{{ base64_encode(0) }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Manage Item</h4>
                    </div>
                    <div class="modal-body card-commission-section">
                        <form>
                            <div class="row account-form">
                                <div class="col">
                                    <label>Category Name</label>
                                    <select class="form-control" name="lCatgIdNo" required>
                                        <option value="">== Select Category ==</option>
                                        @foreach($aCatgLst as $aRec)
                                            <option value="{{$aRec['lCatg_IdNo']}}">{{$aRec['sCatg_Name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row account-form">
                                <div class="col">
                                    <label>Item Name</label>
                                    <input type="text" name="sItemName" class="form-control" onkeypress="return IsAlpha(event, this.value, '30')" required>
                                </div>
                            </div>
                            <div class="row account-form">
                                <div class="col">
                                    <label>Item Description</label>
                                    <textarea class="form-control" name="sItemDscrptn" onkeypress="return LenCheck(event, this.value, '150')" required></textarea>
                                </div>
                            </div>
                            <div class="row account-form">
                                <div class="col">
                                    <label>Item Price</label>
                                    <input type="text" name="sItemPrc" class="form-control" onkeypress="return IsDecimal(event, this.value, '6')" required>
                                </div>
                            </div>
                            <div class="row account-form days-div">
                                <div class="col">
                                    <label>Availability Day</label>
                                    <ul>
                                        @foreach(config('constant.WEEK') as $sWkName => $nWkDay)
                                            <li>
                                                <input type="checkbox" name="aItemWeek[]" id="fruit{{$nWkDay}}" value="{{$nWkDay}}" checked>
                                                <label for="fruit{{$nWkDay}}">{{$sWkName}}</label>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-lg-12 services-btns">
                                <ul class="m-auto text-center">
                                    <li>
                                        <div class="add-btn  mt-0"><button class="mt-0" data-dismiss="modal" aria-label="Close">Cancel</button></div>
                                    </li>
                                    <li>
                                        <div class="add-btn  mt-0"><button title="Add Category" type="submit" class="mt-0">Save</button></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
$('#ItemModel').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var aRec = button.data('rec');
    if(aRec != '')
    {
        var aItemWeek = aRec['aItem_Week'].split(",");
        //$(this).find(':checkbox').prop('checked', false);
        $(this).find('select[name="lCatgIdNo"] option[value='+aRec['lCatg_IdNo']+']').attr('selected','selected');
        $(this).find("input[name='lItemIdNo']").val(btoa(aRec['lItem_IdNo']));
        $(this).find("input[name='sItemName']").val(aRec['sItem_Name']);
        $(this).find("textarea").html(aRec['sItem_Dscrptn']);
        $(this).find("input[name='sItemPrc']").val(aRec['sItem_Prc']);
        $(this).find('select[name="lCatgIdNo"] option[value='+aRec['lCatg_IdNo']+']').attr('selected','selected');
        $("input[name='aItemWeek[]']").each(function(index) {
            var val = $(this).val();
            if (aItemWeek.includes(val)) 
            {
                $(this).prop('checked', true);
            }
            else
            {
                $(this).prop('checked', false);
            }
        });
    }
})

$('#ExprtRcrd').on('click', function() {
    var lCatgIdNo = $("select[name=lCatgIdNo]").find(":selected").val();
    var lCatgIdNo = lCatgIdNo == 'undefined' ? '' : lCatgIdNo;
    var sItemName = $("input[name=sItemName]").val();
    window.location=APP_URL+"/milkbar_panel/item/export?lCatgIdNo="+lCatgIdNo+"&sItemName="+sItemName;
});
</script>