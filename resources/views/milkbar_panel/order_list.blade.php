@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('milkbar_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">My Orders</h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section">
                    <form action="{{url('milkbar_panel/my_orders')}}" method="get">
                        <div class="row first-block">
                            <div class='col-sm-3 from-boxes'>
                                <label>Date of Delivery</label>
                                <input type="date" name="sDelvDate" placeholder="MM/DD/YYYY" value="{{$request['sDelvDate']}}"> 
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>School Name</label>
                                <select name="lSchlIdNo">
                                    <option value="">All School</option>
									@foreach($aAccSchl as $aRec)
										<option {{ $request['lSchlIdNo'] == $aRec['lSchl_IdNo'] ? 'selected' : ''}} value="{{$aRec['lSchl_IdNo']}}">{{$aRec['sSchl_Name']}}</option>
									@endforeach
                                </select>
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>Order Status</label>
                                <select name="nOrdrStatus">
                                    <option value="">All Order</option>
                                    @foreach(config('constant.ORDER_STATUS') as $sStatusName => $nOrdrStatus)
                                        <option {{ $request['nOrdrStatus'] == $nOrdrStatus ? 'selected' : ''}} value="{{$nOrdrStatus}}">{{$sStatusName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button type="submit">Filter</button></li>
                                    <li><button type="button" class="mr-0" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details table-responsive parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Order No</th>
									<th>Student Name</th>
                                    <th>School Name</th>
                                    <th>Class</th>
									<th class="text-right">Amount</th>
									<th>Delivery Date</th>
									<th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
								</tr>
								@if(count($oOrdLst) > 0)
                                    @foreach($oOrdLst As $aRec)
										<tr>
                                            <td>{{$aRec->sOrdr_Id}}</td>
                                            <td>{{$aRec->sFrst_Name}} {{$aRec->sLst_Name}}</td>
                                            <td>{{$aRec->sSchl_Name}}</td>
                                            <td>{{$aRec->sCls_Name}}</td>
                                            <td class="text-right">$ {{$aRec->sGrnd_Ttl}}</td>
											<td>{{date('d M, Y', strtotime($aRec->sDelv_Date))}}</td>
											<td class="text-center">
                                                @if($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Pending'))
                                                    <button class="primary-btn" title="Pending">Pending</button>
                                                @elseif($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Delivered'))
                                                    <button class="active-btn" title="Delivered">Delivered</button>
                                                @else
                                                    <button class="block-btn" title="Cancelled">Cancelled</button>
                                                @endif
                                            </td>
											<td class="action-btns">
											    <ul>
													<li class="detail_btn my-order-btns"><a href="#" title="View" data-rec="{{$aRec->sOrdr_Id}}" data-toggle="modal" data-target="#OrderModel"> View</a></li>
                                                    @if($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Pending'))
                                                        <li class="detail_btn my-order-btns"><a onclick="DelOrder('{{base64_encode($aRec->lOrder_IdNo)}}')">Deliverd</a></li>
                                                    @endif
												</ul>
											</td>
										</tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
							{{$oOrdLst->appends($request->all())->render()}}
                        </div>
                    </div>
					
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
$('#ExprtRcrd').on('click', function() {
    var sDelvDate       = $("input[name=sDelvDate]").val();
    var lSchlIdNo       = $("select[name=lSchlIdNo]").find(":selected").val();
    var lSchlIdNo       = lSchlIdNo == 'undefined' ? '' : lSchlIdNo;
    var nOrdrStatus     = $("select[name=nOrdrStatus]").find(":selected").val();
    var nOrdrStatus     = nOrdrStatus == 'undefined' ? '' : nOrdrStatus;
    window.location=APP_URL+"/milkbar_panel/my_orders/export?sDelvDate="+sDelvDate+"&lSchlIdNo="+lSchlIdNo+"&nOrdrStatus="+nOrdrStatus;
});

$('#OrderModel').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var aRec = button.data('rec');
	GetOrder(aRec);
});

function DelOrder(lOrderIdNo)
{
    if(confirm("Are you sure to deliverd to this order ?") == true)
    {
        window.location=APP_URL+"/milkbar_panel/my_orders/deliverd?lRecIdNo="+lOrderIdNo;
    }
}
</script>
