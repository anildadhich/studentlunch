@include('layouts.header')
<div class="Parent-Student-Registration section-padding bg-parent-2">
    <form action="{{url('registration/parent/save')}}" method="post" id="general_form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="row form-group form-main-div">
                <div class="col-3">&nbsp;</div>
                <div class="col-9 form-area">
                    @if(Session::has('Success'))
                    <div class="alert alert-success">
                        <strong>Success ! </strong> {{Session::get('Success')}}
                    </div>
                    @endif
                    @if(Session::has('Failed'))
                    <div class="alert alert-danger">
                        <strong>Failed ! </strong> {{Session::get('Failed')}}
                    </div>
                    @endif
                    <div class="switch-link">
                        <ul>
                            <li><a href="#" class="active">Parent/Student </a></li>
                            <li><a href="{{url('registration/milkbar')}}">Service Provider</a></li>
                        </ul>
                    </div>
                    <h3 class="form-heading">Parent/Student Registration</h3>
                    <div class="form-field-area">
                        <h3>Your Information</h3>
                        <div class="row form-group form-main-div">
                            <div class="col-6 box-div">
                                <label for="First_Name">First Name </label>
                                <input type="text" class="@error('sFrstName') is-invalid @enderror" name="sFrstName" value="{{ old('sFrstName') }}" onkeypress="return IsAlpha(event, this.value, '15')" required />
                                @error('sFrstName') <div class="invalid-feedback"><span>{{$errors->first('sFrstName')}}</span></div>@enderror
                            </div>
                            <div class="col-6 box-div">
                                <label for="Surname">Surname </label>
                                <input type="text" class="@error('sLstName') is-invalid @enderror" name="sLstName" value="{{ old('sLstName') }}" onkeypress="return IsAlpha(event, this.value, '15')" required />
                                @error('sLstName') <div class="invalid-feedback"><span>{{$errors->first('sLstName')}}</span></div>@enderror
                            </div>
                        </div>
                        <div class="row form-group form-main-div">
                            <div class="col-6 box-div">
                                <label for="Country">Country Name</label>
                                <select class="@error('lCntryIdNo') is-invalid @enderror" name="lCntryIdNo" id="lCntryIdNo" onchange="GetState(this.value)">
                                	<option value="">== Select Country ==</option>
                                    @foreach($aCntryLst as $aRec)
                                        <option {{ old('lCntryIdNo') == $aRec['lCntry_IdNo'] ? 'selected' : ''}} value="{{$aRec['lCntry_IdNo']}}" data-code="{{$aRec['sCntry_Code']}}">{{$aRec['sCntry_Name']}}</option>
                                    @endforeach
                                </select>
                                @error('lCntryIdNo') <div class="invalid-feedback"><span>{{$errors->first('lCntryIdNo')}}</span></div>@enderror
                            </div>
                            <div class="col-6 box-div">
                                <label for="State">State Name</label>
                                <select class="@error('lStateIdNo') is-invalid @enderror" name="lStateIdNo" id="lStateIdNo">
                                    <option value="">== Select State ==</option>
                                </select>
                                @error('lStateIdNo') <div class="invalid-feedback"><span>{{$errors->first('lStateIdNo')}}</span></div>@enderror
                            </div>
                        </div>
                        <div class="row form-group form-main-div">
                            <div class="col-6 box-div">
                                <label for="Contact_Mobile">Contact Mobile</label>
                                <div class="row">
                                    <div class="col-3 p-r-0"> <input type="text" class="cnoutry_code" name="sCntryCode" value="{{ old('sCntryCode') }}" readonly /></div>
                                    <div class="col-9 p-l-0"><input type="text" class="contact_number @error('sMobileNo') is-invalid @enderror" name="sMobileNo" value="{{ old('sMobileNo') }}" onkeypress="return IsNumber(event, this.value, '9')" required /></div>
                                </div>
                                @error('sMobileNo') <div class="invalid-feedback"><span>{{$errors->first('sMobileNo')}}</span></div>@enderror
                            </div>
                            <div class="col-6 box-div">
                                <label for="Relationship_to_Student">Relationship to Student</label>
                                <select id="Relationship_to_Student" class="@error('lRltnIdNo') is-invalid @enderror" name="lRltnIdNo" required>
                                    <option value="">==Select Relationship==</option>
                                    @foreach(config('constant.RLTN_IDNO') as $sRelName => $lRelIdNo)
                                        <option {{ old('lRltnIdNo') == $lRelIdNo ? 'selected' : ''}} value="{{$lRelIdNo}}">{{$sRelName}}</option>
                                    @endforeach
                                </select>
                                @error('lRltnIdNo') <div class="invalid-feedback"><span>{{$errors->first('lRltnIdNo')}}</span></div>@enderror
                            </div>
                        </div>
                        <div class="row form-group form-main-div">
                            <div class="col-12 box-div">
                                <label for="Email">Email Address</label>
                                <input type="text" class="@error('sEmailId') is-invalid @enderror" name="sEmailId" value="{{ old('sEmailId') }}" IsEmail='Yes' onkeypress="return LenCheck(event, this.value, '50')" required />
                                @error('sEmailId') <div class="invalid-feedback"><span>{{$errors->first('sEmailId')}}</span></div>@enderror
                            </div>
                        </div>
                        <div class="row form-group form-main-div">
                            <div class="col-6 box-div">
                                <label for="Suburb">Suburb </label>
                                <input type="text" class="@error('sSbrbName') is-invalid @enderror" name="sSbrbName" value="{{ old('sSbrbName') }}" onkeypress="return IsAlpha(event, this.value, '20')" required />
                                @error('sSbrbName') <div class="invalid-feedback"><span>{{$errors->first('sSbrbName')}}</span></div>@enderror
                            </div>
                            <div class="col-6 box-div">
                                <label for="Post_Code">Post Code </label>
                                <input type="text" class="@error('sPinCode') is-invalid @enderror" name="sPinCode" value="{{ old('sPinCode') }}" onkeypress="return IsNumber(event, this.value, '4')"  required />
                                @error('sPinCode') <div class="invalid-feedback"><span>{{$errors->first('sPinCode')}}</span></div>@enderror
                            </div>
                        </div>
                    </div>
                </div>
           </div>
           <div class="row form-group form-main-div">
                <div class="col-12 box-div child-login-details">
                    <h3>Child Details :</h3>
                    <div class="Add-School-Table">
                        <div style="overflow-x:auto;">
                            <table class="table-border">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>School Type</th>
                                        <th>School Name</th>
                                        <th>Child First Name</th>
                                        <th>Child Surname</th>
                                        <th>Class</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="Row_1">
                                        <td><i class="fa fa-plus" onclick="CrtRow()"></i></td>
                                        <td>
                                            <select name="nSchlType1" class="@error('nSchlType1') is-invalid @enderror" required>
                                                <option value="">Select School Type</option>
                                                @foreach(config('constant.SCHL_TYPE') as $sTypeName => $nType)
                                                    <option {{ old('nSchlType1') == $nType ? 'selected' : ''}} value="{{$nType}}">{{$sTypeName}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="lSchlIdNo1" class="@error('lSchlIdNo1') is-invalid @enderror" required>
                                                <option value="">Select School Name</option>
                                                @foreach($oSchlLst as $aRec)
                                                    <option {{ old('lSchlIdNo1') == $aRec['lSchl_IdNo'] ? 'selected' : ''}} value="{{$aRec['lSchl_IdNo']}}">{{$aRec['sSchl_Name']}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input type="text" class="@error('sFrstName1') is-invalid @enderror" name="sFrstName1" value="{{ old('sFrstName1') }}" onkeypress="return IsAlpha(event, this.value, '15')" required /></td>
                                        <td><input type="text" class="@error('sLstName1') is-invalid @enderror" name="sLstName1"  value="{{ old('sLstName1') }}" onkeypress="return IsAlpha(event, this.value, '15')" required /></td>
                                        <td><input type="text" class="@error('sClsName1') is-invalid @enderror" name="sClsName1" value="{{ old('sClsName1') }}" onkeypress="return IsAlphaNum(event, this.value, '4')" required /></td>
                                    </tr>
                                    <input type="hidden" name="nTtlRec" id="nTtlRec" value="1">
                                </tbody>
                            </table>
                        </div>
                   </div>
                   @error('sFrstName1') <div class="invalid-feedback"><span>{{$errors->first('sFrstName1')}}</span></div>@enderror
                   @error('sLstName1') <div class="invalid-feedback"><span>{{$errors->first('sLstName1')}}</span></div>@enderror
                   <h3 class="form-heading-2">Login Credentials</h3>
                   <div class="row form-group form-main-div">
                        <div class="col-6 box-div">
                            <label for="Password">Password </label>
                            <input type="password" class="@error('sLgnPass') is-invalid @enderror" name="sLgnPass" value="{{ old('sLgnPass') }}" onkeypress="return LenCheck(event, this.value, '16')" required />
                            @error('sLgnPass') <div class="invalid-feedback"><span>{{$errors->first('sLgnPass')}}</span></div>@enderror
                        </div>
                        <div class="col-6 box-div">
                            <label for="Password">Confirm Password </label>
                            <input type="password" class="@error('sCnfrmPass') is-invalid @enderror" name="sCnfrmPass" value="{{ old('sCnfrmPass') }}" onkeypress="return LenCheck(event, this.value, '16')" required />
                            @error('sCnfrmPass') <div class="invalid-feedback"><span>{{$errors->first('sCnfrmPass')}}</span></div>@enderror
                        </div>
                     </div>
                     <div class="row form-group form-main-div text-center">
                        <div class="custom-checkbox">
                            <label class="checkmarkcontainer">Accept Terms & Conditions
                                <input type="checkbox" name="nTerms">
                                <span class="checkmark @error('nTerms') is-invalid @enderror" ></span>
                            </label>
                            @error('nTerms') <div class="invalid-feedback"><span>{{$errors->first('nTerms')}}</span></div>@enderror
                       </div>
                        <div class="row form-group form-main-div text-center">
                            <button type="submit" class="btn-blue">REGISTER</button>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </form>
</div>
@include('layouts.footer')
<script type="text/javascript">
var nRowId;
function CrtRow() 
{
    var rowCount = $('tbody tr').length;
    if(rowCount == 5)
    {
        alert("Maximum 5 child allowed...");
    }
    else
    {
        total = $("#nTtlRec").val();
        next_no = parseInt(total)+1;
        newdiv = document.createElement('tr');
        divid = "Row_"+next_no;
        newdiv.setAttribute('id', divid);
        content = '';
        content += '<tr id="Row_'+next_no+'">';
        content += '<td><i class="fa fa-minus" onclick="DeleteRow('+next_no+')"></i></td>';
        content += '<td><select name="nSchlType'+next_no+'" required><option value="">Select School Type</option>@foreach(config('constant.SCHL_TYPE') as $sTypeName => $nType)<option  {{ old('nSchlType1') == $nType ? 'selected' : ''}} value="{{$nType}}">{{$sTypeName}}</option>@endforeach</select></td>';
        content += '<td><select name="lSchlIdNo'+next_no+'" required><option value="">Select School Name</option>@foreach($oSchlLst as $aRec)<option {{ old('lSchlIdNo1') == $aRec['lSchl_IdNo'] ? 'selected' : ''}} value="{{$aRec['lSchl_IdNo']}}">{{$aRec['sSchl_Name']}}</option>@endforeach</select></td>';
        content += '<td><input type="text" name="sFrstName'+next_no+'" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>';
        content += '<td><input type="text" name="sLstName'+next_no+'" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>';
        content += '<td><input type="text" name="sClsName'+next_no+'" onkeypress="return IsAlphaNum(event, this.value, 4)" required /></td>';
        content += '</tr>';
        newdiv.innerHTML = content;
        $("#nTtlRec").val(next_no);
        $("tbody").last().append(newdiv);
    }
}

function DeleteRow(nRow)
{
    if(confirm("Are you sure to delete this row") == true) {
        var row = $('#Row_'+nRow);
        row.remove();
    }
}
$(document).ready(function() {
    var lCntryIdNo = $('#lCntryIdNo').val();
    GetState(lCntryIdNo);
});
</script>