@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('parent_panel.layouts.side_panel')
            <main>
                <!-- My Commissions From -->
                @include('admin_panel.layouts.message')
                    <div class="page-breadcrumb">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="page-title">My Order</h4>
                            </div>
                        </div>
                    </div>
					<div class="container-fluid card-commission-section my-form my-credits-section">
						<form action="{{url('parent_panel/place_order/save')}}" method="post" id="general_form">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row first-block">
								<div class='col-sm-3 from-boxes'>
									<label>Date</label>
									<input type="date" id="sDtTm" name="sDtTm"  min="9/10/2018" required>
								</div>
								<div class='col-sm-3 from-boxes'>
									<label>Student Name</label>
									<select name="lChldIdNo" id="lChldIdNo" required>
									<option value="">Child Name</option>
										@foreach($aChldLst as $aChld)
											<option "@if($aCart != null) @if($aCart->lChldIdNo == $aChld['lChld_IdNo']) selected @endif @endif" data-schoolId='@if($aChld["nBlk_UnBlk"] == config("constant.STATUS.UNBLOCK")) 1 @else 0 @endif' data-school='{{$aChld["sSchl_Name"]}}' value='{{$aChld["lChld_IdNo"]}}'>{{$aChld['sFrst_Name']}} {{$aChld['sLst_Name']}}</option>
										@endforeach
									</select>
								</div>
								<div class='col-sm-3 from-boxes'>
									<label>Student School Name</label>
									<input type="text" name="sSchlName" id="sSchlName" required readonly> 
								</div>
								<div class='col-sm-3 from-boxes'>
									<label>Select Milk Bar</label>
									<select name="lMilkIdNo" id="lMilkIdNo" data-milk="@if(is_object($aCart)){{ $aCart->lMilkIdNo }} @endif" required>
										<option value="">Milk Bar</option>
									</select>
								</div>
							</div>
							<!-- Commssions Details Tabel -->
							<div class="row">
								<div class="col-sm-12 col-lg-12 commssions-table-details table-responsive lunch-order-form">
									<table id="orderTable">
										<thead>
											<tr>
												<th></th>
												<th>Item ID</th>
												<th>Category</th>
												<th>Item Description</th>
												<th>Ingredinets</th>
												<th>Quantity</th>
												<th class="text-right">Price</th>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 text-center">
									<div class="order-total mb-2 mt-5">Order Total Item $<label id="lTotalOrder">0.00</label></div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 services-btns">
									<ul class="m-auto text-center pt-4 pb-4">
										<li>
											<div class="add-btn d-none mt-0"><button title="Review Order" class="mt-0">Review Your Order</button></div>
										</li>
									</ul>
								</div>
							</div>
						</form>
                    </div>
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});
$( document ).ready(function() {
	$('#sDtTm').attr('min', new Date().toDateInputValue());
	
	$('#sDtTm').val(new Date().toDateInputValue());
    var optionSelected = $("option:selected", $('#lChldIdNo'));
	$('#sSchlName').val(optionSelected.data('school'));
	var milk = JSON.stringify(optionSelected.data('milk'));
	var schlId = optionSelected.data('schoolid');
	
	$.ajax({url: "parent_panel/get_milk?schl="+optionSelected.data('schoolid')+"&dttm="+ $('#sDtTm').val(), success: function(result){
		var oMlkBars = JSON.parse(result);
		if(schlId == 0){
			alert("The school has been blocked. Hence no Service Provider available.");
		}else{
			content = '<option value="">Milk Bar</option>';
			for(var i in oMlkBars) {
				var oMlkBar = oMlkBars[i];
				if($('#lMilkIdNo').data('milk') == oMlkBar['lMilk_IdNo'])
				{
					content += '<option selected value="'+oMlkBar['lMilk_IdNo']+'">'+oMlkBar['sBuss_Name']+'</option>';
				}else{
					content += '<option value="'+oMlkBar['lMilk_IdNo']+'">'+oMlkBar['sBuss_Name']+'</option>';
				}
			}
			$('#lMilkIdNo').html(content);
			
			var optionSelected = $("option:selected", $('#lMilkIdNo'));
			$.ajax({url: "parent_panel/get_menu?milkbar="+optionSelected.val(), success: function(result){
				var oMenuItems = JSON.parse(result);
				content = '';
				for(var i in oMenuItems) {
					var oMenuItem = oMenuItems[i];
					if(oMenuItem['selected']){
						content += '<tr> <td> <div class="checkbox-warning"> <label><input type="checkbox" name="lItemIdNo[]" checked value="'+oMenuItem['lItem_IdNo']+'" class="styled checkbox"></label> </div> </td>';
					}else{
						content += '<tr> <td> <div class="checkbox-warning"> <label><input type="checkbox" name="lItemIdNo[]" value="'+oMenuItem['lItem_IdNo']+'" class="styled checkbox"></label> </div> </td>';
					}
					content += '<td>'+oMenuItem['lItem_Unq_Id']+'</td> <td>';
					content += oMenuItem['sCatg_Name']+'</td>';
					content += ' <td>'+oMenuItem['sItem_Name']+'</td>';
					content += '<td>'+oMenuItem['sItem_Dscrptn']+'</td>';
					content += '<td><div id="field1">';
					if(oMenuItem["nItemQty"] === undefined){
						content += '<input type="text" id="lItemQty"  name="lItemQty'+oMenuItem["lItem_IdNo"]+'" value="1" min="1" max="10" readonly/>';
					}else{
						if(oMenuItem["nItemQty"] == 1){
							content += '<button type="button" id="sub" class="sub d-none">-</button>';
							content += '<input type="text" id="lItemQty"  name="lItemQty'+oMenuItem["lItem_IdNo"]+'" value="'+oMenuItem["nItemQty"]+'" min="1" max="10" readonly/>';
						}else{
							content += '<button type="button" id="sub" class="sub">-</button>';
							content += '<input type="text" id="lItemQty"  name="lItemQty'+oMenuItem["lItem_IdNo"]+'" value="'+oMenuItem["nItemQty"]+'" min="1" max="10" readonly/>';
						}
					}
					content += '<button type="button" id="add" class="add" >+</button>';
					content += '<input type="hidden" value="'+oMenuItem['sItem_Prc']+'"/>';
					if(oMenuItem["nItemQty"] === undefined){
						content += '<input class="total-price" type="hidden" value="0"  />';
					}else{
						content += '<input class="total-price" type="hidden" value="'+oMenuItem['sItem_Prc']*oMenuItem["nItemQty"]+'"  />';
					}
					content += '</div> </td>';
					if(oMenuItem["nItemQty"] === undefined){
						content += '<td class="text-right tot-price">'+parseFloat(oMenuItem['sItem_Prc']).toFixed(2)+'</td></tr>';
					}else{
						content += '<td class="text-right tot-price">'+parseFloat(oMenuItem['sItem_Prc']*oMenuItem["nItemQty"]).toFixed(2)+'</td></tr>';
					}
				}
				$('#orderTable tbody').html(content);
				totalOrder();
			}});
		}
	}});
});

$('#lChldIdNo').on('change', function (event) {
	var optionSelected = $("option:selected", this);
	$('#sSchlName').val(optionSelected.data('school'));
	if(optionSelected.val() != '' && $('#sDtTm').val() != ''){
		$.ajax({url: "parent_panel/get_milk?schl="+optionSelected.data('schoolid')+"&dttm="+ $('#sDtTm').val(), success: function(result){
			var oMlkBars = JSON.parse(result);
			if(optionSelected.data('schoolid') == 0){
				alert("The school has been blocked. Hence no Service Provider available.");
			}else{
				content = '<option value="">Milk Bar</option>';
				for(var i in oMlkBars) {
					var oMlkBar = oMlkBars[i];
					content += '<option value="'+oMlkBar['lMilk_IdNo']+'">'+oMlkBar['sBuss_Name']+'</option>';
				}
				$('#lMilkIdNo').html(content);
			}
			
		}});
	}
	else{
		$('#lMilkIdNo').html('<option value="">Milk Bar</option>');
		$('#orderTable tbody').html('');
	}
});

$('#lMilkIdNo').on('change', function (event) {
	var optionSelected = $("option:selected", this);
	$.ajax({url: "parent_panel/get_menu?milkbar="+optionSelected.val(), success: function(result){
		var oMenuItems = JSON.parse(result);
		content = '';
		for(var i in oMenuItems) {
			var oMenuItem = oMenuItems[i];
			content += '<tr> <td> <div class="checkbox-warning"> <label><input type="checkbox" name="lItemIdNo[]" value="'+oMenuItem['lItem_IdNo']+'" class="styled checkbox"></label> </div> </td>';
            content += '<td>'+oMenuItem['lItem_Unq_Id']+'</td> <td>';
			content += oMenuItem['sCatg_Name']+'</td>';
			content += ' <td>'+oMenuItem['sItem_Name']+'</td>';
			content += '<td>'+oMenuItem['sItem_Dscrptn']+'</td>';
			content += '<td><div id="field1">';
			content += '<button type="button" id="sub" class="sub d-none">-</button>';
			content += '<input type="text" id="lItemQty" class="lItemQty"  name="lItemQty'+oMenuItem["lItem_IdNo"]+'" value="1" min="1" max="10" readonly/>';
			content += '<button type="button" id="add" class="add" >+</button>';
			content += '<input type="hidden" value="'+oMenuItem['sItem_Prc']+'" min="1" max="100"  />';
			content += '<input class="total-price" type="hidden" value="0" min="1" max="100"  />';
			content += '</div> </td>';
			content += '<td class="text-right tot-price">'+parseFloat(oMenuItem['sItem_Prc']).toFixed(2)+'</td></tr>';
		}
		$('#orderTable tbody').html(content);
	}});
});  
$('#orderTable tbody').on('click', '.add', function() {
	$(this).closest('tr').find(".checkbox").prop("checked", true);
	if ($(this).prev().val() < 10) {
		$(this).prev().val(+$(this).prev().val() + 1);
		if($(this).prev().prev().hasClass('d-none')){
			$(this).prev().prev().removeClass('d-none');
		}
		var nItemPrice = $(this).next().val();
		var lItemQty = $(this).prev().val();
		$(this).next().next().val(+(nItemPrice*lItemQty).toFixed(2));
		$(this).closest('td').next().html((nItemPrice*lItemQty).toFixed(2));
		totalOrder();
	}
});

$('#orderTable tbody').on('click', '.sub', function() {
	if ($(this).next().val() > 1) {
		if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		if($(this).next().val() == 1){
			$(this).addClass('d-none');
		}
		var nItemPrice = $(this).next().next().next().val();
		var lItemQty = $(this).next().val();
		$(this).next().next().next().next().val(+(nItemPrice*lItemQty).toFixed(2));
		$(this).closest('td').next().html((nItemPrice*lItemQty).toFixed(2));
		totalOrder();
	}
});

$('#orderTable tbody').on('click', '.checkbox', function() {
	if($(this).is(':checked')){
		$(this).closest('tr').find(".lItemQty").next().next().next().val($(this).closest('tr').find(".lItemQty").next().next().val());
	}else {
		$(this).closest('tr').find(".lItemQty").val("1");
		$(this).closest('tr').find(".lItemQty").prev().addClass('d-none');
		$(this).closest('tr').find(".lItemQty").next().next().next().val('0');
		$(this).closest('tr').find(".tot-price").html($(this).closest('tr').find(".lItemQty").next().next().val());
	}
	totalOrder();
});

function totalOrder(){
	var sum = 0;
	$(".total-price").each(function(){
		sum += parseFloat($(this).val());
	});
	if(sum == 0){
		$('.add-btn').addClass('d-none');
	}
	else
	{
		$('.add-btn').removeClass('d-none');
	}
	$('.order-total').html("Order Total Item $ "+sum.toFixed(2));
}

</script>