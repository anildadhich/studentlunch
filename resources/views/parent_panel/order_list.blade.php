@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('parent_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Manage Orders</h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section">
                    <form action="{{url('parent_panel/manage_order')}}" method="get">
                        <div class="row first-block">
                            <div class='col-sm-3 from-boxes'>
                                <label>Transaction Date</label>
                                <input type="date" name="sCrtDtTm" placeholder="MM/DD/YYYY" value="{{ $request['sCrtDtTm'] }}"> 
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>Order Status</label>
                                <select name="nOrdrStatus">
                                    <option value="">All Order</option>
                                    @foreach(config('constant.ORDER_STATUS') as $sStatusName => $nOrdrStatus)
                                        <option {{ $request['nOrdrStatus'] == $nOrdrStatus ? 'selected' : ''}} value="{{$nOrdrStatus}}">{{$sStatusName}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button type="submit" title="Filter">Filter</button></li>
                                    <li><button type="button" class="mr-0" title="Export Order" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details table-responsive parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Order ID</th>
									<th>Delivery Date</th>
									<th>Student Name</th>
									<th>Milk Bar Name</th>
									<th class="text-right">Price</th>
									<th class="text-center">Status</th>
									<th>Transaction Date</th>
									<th>Action</th>
                                </tr>
								@if(count($oOrderLst) > 0)
                                    @foreach($oOrderLst As $aRec)
										<tr>
											<td>{{$aRec->sOrdr_Id}}</td>
											<td>{{date('d M, Y', strtotime($aRec->sDelv_Date))}}</td>
											<td>{{$aRec->sFrst_Name}} {{$aRec->sLst_Name}}</td>
											<td>{{$aRec->sBuss_Name}}</td>
											<td class="text-right">${{$aRec->sGrnd_Ttl}}</td>
											<td class="text-center">
    											@if($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Pending'))
                                                    <button class="primary-btn" title="Pending">Pending</button>
                                                @elseif($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Delivered'))
                                                    <button class="active-btn" title="Delivered">Delivered</button>
                                                @else
                                                    <button class="block-btn" title="Cancelled">Cancelled</button>
    											@endif
                                            </td>
											<td>{{date('d M, Y h:i A', strtotime($aRec->sCrt_DtTm))}}</td>
											<td class="action-btns">
												<ul>
													<li class="detail_btn my-order-btns"><a href="#" title="View" data-rec="{{$aRec->sOrdr_Id}}" data-toggle="modal" data-target="#OrderModel"> View</a></li>
													@if($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Pending'))
													   <li class="detail_btn my-order-btns"><a href="{{url('parent_panel/cancel_order')}}?lRecIdNo={{base64_encode($aRec->lOrder_IdNo)}}">Cancel</a></li>
													@endif
												</ul>
											</td>
										</tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="8" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
							{{$oOrderLst->appends($request->all())->render()}}
                            
                        </div>
                    </div>
					
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')

<script>
$('#OrderModel').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var aRec = button.data('rec');
	GetOrder(aRec);
});

$('#ExprtRcrd').on('click', function() {
    var sCrtDtTm        = $("input[name=sCrtDtTm]").val();
    var nOrdrStatus     = $("select[name=nOrdrStatus]").find(":selected").val();
    var nOrdrStatus     = nOrdrStatus == 'undefined' ? '' : nOrdrStatus;
    window.location=APP_URL+"/parent_panel/manage_order/export?sCrtDtTm="+sCrtDtTm+"&nOrdrStatus="+nOrdrStatus;
});
</script>