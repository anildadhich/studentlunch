@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('parent_panel.layouts.side_panel')
            <form action="{{url('parent_panel/manage_account/save')}}" method="post" id="general_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="lPrntIdNo" id="lPrntIdNo" value="{{base64_encode($aPrntsDtl['lPrnt_IdNo'])}}">
                <main>
                    <div class="page-breadcrumb">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="page-title">Manage Account</h4>
                            </div>
                        </div>
                    </div>
                    @include('admin_panel.layouts.message')
                    <div class="container-fluid card-commission-section  parent-details-section">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div>
                                    <h4>Basic Information</h4>
                                    <h5>Change all information below</h5>
                                </div>
                            </div>
                        </div>
                        <form>
                            <div class="row  account-form">
                                <div class="col">
                                    <label>Account ID</label>
                                    <input type="text" class="form-control" value="{{$aPrntsDtl['sAcc_Id']}}" readonly>
                                </div>
                                <div class="col">
                                    <label>Relationship with Student</label>
                                    <select class="form-control @error('lRltnIdNo') is-invalid @enderror" name="lRltnIdNo" required>
                                    <option value="">==Select Relationship==</option>
                                    @foreach(config('constant.RLTN_IDNO') as $sRelName => $lRelIdNo)
                                        <option {{ old('lRltnIdNo', $aPrntsDtl['lRltn_IdNo']) == $lRelIdNo ? 'selected' : ''}} value="{{$lRelIdNo}}">{{$sRelName}}</option>
                                    @endforeach
                                </select>
                                @error('lRltnIdNo') <div class="invalid-feedback"><span>{{$errors->first('lRltnIdNo')}}</span></div>@enderror
                                </div>
                            </div>
                            <div class="row account-form">
                                <div class="col">
                                    <label>First  Name</label>
                                    <input type="text" class="form-control @error('sFrstName') is-invalid @enderror" name="sFrstName" value="{{ old('sFrstName', $aPrntsDtl['sFrst_Name']) }}" onkeypress="return IsAlpha(event, this.value, '15')" required />
                                    @error('sFrstName') <div class="invalid-feedback"><span>{{$errors->first('sFrstName')}}</span></div>@enderror
                                </div>
                                <div class="col">
                                    <label>Surname </label>
                                    <input type="text" class="form-control @error('sLstName') is-invalid @enderror" name="sLstName" value="{{ old('sLstName', $aPrntsDtl['sLst_Name']) }}" onkeypress="return IsAlpha(event, this.value, '15')" required />
                                    @error('sLstName') <div class="invalid-feedback"><span>{{$errors->first('sLstName')}}</span></div>@enderror
                                </div>
                            </div>
                            <div class="row account-form">
                                <div class="col">
                                    <label>Country</label>
                                    <select class="form-control @error('lCntryIdNo') is-invalid @enderror" name="lCntryIdNo" id="lCntryIdNo" onchange="GetState(this.value)" required>
                                        <option value="">== Select Country ==</option>
                                        @foreach($aCntryLst as $aRec)
                                            <option {{ old('lCntryIdNo', $aPrntsDtl['lCntry_IdNo']) == $aRec['lCntry_IdNo'] ? 'selected' : ''}} value="{{$aRec['lCntry_IdNo']}}" data-code="{{$aRec['sCntry_Code']}}">{{$aRec['sCntry_Name']}}</option>
                                        @endforeach
                                    </select>
                                    @error('lCntryIdNo') <div class="invalid-feedback"><span>{{$errors->first('lCntryIdNo')}}</span></div>@enderror
                                </div>
                                <div class="col">
                                    <label>State</label>
                                    <select class="form-control @error('lStateIdNo') is-invalid @enderror" name="lStateIdNo" id="lStateIdNo" required>
                                        <option value="">== Select State ==</option>
                                        @foreach($aStateLst as $aRec)
                                            <option {{ old('lStateIdNo', $aPrntsDtl['lState_IdNo']) == $aRec['lState_IdNo'] ? 'selected' : ''}} value="{{$aRec['lState_IdNo']}}">{{$aRec['sState_Name']}}</option>
                                        @endforeach
                                    </select>
                                    @error('lStateIdNo') <div class="invalid-feedback"><span>{{$errors->first('lStateIdNo')}}</span></div>@enderror
                                </div>
                            </div>
                            <div class="row account-form">
                                <div class="col">
                                    <label>Post Code</label>
                                    <input type="text" class="form-control @error('sPinCode') is-invalid @enderror" name="sPinCode" value="{{ old('sPinCode', $aPrntsDtl['sPin_Code']) }}" onkeypress="return IsNumber(event, this.value, '4')" required />
                                    @error('sPinCode') <div class="invalid-feedback"><span>{{$errors->first('sPinCode')}}</span></div>@enderror
                                </div>
                                @php
                                $aMobileNo = explode(" ",$aPrntsDtl['sMobile_No']);
                                @endphp
                                <div class="col">
                                    <label>Mobile Number </label>
                                    <div class="row">
                                        <div class="col-lg-2 p-l-15">
                                            <input type="text" class="form-control cnoutry_code" name="sCntryCode" value="{{ old('sFrstName', $aMobileNo[0]) }}" readonly />    
                                        </div>
                                        <div class="col-lg-10  p-r-15">
                                            <input type="text" class="form-control @error('sMobileNo') is-invalid @enderror" name="sMobileNo" value="{{ old('sMobileNo', $aMobileNo[1]) }}" onkeypress="return IsNumber(event, this.value, '9')" required />    
                                        </div>
                                    </div>
                                    @error('sFrstName') <div class="invalid-feedback"><span>{{$errors->first('sFrstName')}}</span></div>@enderror
                                </div>
                            </div>
                            <div class="row account-form">
                                <div class="col">
                                    <label>Suburb</label>
                                    <input type="text" class="form-control @error('sSbrbName') is-invalid @enderror" name="sSbrbName" value="{{ old('sSbrbName', $aPrntsDtl['sSbrb_Name']) }}" onkeypress="return IsAlpha(event, this.value, '20')" required />
                                    @error('sSbrbName') <div class="invalid-feedback"><span>{{$errors->first('sSbrbName')}}</span></div>@enderror
                                </div>
                                <div class="col">
                                    <label>Email </label>
                                    <input type="text" class="form-control" value="{{ $aPrntsDtl['sEmail_Id'] }}" readonly />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="container-fluid card-commission-section  parent-details-section">
                        <div class="row">
                            <div class="col-sm-3 col-lg-6">
                                <div>
                                    <h4>Child Details</h4>
                                    <h5>Change all information below</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-12 school-service-table">
                                    <table style="border: 1px red solid; width: 100%; " id="display-table">
                                        <tr>
                                            <th></th>
                                            <th>School Type</th>
                                            <th>School Name</th>
                                            <th>Child First Name</th>
                                            <th>Child Surname</th>
                                            <th>Class</th>
                                        </tr>
                                        <tbody>
                                            @php
                                            $i = 1;
                                            @endphp
                                            @foreach($aChldLst as $aRes)
                                                <input type="hidden" name="lChldIdNo{{$i}}" value="{{$aRes['lChld_IdNo']}}"/>
                                                <tr id="Row_{{$i}}">
                                                    @if($i == 1)
                                                        <td><i class="fa fa-plus" onclick="CrtRow()"></i></td>
                                                    @else
                                                        <td><i class="fa fa-minus" onclick="DeleteRow('{{$i}}')"></i></td>
                                                    @endif
                                                    <td>
                                                        <select name="nSchlType{{$i}}" class="form-control">
                                                            <option value="">School Type</option>
                                                             @foreach(config('constant.SCHL_TYPE') as $sTypeName => $nType)
                                                                <option {{ $aRes['nSchl_Type'] == $nType ? 'selected' : ''}} value="{{$nType}}">{{$sTypeName}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="lSchlIdNo{{$i}}" class="form-control">
                                                            <option value="">Child School Name</option>
                                                            @foreach($aSchlLst as $aRec)
                                                                <option {{ $aRes['lSchl_IdNo'] == $aRec['lSchl_IdNo'] ? 'selected' : ''}} value="{{$aRec['lSchl_IdNo']}}">{{$aRec['sSchl_Name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input type="text" class="form-control" name="sFrstName{{$i}}" value="{{$aRes['sFrst_Name']}}" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>
                                                    <td><input type="text" class="form-control" name="sLstName{{$i}}" value="{{$aRes['sLst_Name']}}" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>
                                                    <td><input type="text" class="form-control" name="sClsName{{$i}}"  value="{{$aRes['sCls_Name']}}" onkeypress="return IsAlphaNum(event, this.value, 4)" required /></td>
                                                </tr>
                                                @php
                                                $i++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="nTtlRec" id="nTtlRec" value="{{count($aChldLst)}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 services-btns">
                                <ul class="m-auto text-center pt-4 pb-4">
                                    <li>
                                        <div class="add-btn  mt-0"><button title="Back" class="mt-0" onclick="history.back()">Back</button></div>
                                    </li>
                                    <li>
                                        <div class="add-btn  mt-0"><button title="Update Account" class="mt-0">Update</button></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </main>
            </form>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
function CrtRow()
{
    var rowCount = $('#display-table tbody tr').length-1;
    if(rowCount == 5)
    {
        alert("Maximum 5 childs allowed...");
    }
    else
    {
        total = $("#nTtlRec").val();
        next_no = parseInt(total)+1;
        newdiv = document.createElement('tr');
        divid = "Row_"+next_no;
        newdiv.setAttribute('id', divid);
        content = '';
        content += '<tr id="Row_'+next_no+'">';
        content += '<td><i class="fa fa-minus" onclick="DeleteRow('+next_no+')"></i></td>';
        content += '<td><select name="nSchlType'+next_no+'" class="form-control"><option value="">School Type</option>@foreach(config('constant.SCHL_TYPE') as $sTypeName => $nType)<option value="{{$nType}}">{{$sTypeName}}</option>  @endforeach</select></td>';
        content += '<td><select name="lSchlIdNo'+next_no+'" class="form-control"><option value="">Child School Name</option>@foreach($aSchlLst as $aRec)<option value="{{$aRec['lSchl_IdNo']}}">{{$aRec['sSchl_Name']}}</option>@endforeach</select></td>';
        content += '<td><input type="text" class="form-control" name="sFrstName'+next_no+'" value="" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>';
        content += '<td><input type="text" class="form-control" name="sLstName'+next_no+'" value="" onkeypress="return IsAlpha(event, this.value, 15)" required /></td>';
        content += '<td><input type="text" class="form-control" name="sClsName'+next_no+'"  value="" onkeypress="return IsAlphaNum(event, this.value, 4)" required /></td>';
        content += '</tr>';
        newdiv.innerHTML = content;
        $("#nTtlRec").val(next_no);
        $("#display-table tbody").last().append(newdiv);
    }
}

function DeleteRow(nRow)
{
    if(confirm("Are you sure to delete this row") == true) {
        var row = $('#Row_'+nRow);
        row.remove();
    }
}
</script>