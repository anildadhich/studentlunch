@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('parent_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <div class="welcome-box">
                                <div class="welcome-box1">
                                    <div class="left-text-holder">
                                        <h4>Welcome Back !</h4>
                                        <h5>Parents Dashboard</h5>
                                    </div>
                                    <div class="right-img-holder">
                                        <img src="assets/images/user2.png" style="max-width: 82%;">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="welcome-box2">
                                    <div class="student-lunch-img">
                                        <img src="assets/images/user4.png">
                                    </div>
                                    <div class="heading-text">
                                        <h3>{{session('USER_NAME')}}</h3>
                                    </div>
                                    <div class="view_profile_btn"><a href="{{url('parent_panel/manage_account')}}"> View Profile </a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-7 pl-0">
                            <div class="row">
                                    <div class="col-4">
                                        <div class="boxes_dash">
                                            <div class="total-div">
                                                <h5>Children</h5>
                                                <p>{{ $aCntChld['nTtlRec'] }}</p>
                                            </div>
                                            <div class="icon-div"><i class="fa fa-user"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="boxes_dash">
                                            <div class="total-div">
                                                <h5>Total Order</h5>
                                                <p>{{ $aTtlOrd['nTtlRec'] }}</p>
                                            </div>
                                            <div class="icon-div"><i class="fa fa-bars"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="boxes_dash">
                                            <div class="total-div">
                                                <h5>Pending Orders</h5>
                                                <p>{{ $aPndgOrd['nTtlRec'] }}</p>
                                            </div>
                                            <div class="icon-div"><i class="fa fa-bars"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="boxes_dash">
                                            <div class="total-div">
                                                <h5>Completed Orders</h5>
                                                <p>{{ $aDlvrdOrd['nTtlRec'] }}</p>
                                            </div>
                                            <div class="icon-div"><i class="fa fa-bars"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="boxes_dash">
                                            <div class="total-div">
                                                <h5>Cancelled Orders</h5>
                                                <p>{{ $aTtlOrd['nTtlRec'] - ($aPndgOrd['nTtlRec'] + $aDlvrdOrd['nTtlRec']) }}</p>
                                            </div>
                                            <div class="icon-div"><i class="fa fa-bars"></i></div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="generate-report generate-report2" style="padding-bottom: 220px;">
                                <h4>Generate Report</h4>
                                <ul>
                                    <li>
                                        <input type="radio" id="f-option" name="selector">
                                        <label for="f-option"> 30 Days                                       </label>
                                        <div class="check"></div>
                                    </li>
                                    <li>
                                        <input type="radio" id="s-option" name="selector">
                                        <label for="s-option">60 Days</label>
                                        <div class="check">
                                            <div class="inside"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="radio" id="t-option" name="selector">
                                        <label for="t-option">90 Days</label>
                                        <div class="check">
                                            <div class="inside"></div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                                <form>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Start Date</label>
                                        <input type="date" name="date" placeholder="MM/DD/YYYY"> 
                                        
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect2"> End Date</label>
                                        <input type="date" name="date" placeholder="MM/DD/YYYY"> 
                                        
                                    </div>
                                    <div class="form-group" style="float: none;">
                                        <label>Milk Bar Name</label>
                                        <select>
                                            <option>Willow Avenue Milk Bar</option>
                                            <option>Synday Milk Bar</option>
                                        </select>
                                    </div>
                                </form>
                                <div class='form-btns text-center'>
                                    <ul>
                                        <li><button>Submit</button></li>
                                        <li><button class="mr-0">Download To Excel</button></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="generate-report">
                                <div class="text-center">{!! $chart->html() !!}</div>
								<div class="colors">
                                    <ul>
									@foreach($aLbl as $key => $sMnth)
                                        <li>
                                            <div class="color_box1" style="background-color: {{$aClrs[$key]}} !important;"></div>
                                            <div class="colors-text">
                                                <p>{{$sMnth}} {{$aValue[$key]}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
{!! $chart->script() !!}