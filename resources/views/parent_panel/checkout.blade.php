@include('admin_panel.layouts.header')
<style type="text/css">
	.panel-title {
	display: inline;
	font-weight: bold;
	}
	.display-table {
		display: table;
	}
	.display-tr {
		display: table-row;
	}
	.display-td {
		display: table-cell;
		vertical-align: middle;
		width: 61%;
	}
	.col-md-12.error.form-group.hide {
		display: none;
	}
</style>
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('parent_panel.layouts.side_panel')
            <main>
                <!-- My Commissions From -->
                @include('admin_panel.layouts.message')
                <div class="page-breadcrumb">
					<div class="row">
						<div class="col-6">
							<h4 class="page-title">Checkout</h4>
						</div>
					</div>
				</div>
				<!-- My Commissions From -->
				<div class="container-fluid">
					<div class="row">
						<div class="col-6 col-lg-6 col-sm-6 payment-box">
							<div class="payment-box-inner">
								<h4 class="mb-0">Available Credits: ${{$nTtlCrdt}}</h4>
								<h4 class="mb-0">Credits against this provider: ${{$sMlkCrdt}}</h4>
							</div>
							<div class="payment-box-inner">
								<h4 class="mb-0 pb-4">Details</h4>
								<h4>{{$aPrntsDtl['sFrst_Name']}} {{$aPrntsDtl['sLst_Name']}}</h4>
								<p>Price ({{ $qty }} Items)   ${{ (config('constant.GST') * $total) +  $total}}</p>
							</div>
							<div class="payment-box-inner">
								<h4 class="mb-0 pb-4">Delivery Address</h4>
								<h4>School</h4>
								<p>{{ $aSchlDtl['sSchl_Name'] }}, {{ $aSchlDtl['sStrt_No'] }} {{ $aSchlDtl['sStrt_Name'] }} {{ $aSchlDtl['sSbrb_Name'] }} {{ $aSchlDtl['sState_Name'] }} {{ $aSchlDtl['sCntry_Name'] }} {{ $aSchlDtl['sPin_Code'] }} </p>
							</div>
						</div>
						<div class="col-6 col-lg-6 col-sm-6 payment-box">
							<div class="payment-box-inner">
								@if(((config('constant.GST') * $total) +  $total) > $sMlkCrdt)
								<form role="form" action="{{ route('checkout.post') }}" method="post" class="require-validation"
																 data-cc-on-file="false"
																data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
																id="payment-form">
									@csrf
			  
									<div class='form-row row'>
										<div class='col-xs-12 form-group required'>
											<label class='control-label'>Name on Card</label> <input
												class='form-control' size='4' type='text' onkeypress="return LenCheck(event, this.value, '50')">
										</div>
									</div>
									<input class='form-control' size='4' type='hidden' name="sAmount" value="{{ (config('constant.GST') * $total) +  $total }}">
									<div class='form-row row'>
										<div class='col-xs-12 form-group card required'>
											<label class='control-label'>Card Number</label> <input
												autocomplete='off' class='form-control card-number' size='20'
												type='text' onkeypress="return IsNumber(event, this.value, '16')">
										</div>
									</div>
			  
									<div class='form-row row'>
										<div class='col-xs-12 col-md-4 form-group cvc required'>
											<label class='control-label'>CVC</label> <input autocomplete='off'
												class='form-control card-cvc' placeholder='ex. 311' size='4'
												type='text' onkeypress="return IsNumber(event, this.value, '3')">
										</div>
										<div class='col-xs-12 col-md-4 form-group expiration required'>
											<label class='control-label'>Expiration Month</label> <input
												class='form-control card-expiry-month' placeholder='MM' size='2'
												type='text' onkeypress="return IsNumber(event, this.value, '2')">
										</div>
										<div class='col-xs-12 col-md-4 form-group expiration required'>
											<label class='control-label'>Expiration Year</label> <input
												class='form-control card-expiry-year' placeholder='YYYY' size='4'
												type='text' onkeypress="return IsNumber(event, this.value, '4')">
										</div>
									</div>
			  
									<div class='form-row row'>
										<div class='col-md-12 error form-group hide'>
											<div class='alert-danger alert'>Please correct the errors and try
												again.</div>
										</div>
									</div>
			  
									<div class="row">
										<div class="col-xs-12">
											<button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now (${{ ((config('constant.GST') * $total) +  $total) - $sMlkCrdt }})</button>
										</div>
									</div>
									  
								</form>
								@else
								<form role="form" action="{{ route('checkout.crpost') }}" method="post" >
									@csrf
			  
									<div class="row">
										<div class="col-xs-12">
											<button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now (${{ (config('constant.GST') * $total) +  $total }})</button>
										</div>
									</div>
									  
								</form>	
								@endif
							</div>
						</div>
					</div>
				</div>
			</main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file') && $('.card-cvc').val() != '') {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});
</script>