@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('admin_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Manage Milk Bar</h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section">
                    <form action="{{url('admin_panel/milk_bar/list')}}" method="get">
                        <div class="row first-block parent-list-form">
                            <div class='col-sm-3'>
                                <label>Milk Bar Name</label>
                                <input type="text" name="sBussName" placeholder="Milk Bar Name" value="{{$request['sBussName']}}">
                            </div>
                            <div class='col-sm-3'>
                                <label>Mobile Number</label>
                                <input type="text" name="sMobileNo" placeholder="Mobile Number" value="{{$request['sMobileNo']}}">
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button type="submit" title="Filter">Filter</button></li>
                                    <li><button class="mr-0" type="button" title="Export To Excel" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                            <div class='col-sm-3 text-right'>
                                <div class="add-btn">
                                    <a href="{{url('admin_panel/milk_bar/manage')}}">
                                        <button title="Add Milk Bar" type="button">Add Milk Bar</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details table-responsive parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Account ID</th>
                                    <th>Milk Bar Name </th>
                                    <th>Mobile Number</th>
                                    <th>Create Date</th>
                                    <th>Admin Status</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                @if(count($aMlkBarLst) > 0)
                                    @foreach($aMlkBarLst As $aRec)
                                    <tr>
                                        <td>{{$aRec->sAcc_Id}}</td>
                                        <td>{{$aRec->sBuss_Name}}</td>
                                        <td>{{$aRec->sMobile_No}}</td>
                                        <td>{{date('d F, Y', strtotime($aRec->sCrt_DtTm))}}</td>
                                        <td>
                                            @if($aRec->nAdmin_Status == config('constant.MLK_STATUS.UNACTIVE'))
                                                <button class="block-btn" title="Inactive" onclick="ActvStatus('{{base64_encode($aRec->lMilk_IdNo)}}')">Inactive</button>
                                            @else
                                                <button class="active-btn" title="Active">Active</button>
                                            @endif
                                        </td>
                                        <td>
                                            @if($aRec->nBlk_UnBlk == config('constant.STATUS.BLOCK'))
                                                <button class="block-btn" title="Block" onclick="chngStatus('{{base64_encode('mst_milk_bar')}}','{{base64_encode('lMilk_IdNo')}}','{{base64_encode($aRec->lMilk_IdNo)}}','{{base64_encode(config('constant.STATUS.UNBLOCK'))}}')">Block</button>
                                            @else
                                                <button class="active-btn" title="Unblock" onclick="chngStatus('{{base64_encode('mst_milk_bar')}}','{{base64_encode('lMilk_IdNo')}}','{{base64_encode($aRec->lMilk_IdNo)}}','{{base64_encode(config('constant.STATUS.BLOCK'))}}')">Unblock</button>
                                            @endif
                                        </td>
                                        <td class="action-btns">
                                            <ul>
                                                <li><a href="{{url('admin_panel/milk_bar/detail')}}?lRecIdNo={{base64_encode($aRec->lMilk_IdNo)}}" title="View {{$aRec->sBuss_Name}}"> <i class="fa fa-eye"></i></a></li>
                                                <li><a href="{{url('admin_panel/milk_bar/manage')}}?lRecIdNo={{base64_encode($aRec->lMilk_IdNo)}}" title="Edit {{$aRec->sBuss_Name}}"> <i class="fa fa-edit"></i></a></li>
                                                <li><i class="fa fa-trash" onclick="DelRec('{{base64_encode('mst_milk_bar')}}','{{base64_encode('lMilk_IdNo')}}','{{base64_encode($aRec->lMilk_IdNo)}}')" title="Delete {{$aRec->sBuss_Name}}"></i></li>
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            {{$aMlkBarLst->appends($request->all())->render()}}
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
$('#ExprtRcrd').on('click', function() {
    var sBussName = $("input[name=sBussName]").val();
    var sMobileNo = $("input[name=sMobileNo]").val();
    window.location=APP_URL+"/admin_panel/milk_bar/export?sBussName="+sBussName+"&sMobileNo="+sMobileNo;
});
</script>