<div class="sidebar">
    <div class="logo">
        <a href="{{url('/admin_panel')}}">
        <span class="logo-full">Students Lunch</span>
        <span class="tag_line">As Nuture Teaches us</span>
        </a>
        <div class="sidebar-close-icon">
            <h4>SL</h4>
        </div>
    </div>
    <ul id="sidebarCookie">
        <li class="nav-item dashbord-itme">
            <a class="nav-link wave-effect collapsed wave-effect {{ Request::is('admin_panel') ? 'active' : '' }}" href="{{url('admin_panel')}}" >
            <i class="fa fa-th-large"></i>
            <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect"  href="/admin_panel">
            <i class="fa fa-user-circle-o" aria-hidden="true"></i>
            <span class="menu-title">Manage Account</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect {{ (Request::is('admin_panel/milk_bar/list*') || Request::is('admin_panel/milk_bar/manage*') || Request::is('admin_panel/milk_bar/detail*')) ? 'active' : '' }}" href="{{url('admin_panel/milk_bar/list')}}">
            <i class="fa fa-cutlery"></i>
            <span class="menu-title">Manage Milk Bar</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect {{ (Request::is('admin_panel/school/list*') || Request::is('admin_panel/school/manage*') || Request::is('admin_panel/school/detail*')) ? 'active' : '' }}" href="{{url('admin_panel/school/list')}}">
            <i class="fa fa-building"></i>
            <span class="menu-title">Manage School</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect {{ (Request::is('admin_panel/parent/list*') || Request::is('admin_panel/parent/detail*')) ? 'active' : '' }}"  href="{{url('admin_panel/parent/list')}}">
            <i class="fa fa-users" aria-hidden="true"></i>
            <span class="menu-title">Manage Parent</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect {{ Request::is('admin_panel/manage_order*') ? 'active' : '' }}" href="{{url('admin_panel/manage_order')}}">
            <i class="fa fa-sort"></i>
            <span class="menu-title">Manage Orders</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect {{ Request::is('admin_panel/manage_commission*') ? 'active' : '' }}" href="{{url('admin_panel/manage_commission')}}">
            <i class="fa fa-usd"></i>
            <span class="menu-title">My Commissions</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect {{ Request::is('admin_panel/change_password*') ? 'active' : '' }}" href="{{url('admin_panel/change_password')}}">
            <i class="fa fa-lock"></i>
            <span class="menu-title">Change Password</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link wave-effect collapsed wave-effect" href="{{url('admin_panel/logout')}}" 
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i>
            <span class="menu-title">Log Out</span>
            </a>
            <form id="logout-form" action="{{ url('admin_panel/logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</div>