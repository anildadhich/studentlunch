@if(Session::has('Success'))
<div class="alert alert-success">
    <strong>Success ! </strong> {{Session::get('Success')}}
</div>
@endif
@if(Session::has('Failed'))
<div class="alert alert-danger">
    <strong>Failed ! </strong> {{Session::get('Failed')}}
</div>
@endif
@if(Session::has('Alert'))
<div class="alert alert-warning">
    <strong>Alert ! </strong> {{Session::get('Alert')}}
</div>
@endif