<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
      <base href="{{url('./')}}">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <!-- Font-Awesome CSS -->
      <link rel="stylesheet" href="assets/css/all.css">
      <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- Theme CSS -->
      <link rel="stylesheet" href="assets/css/admin-theme.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
      <!-- Custome CSS -->
      <link rel="stylesheet" type="text/css" href="assets/css/main-style.css">
        <title>{{$sTitle}} | Student Lunch</title>
    </head>
    <body id="landing" class="sidebar-open">