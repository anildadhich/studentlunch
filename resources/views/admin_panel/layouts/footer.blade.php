		<div class="modal fade bd-example-modal-lg" id="OrderModel" tabindex="-1" role="dialog" aria-spanledby="myLargeModalspan"  >
			<div class="modal-dialog modal-lg">
			  <div class="modal-content"> 
				 
				<div class="modal-body">
					<div class="row">
						<div class="col-6">
							<h4>Get Details</h4>
						</div>
						<div class="col-6 text-right">
						   <span id="sOrdrIdNo"></span> <br>  <span id="sCrtDtTm"></span>
						</div>
					</div>
					<div class="row pt-4">
						<div class="col-md-6">
							<strong>From: </strong> <br>
							<p class="pl-4">
								<span id="sBussName"></span> <br>
								<span id="sMlkAdrs"></span> <br>
								<span id="sMlkPhnNo"></span> <br>
								<span id="sMlkEmail"></span>
							</p>
						  
						</div>
						<div class="col-md-6">
							<strong>To:  </strong> <br>
							 
							<p class="pl-4">  
								<span id="sChldName"></span> <br>
								<span id="sSchlAdrs"></span> <br>
								<span id="sSchlPhnNo"></span> <br>
								<span id="sSchlEmail"></span>
							</p>
						</div>
					</div>
					<div class="row pt-4">
						<div class="col-12">
							<table class="table  addresspopuptable">
								<thead>
								  <tr>
									<th scope="col">S.No</th>
									<th scope="col">Item Name</th>
									<th scope="col">Quantity</th>
									<th scope="col">Unit Cost</th>
									<th scope="col">Total</th>
								  </tr>
								</thead>
								<tbody id="aItms">
								  
								</tbody>
							  </table>
						</div>
					</div>
					<div class="row justify-content-end">  
						<div class="col-md-4 col-sm-6  col-9">
							<table  class=" w-100">
								<tr>
									<td>
										Subtotal:
									</td>
									<td>
										<span id="sSubTtl" class='text-right'></span>
									</td>
								</tr>
								<tr>
									<td>
										GST:
									</td>
									<td>
										<span id="sGst" class='text-right'></span>
									</td>
								</tr>
								<tr>
									<td>
										Total Used Credit:
									</td>
									<td>
										<span id="sCrdt" class='text-right'></span>
									</td>
								</tr>
								<tr>
									<td>
										Total Used Pay:
									</td>
									<td>
										<span id="sPay" class='text-right'></span>
									</td>
								</tr>
								<tr>
									<td>
										Grand Total:
									</td>
									<td>
										<span id="sTtl" class='text-right'></span>
									</td>
								</tr>
							</table> 
						</div> 
					</div>
					<div class="row">
						<div class="col-12 text-center">
							<button class="btn btn-primary"  data-dismiss="modal" aria-span="Close">
								Close
							</button>
						</div>
					</div>
				</div>
			  </div>
			</div>
		</div>
		<style>
			.addresspopuptable td, .addresspopuptable th{
				border:1px solid #0009 !important;

			}
			.addresspopuptable thead tr{
				background:#f2f2f2;
			}
			 
			@media only screen and (max-width: 500px) {
				.addresspopuptable{
					display: block;
					overflow: scroll;
				}
			  }
		</style>
		<script type="text/javascript">
		var APP_URL = "{{url('/')}}";
		</script>
		{!! Charts::scripts() !!}
 <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap js -->
        <script src="assets/scripts/jquery.min.js"></script>
		<script src="js/form-validation.min.js"></script>
        <script src="assets/scripts/popper.min.js" ></script>
        <script src="assets/scripts/bootstrap.min.js" ></script>
        <script src="assets/scripts/library.min.js"></script>
        <script src="assets/scripts/main.js"></script>
        <script src="assets/js/custome-script.js"></script>
        <script src="js/common-script.js"></script>
    </body>
</html>