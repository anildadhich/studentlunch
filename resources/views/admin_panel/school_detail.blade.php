@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('admin_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">School Details </h4>
                        </div>
                    </div>
                </div>
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section  parent-details-section">
                    <div class="row">
                        <div class="col-sm-3 col-lg-12">
                            <div>
                                <h4>Get Details</h4>
                            </div>
                            <div>
                                <h5>School all information below</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-lg-6 table-responsive">
                            <table class="parent-details-table">
                                <tr>
                                    <td>Account ID</td>
                                    <td class="text-right">{{$aSchlDtl['lAcc_Id']}}</td>
                                </tr>
                                <tr>
                                    <td>School Type</td>
                                    <td class="text-right">{{array_search($aSchlDtl['lSchl_Type'], config('constant.SCHL_TYPE'))}}</td>
                                </tr>
                                <tr>
                                    <td>School Name</td>
                                    <td class="text-right">{{$aSchlDtl['sSchl_Name']}}</td>
                                </tr>
                                <tr>
                                    <td>Mobile No</td>
                                    <td class="text-right">{{$aSchlDtl['sMobile_No']}}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td class="text-right">{{$aSchlDtl['sEmail_Id']}}</td>
                                </tr>
                                <tr>
                                    <td>Phone Number</td>
                                    <td class="text-right">{{$aSchlDtl['sPhone_No']}}</td>
                                </tr>
                                
                            </table>
                        </div>
                        <!-- <div class="col-2"></div> -->
                        <div class="col-sm-6 col-lg-6 table-responsive">
                            <table class="parent-details-table">
                                <tr>
                                    <td>Street Number</td>
                                    <td class="text-right">{{$aSchlDtl['sStrt_No']}}</td>
                                </tr>
                                <tr>
                                    <td>Street Name</td>
                                    <td class="text-right">{{$aSchlDtl['sStrt_Name']}}</td>
                                </tr>
                                <tr>
                                    <td>Suburb</td>
                                    <td class="text-right">{{$aSchlDtl['sSbrb_Name']}}</td>
                                </tr>
                                <tr>
                                    <td>State Name</td>
                                    <td class="text-right">{{$aSchlDtl['sState_Name']}}</td>
                                </tr>
                                <tr>
                                    <td>Post Code</td>
                                    <td class="text-right">{{$aSchlDtl['sPin_Code']}}</td>
                                </tr>
								<tr>
                                    <td>Country</td>
                                    <td class="text-right">{{$aSchlDtl['sCntry_Name']}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="container-fluid card-commission-section  parent-details-section mt-5">
                    <div class="row">
                        <div class="col-sm-3 col-lg-12">
                            <div>
                                <h4>Contact Details</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 table-responsive">
                            <table width="100%" class="child-details-table">
                                <tr>
									<th>Role</th>
									<th>Title</th>
									<th>First Name</th>
									<th>Surname</th>
									<th>Phone Number</th>
									<th>Mobile Number</th>
									<th>Email</th>
								</tr>
                                @foreach($aSchlCntctLst As $aRec)
                                <tr>
                                    <td>{{array_search($aRec['nCntct_Role'], config('constant.SCHL_ROLE'))}}</td>
									<td>{{array_search($aRec['nCntct_Title'], config('constant.TITLE'))}}</td>
                                    <td>{{$aRec['sFrst_Name']}}</td>
                                    <td>{{$aRec['sLst_Name']}}</td>
                                    <td>{{$aRec['sPhone_No']}}</td>
                                    <td>{{$aRec['sMobile_No']}}</td>
                                    <td>{{$aRec['sEmail_Id']}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="back-btn text-center">
                                <button title="Back" onclick="history.back()"> Back</button>
                                <a href="{{url('admin_panel/school/manage')}}?lRecIdNo={{base64_encode($aSchlDtl['lSchl_IdNo'])}}">
                                    <button title="Edit {{$aSchlDtl['sSchl_Name']}}"> Edit</button>
                                </a>
                            </div>
                        </div>
                    </div>
				</div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')