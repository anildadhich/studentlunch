@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('admin_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Manage Commission</h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section">
                    <form action="{{url('admin_panel/manage_commission')}}" method="get" id="commission_form">
                        <div class="row first-block">
                            <div class='col-sm-3 from-boxes'>
                                <label>From Date (Transaction)</label>
                                <input type="date" name="sFrmDate" placeholder="MM/DD/YYYY" value="{{$request['sFrmDate']}}"> 
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>To Date (Transaction)</label>
                                <input type="date" name="sToDate" placeholder="MM/DD/YYYY" value="{{$request['sToDate']}}"> 
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>Milk Bar Name</label>
                                <select name="lMilkIdNo">
                                    <option value="">All Milk Bar</option>
									@foreach($aMilkLst as $aRec)
										<option {{ $request['lMilkIdNo'] == $aRec['lMilk_IdNo'] ? 'selected' : ''}} value="{{$aRec['lMilk_IdNo']}}">{{$aRec['sBuss_Name']}}</option>
									@endforeach
                                </select>
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button id="Filter">Filter</button></li>
                                    <li><button type="button" class="mr-0" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details table-responsive parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Order No</th>
                                    <th>Transaction Date</th>
                                    <th>Milk Bar Name</th>
									<th class="text-right">Order Amount</th>
                                    <th class="text-right">Used Credit</th>
                                    <th class="text-right">Online Pay</th>
                                    <th class="text-right">Commission</th>
                                    <th class="text-center">Action</th>
								</tr>
								@if(count($oComLst) > 0)
                                    @foreach($oComLst As $aRec)
										<tr>
                                            <td>{{$aRec->sOrdr_Id}}</td>
                                            <td>{{date('d M, Y h:i A', strtotime($aRec->sCrt_DtTm))}}</td>
                                            <td>{{$aRec->sBuss_Name}}</td>
                                            <td class="text-right">$ {{$aRec->sGrnd_Ttl}}</td>
                                            <td class="text-right">$ {{$aRec->sTtl_Amo}}</td>
                                            <td class="text-right">$ {{$aRec->sGrnd_Ttl - $aRec->sTtl_Amo}}</td>
                                            <td class="text-right">$ {{ number_format(($aRec->sGrnd_Ttl - $aRec->sTtl_Amo)*0.125, 2)}}</td>
											<td class="action-btns text-center">
											    <ul>
													<li class="detail_btn my-order-btns"><a href="#" title="View" data-rec="{{$aRec->sOrdr_Id}}" data-toggle="modal" data-target="#OrderModel"> View</a></li>
												</ul>
											</td>
										</tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="8" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
							{{$oComLst->appends($request->all())->render()}}
                        </div>
                    </div>
					
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
$('#Filter').on('click', function() {
    var sFrmDate = $("input[name=sFrmDate]").val();
    var sToDate = $("input[name=sToDate]").val();
    if(sFrmDate != '' && sFrmDate > sToDate)
    {
        alert('To Date should be greater then From Date');
        return false;
    }
    else
    {
        $('#commission_form').submit();
    }
});

$('#ExprtRcrd').on('click', function() {
    var sFrmDate       = $("input[name=sFrmDate]").val();
    var sToDate       = $("input[name=sToDate]").val();
    if(sFrmDate != '' && sFrmDate > sToDate)
    {
        alert('To Date should be greater then From Date');
        return false;
    }
    else
    {
        var lMilkIdNo       = $("select[name=lMilkIdNo]").find(":selected").val();
        var lMilkIdNo       = lMilkIdNo == 'undefined' ? '' : lMilkIdNo;
        window.location=APP_URL+"/admin_panel/manage_commission/export?sFrmDate="+sFrmDate+"&sToDate="+sToDate+"&lMilkIdNo="+lMilkIdNo;
    }
});

$('#OrderModel').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var aRec = button.data('rec');
	GetOrder(aRec);
});
</script>
