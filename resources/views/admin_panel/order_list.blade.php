@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('admin_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Manage Orders</h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section">
                    <form action="{{url('admin_panel/manage_order')}}" method="get" id="order_form">
                        <div class="row first-block">
                            <div class='col-sm-3 from-boxes'>
                                <label>From Date (Delivery)</label>
                                <input type="date" name="sFrmDate" placeholder="MM/DD/YYYY" value="{{$request['sFrmDate']}}"> 
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>To Date (Delivery)</label>
                                <input type="date" name="sToDate" placeholder="MM/DD/YYYY" value="{{$request['sToDate']}}"> 
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>Parent Name</label>
                                <input type="text" name="sPrntName" value="{{$request['sPrntName']}}" onkeypress="return IsAlpha(event, this.value, '15')"> 
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>Milk Bar Name</label>
                                <select name="lMilkIdNo">
                                    <option value="">All Milk Bar</option>
									@foreach($aMilkLst as $aRec)
										<option {{ $request['lMilkIdNo'] == $aRec['lMilk_IdNo'] ? 'selected' : ''}} value="{{$aRec['lMilk_IdNo']}}">{{$aRec['sBuss_Name']}}</option>
									@endforeach
                                </select>
                            </div>
                            <div class='col-sm-3 from-boxes'>
                                <label>Sbrb Name</label>
                                <input type="text" name="sSbrbName" value="{{$request['sSbrbName']}}" onkeypress="return IsAlpha(event, this.value, '20')"> 
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button id="Filter">Filter</button></li>
                                    <li><button type="button" class="mr-0" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details table-responsive parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Order No</th>
									<th>Student Name</th>
                                    <th>Parent Name</th>
                                    <th>Milk Bar Name</th>
									<th class="text-right">Amount</th>
									<th>Delivery Date</th>
									<th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
								</tr>
								@if(count($oOrdLst) > 0)
                                    @foreach($oOrdLst As $aRec)
										<tr>
                                            <td>{{$aRec->sOrdr_Id}}</td>
                                            <td>{{$aRec->sChld_FName}} {{$aRec->sChld_LName}}</td>
                                            <td>{{$aRec->sPrnt_FName}} {{$aRec->sPrnt_LName}}</td>
                                            <td>{{$aRec->sBuss_Name}}</td>
                                            <td class="text-right">$ {{$aRec->sGrnd_Ttl}}</td>
											<td>{{date('d M, Y', strtotime($aRec->sDelv_Date))}}</td>
											<td class="text-center">
                                                @if($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Pending'))
                                                    <button class="primary-btn" title="Pending">Pending</button>
                                                @elseif($aRec->nOrdr_Status == config('constant.ORDER_STATUS.Delivered'))
                                                    <button class="active-btn" title="Delivered">Delivered</button>
                                                @else
                                                    <button class="block-btn" title="Cancelled">Cancelled</button>
                                                @endif
                                            </td>
											<td class="action-btns text-center">
											    <ul>
													<li class="detail_btn my-order-btns"><a href="#" title="View" data-rec="{{$aRec->sOrdr_Id}}" data-toggle="modal" data-target="#OrderModel"> View</a></li>
												</ul>
											</td>
										</tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="8" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
							{{$oOrdLst->appends($request->all())->render()}}
                        </div>
                    </div>
					
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
$('#Filter').on('click', function() {
    var sFrmDate = $("input[name=sFrmDate]").val();
    var sToDate = $("input[name=sToDate]").val();
    if(sFrmDate != '' && sFrmDate > sToDate)
    {
        alert('To Date should be greater then From Date');
        return false;
    }
    else
    {
        $('#order_form').submit();
    }
});

$('#ExprtRcrd').on('click', function() {
    var sFrmDate       = $("input[name=sFrmDate]").val();
    var sToDate       = $("input[name=sToDate]").val();
    if(sFrmDate != '' && sFrmDate > sToDate)
    {
        alert('To Date should be greater then From Date');
        return false;
    }
    else
    {
        var sPrntName       = $("input[name=sPrntName]").val();
        var lMilkIdNo       = $("select[name=lMilkIdNo]").find(":selected").val();
        var lMilkIdNo       = lMilkIdNo == 'undefined' ? '' : lMilkIdNo;
        var sSbrbName       = $("input[name=sSbrbName]").val();
        window.location=APP_URL+"/admin_panel/manage_order/export?sFrmDate="+sFrmDate+"&sToDate="+sToDate+"&sPrntName="+sPrntName+"&lMilkIdNo="+lMilkIdNo+"&sSbrbName="+sSbrbName;
    }
});

$('#OrderModel').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var aRec = button.data('rec');
	GetOrder(aRec);
});
</script>
