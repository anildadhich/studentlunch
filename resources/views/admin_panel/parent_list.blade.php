@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('admin_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Parent List </h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section ">
                    <form action="{{url('admin_panel/parent/list')}}" method="get">
                        <div class="row first-block parent-list-form">
                            <div class="col-sm-3">
                                <label>Parent Name</label>
                                <input type="text" name="sPrntName" placeholder="Parent Name" value="{{$request['sPrntName']}}">
                            </div>
                            <div class="col-sm-3">
                                <label>Parent Mobile Number</label>
                                <input type="text" name="sMobileNo"  placeholder="Mobile Number" value="{{$request['sMobileNo']}}">
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button title="Filter">Filter</button></li>
                                    <li><button class="mr-0" type="button" title="Export To Excel" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details table-responsive parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Account ID</th>
                                    <th>Parent Name</th>
                                    <th>Mobile Number</th>
                                    <th>Create Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                @if(count($aPrntsLst) > 0)
                                    @foreach($aPrntsLst As $aRec)
                                    <tr>
                                        <td>{{$aRec->sAcc_Id}}</td>
                                        <td>{{$aRec->sFrst_Name}} {{$aRec->sLst_Name}}</td>
                                        <td>{{$aRec->sMobile_No}}</td>
                                        <td>{{date('d F, Y', strtotime($aRec->sCrt_DtTm))}}</td>
                                        <td>
                                            @if($aRec->nBlk_UnBlk == config('constant.STATUS.BLOCK'))
                                                <button class="block-btn" title="Block" onclick="chngStatus('{{base64_encode('mst_prnts')}}','{{base64_encode('lPrnt_IdNo')}}','{{base64_encode($aRec->lPrnt_IdNo)}}','{{base64_encode(config('constant.STATUS.UNBLOCK'))}}')">Block</button>
                                            @else
                                                <button class="active-btn" title="Unblock" onclick="chngStatus('{{base64_encode('mst_prnts')}}','{{base64_encode('lPrnt_IdNo')}}','{{base64_encode($aRec->lPrnt_IdNo)}}','{{base64_encode(config('constant.STATUS.BLOCK'))}}')">Unblock</button>
                                            @endif
                                        </td>
                                        <td class="detail_btn"><a href="{{url('admin_panel/parent/detail')}}?lRecIdNo={{base64_encode($aRec->lPrnt_IdNo)}}">Get Details</a></td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            {{$aPrntsLst->appends($request->all())->render()}}
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
$('#ExprtRcrd').on('click', function() {
    var sPrntName = $("input[name=sPrntName]").val();
    var sMobileNo = $("input[name=sMobileNo]").val();
    window.location=APP_URL+"/admin_panel/parent/export?sPrntName="+sPrntName+"&sMobileNo="+sMobileNo;
});
</script>