@include('admin_panel.layouts.header')
    <div class="page-container animsition">
        <div id="dashboardPage">
            <!-- Main Menu -->
            @include('admin_panel.layouts.top_bar')
            <!-- Main Menu -->
            @include('admin_panel.layouts.side_panel')
            <main>
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="page-title">Manage School</h4>
                        </div>
                    </div>
                </div>
                @include('admin_panel.layouts.message')
                <!-- My Commissions From -->
                <div class="container-fluid card-commission-section">
                    <form action="{{url('admin_panel/school/list')}}" method="get">
                        <div class="row first-block parent-list-form">
                            <div class='col-sm-3'>
                                <label>School Name</label>
                                <input type="text" name="sSchlName" placeholder="School Name" value="{{$request['sSchlName']}}">
                            </div>
                            <div class='col-sm-3'>
                                <label>Mobile Number</label>
                                <input type="text" name="sMobileNo" placeholder="Mobile Number" value="{{$request['sMobileNo']}}">
                            </div>
                            <div class='col-sm-3 form-btns'>
                                <ul>
                                    <li><button type="submit" title="Filter">Filter</button></li>
                                    <li><button class="mr-0" type="button" title="Export To Excel" id="ExprtRcrd">Export To Excel</button></li>
                                </ul>
                            </div>
                            <div class='col-sm-3 text-right'>
                                <div class="add-btn">
                                    <a href="{{url('admin_panel/school/manage')}}">
                                        <button title="Add School" type="button">Add School</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Commssions Details Tabel -->
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 commssions-table-details table-responsive parent-list-table">
                            <table style="width:100%">
                                <tr>
                                    <th>Account ID</th>
                                    <th>School Name </th>
                                    <th>Mobile Number</th>
                                    <th>Create Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
								@if(count($aSchlLst) > 0)
                                    @foreach($aSchlLst As $aRec)
										<tr>
											<td>{{$aRec->lAcc_Id}}</td>
											<td>{{$aRec->sSchl_Name}}</td>
											<td>{{$aRec->sMobile_No}}</td>
											<td>{{date('d F, Y', strtotime($aRec->sCrt_DtTm))}}</td>
											<td>
                                                @if($aRec->nBlk_UnBlk == config('constant.STATUS.BLOCK'))
                                                    <button class="block-btn" title="Block" onclick="chngStatus('{{base64_encode('mst_schl')}}','{{base64_encode('lSchl_IdNo')}}','{{base64_encode($aRec->lSchl_IdNo)}}','{{base64_encode(config('constant.STATUS.UNBLOCK'))}}')">Block</button>
                                                @else
                                                    <button class="active-btn" title="Unblock" onclick="chngStatus('{{base64_encode('mst_schl')}}','{{base64_encode('lSchl_IdNo')}}','{{base64_encode($aRec->lSchl_IdNo)}}','{{base64_encode(config('constant.STATUS.BLOCK'))}}')">Unblock</button>
                                                @endif
                                            </td>
											<td class="action-btns">
												<ul>
													<li><a href="{{url('admin_panel/school/detail')}}?lRecIdNo={{base64_encode($aRec->lSchl_IdNo)}}" title="{{$aRec->sSchl_Name}} Details"> <i class="fa fa-eye"></i></a></li>
													<li><a href="{{url('admin_panel/school/manage')}}?lRecIdNo={{base64_encode($aRec->lSchl_IdNo)}}" title="Edit {{$aRec->sSchl_Name}}"> <i class="fa fa-edit"></i></a></li>
													<li><i class="fa fa-trash" onclick="DelRec('{{base64_encode('mst_schl')}}','{{base64_encode('lSchl_IdNo')}}','{{base64_encode($aRec->lSchl_IdNo)}}')"></i></li>
												</ul>
											</td>
										</tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6" class="text-center"><strong>No Record(s) Found</strong></td></tr>
                                @endif
                            </table>
                        </div>
                    </div>
            <!--        <div class="row">
                        <div class="col-sm-12 col-lg-12">
                  
                            
                        </div>
                    </div>
					-->
                </div>
            </main>
        </div>
    </div>
@include('admin_panel.layouts.footer')
<script type="text/javascript">
$('#ExprtRcrd').on('click', function() {
    var sSchlName = $("input[name=sSchlName]").val();
    var sMobileNo = $("input[name=sMobileNo]").val();
    window.location=APP_URL+"/admin_panel/school/export?sSchlName="+sSchlName+"&sMobileNo="+sMobileNo;
});
</script>